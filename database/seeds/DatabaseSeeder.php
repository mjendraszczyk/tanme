<?php

use Illuminate\Database\Seeder;
use App\Blog;
use App\Ekspert;
use App\City;
use App\Province;
use App\Review;
use App\Role;
use App\Cms;
use App\Country;
use App\User;

use App\Category;
use App\Service;
use App\Opening;
use App\UserServices;

use mrcnpdlk\Teryt\Client;
use mrcnpdlk\Teryt\NativeApi;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Country
         */

         $kraj = new Country();
         $kraj->name = 'Polska';
         $kraj->save();


        /**
         * Role
         */
        $role = [
            'Użytkownik',
            'Administrator',
        ];
        foreach ($role as $rola) {
            $r = new Role();
            $r->name = $rola;
            $r->save();
        }
        /**
         * Category
         */
        $categories = [
            'Zabieg opalania natryskowego',
            'Dojazd do klienta',
            'Karnety',
        ];
        foreach ($categories as $category) {
            $c = new Category();
            $c->name = $category;
            $c->save();
        }

        /**
         * Services
         */

         $services = [
             [
            'name' => 'Opalanie całego ciała',
            'id_category' => '1'
             ],
             [
            'name' => 'Opalanie połowy ciała',
            'id_category' => '1'
             ],
             [
            'name' => 'Opalanie twarzy, dekoltu, rąk',
            'id_category' => '1'
             ],
             [
            'name' => 'Opalanie twarz i dekoltu',
            'id_category' => '1'
             ],
             [
            'name' => 'Na terenie miasta',
            'id_category' => '2'
             ],
             [
            'name' => 'Poza miastem',
            'id_category' => '2'
             ],
             [
            'name' => '4 zabigi',
            'id_category' => '3'
             ],
             [
            'name' => '6 zabiegów',
            'id_category' => '3'
             ],
             [
            'name' => '8 zabiegów',
            'id_category' => '3'
             ],
         ];

         foreach ($services as $service) {
                $s = new Service();
                $s->name = $service['name'];
                $s->id_category = $service['id_category'];
                $s->save();
         }

        /**
         * Provinces
         */

//         $provinces = array(
//             'Zachodniopomorskie',
//             'Pomorskie',
//             'Lubuskie',
//             'Wielkopolskie',
//             'Dolnośląskie',
//             'Warmińsko-Mazurskie',
//             'Mazowieckie',
//             'Podlaskie',
//             'Łódzkie',
//             'Śląskie',
//             'Małopolskie',
//             'Opolskie',
//             'Lubelskie',
//             'Podkarpackie',
//             'Świętokrzykie',
//             'Kujawsko-pomorskie'
//         );

//         foreach($provinces as $p) {
//             $province = new Province();
//             $province->name = $p;
//             $province->content = '<h1>Opalanie natryskowe '.$p.'</h1><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris et volutpat ipsum. Suspendisse dictum ullamcorper iaculis. Maecenas ut faucibus arcu. Pellentesque vel faucibus arcu. Integer dui leo, sodales nec consectetur id, vestibulum sit amet nibh. Morbi vitae dolor vitae neque sagittis bibendum. Vestibulum imperdiet, est in rutrum sodales, elit ex malesuada neque, a sollicitudin nunc orci ut eros. Sed aliquam lorem eget pellentesque semper. Sed ante nulla, malesuada sed dictum vulputate, semper vel felis. Aenean nec diam a libero faucibus rutrum. Phasellus ac cursus mauris. In in volutpat sem. Curabitur porta lacus nibh, et scelerisque eros lacinia non. Aenean tellus nulla, lobortis eu tempus euismod, suscipit eu urna. Etiam porttitor lobortis tellus, vel malesuada nisi efficitur non. Duis nec dolor tempor, fermentum libero in, euismod ipsum.</p>
// <br/><br/>
// <p>
// Duis congue vitae erat ut rhoncus. Donec ut augue in nulla euismod fringilla. Nulla at nisi sed libero imperdiet tempus in a justo. Pellentesque molestie eleifend justo, blandit ultricies neque. Curabitur consectetur, elit vitae varius tincidunt, ex velit congue orci, et tristique lorem sem at tellus. Ut id velit consectetur, consectetur nisl eget, vulputate erat. Aliquam tempus, risus sit amet egestas pretium, nisl nunc pulvinar leo, at porta ante orci at mauris.</p> ';
//             $province->id_country = '1';
//             $province->save();
//         }

        /**
         * City
         */

//         $cities = array(
//             'Szczecin',
//             'Warszawa',
//             'Gdańsk',
//             'Kalisz',
//             'Poznań',
//             'Katowice',
//             'Kraków',
//             'Wrocław',
//             'Piekary Śląskie'
//         );
        
//          foreach ($cities as $c) {
//              $city = new City();
//              $city->name = $c;
//              $city->image = str_slug($c).'.jpg';
//              $city->id_province = rand(1,16);
//              $city->content = '<h1>Opalanie natryskowe '.$c.'</h1><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris et volutpat ipsum. Suspendisse dictum ullamcorper iaculis. Maecenas ut faucibus arcu. Pellentesque vel faucibus arcu. Integer dui leo, sodales nec consectetur id, vestibulum sit amet nibh. Morbi vitae dolor vitae neque sagittis bibendum. Vestibulum imperdiet, est in rutrum sodales, elit ex malesuada neque, a sollicitudin nunc orci ut eros. Sed aliquam lorem eget pellentesque semper. Sed ante nulla, malesuada sed dictum vulputate, semper vel felis. Aenean nec diam a libero faucibus rutrum. Phasellus ac cursus mauris. In in volutpat sem. Curabitur porta lacus nibh, et scelerisque eros lacinia non. Aenean tellus nulla, lobortis eu tempus euismod, suscipit eu urna. Etiam porttitor lobortis tellus, vel malesuada nisi efficitur non. Duis nec dolor tempor, fermentum libero in, euismod ipsum.</p>
// <br/><br/>
// <p>
// Duis congue vitae erat ut rhoncus. Donec ut augue in nulla euismod fringilla. Nulla at nisi sed libero imperdiet tempus in a justo. Pellentesque molestie eleifend justo, blandit ultricies neque. Curabitur consectetur, elit vitae varius tincidunt, ex velit congue orci, et tristique lorem sem at tellus. Ut id velit consectetur, consectetur nisl eget, vulputate erat. Aliquam tempus, risus sit amet egestas pretium, nisl nunc pulvinar leo, at porta ante orci at mauris.</p> ';
//              $city->save();

//          }
         /**
          * Ekspert
          */
        for ($j=1;$j<=5;$j++) {
            $ekspert = new Ekspert();
            $ekspert->name='Timetotan';
            $ekspert->content = '
                            <h6>Zabieg opalania natryskowego</h6>
                            <table>
                                <tbody><tr>
                                    <td>Opalanie całego ciała</td>
                                    <td>100 zł</td>
                                </tr>
                                <tr>
                                    <td>Opalanie połowy ciała</td>
                                    <td>70 zł</td>
                                </tr>
                                <tr>
                                    <td>Opalanie twarzy, dekoltu, rąk</td>
                                    <td>50 zł</td>
                                </tr>
                                <tr>
                                    <td>Opalanie twarz i dekoltu</td>
                                    <td>40 zł</td>
                                </tr>
                            </tbody></table>


                            <h6>Dojazd do klienta</h6>
                            <table>
                                <tbody><tr>
                                    <td>Na terenie miasta</td>
                                    <td>30 zł</td>
                                </tr>
                                <tr>
                                    <td>Poza miast</td>
                                    <td>indywidualnie</td>
                                </tr>

                            </tbody></table>

                            <h6>Karnety</h6>
                            <table>
                                <tbody><tr>
                                    <td>4 zabigi</td>
                                    <td>350 zł</td>
                                </tr>
                                <tr>
                                    <td>6 zabiegów</td>
                                    <td>500 zł</td>
                                </tr>
                                <tr>
                                    <td>8 zabiegów</td>
                                    <td>600 zł</td>
                                </tr>

                            </tbody></table>
                        ';
                        $ekspert->feature = '1';
            $ekspert->feature_image = 'glowna_polecane'.$j.'.jpg';
            $ekspert->cert = rand(0,1);
            $ekspert->phone = '123 123 123';
            $ekspert->typ = rand(0,1);
            $ekspert->address = 'Kurkowa 5/U1, 70-535';
            $ekspert->city = rand(1,5);
            $ekspert->lat = '53.43';
            $ekspert->lon = '14.48';
            $ekspert->province = rand(1,16);
            $ekspert->expert_email = 'example'.$j.'@example.com';
            $ekspert->id_user = $j;
            $ekspert->short_description = 'Krotki opis...';
            $ekspert->save();

            $ceny = [
                rand(10,200),
                rand(10,200),
                rand(10,200),
                rand(10,200),
                rand(10,200),
                rand(10,200),
                rand(10,200),
                rand(10,200),
                rand(10,200),
            ];

            $services = Service::all();
            $i=0;
        foreach($ceny as $key => $cena) {
            // if (!empty($cena)) {
                $userServices = new UserServices();
                $userServices->id_ekspert = $ekspert->id_ekspert;
                $userServices->id_service = $services[$i]['id_service'];
                $userServices->price = $cena;
                $userServices->save();
                $i++;
            // }
        }


$godzina_od_tab = [
    rand(0,23),
    rand(0,23),
    rand(0,23),
    rand(0,23),
    rand(0,23),
    rand(0,23),
    rand(0,23),
];

$godzina_do_tab = [
    rand(0,23),
    rand(0,23),
    rand(0,23),
    rand(0,23),
    rand(0,23),
    rand(0,23),
    rand(0,23),
];

        foreach($godzina_od_tab as $key => $godzina_od) {
    
            $opening = new Opening();
            $opening->id_user = $ekspert->id_ekspert;
            $opening->dzien = $key+1;
            $opening->godzina_od = $godzina_od;
            $opening->godzina_do = $godzina_do_tab[$key];
            $opening->save();
        }
       

            //   'name', 'content', 'feature_image','cert','phone','expert_mobile','address','city','province','expert_email'
        }

        /**
         * Opinie
         */
        for($i=1;$i<=10;$i++) {
            $opinia = new Review();
            $opinia->id_user = rand(1,5);
            $opinia->comment = ' Lorem ipsum dolor sit amet, ne novum debet invenire usu, latine saperet definiebas vis te. Cu sea mundi consul. An vis quod esse appetere, case vide deterruisset sit cu, mea no appareat disputando. Duo habeo fugit patrioque ut, facer aeque vituperata no mel, libris possit vim ad. ';
            $opinia->rate = rand(1,5);
            $opinia->id_ekspert = rand(1,10);
            $opinia->nick = 'example_'.$i.'';
            //$opinia->state = '1';
            $opinia->save();
        }

        // $this->call(UsersTableSeeder::class);
        /**
         * Blog
         */
        for ($i=1;$i<=5;$i++) {
            $blog = new Blog();
            $blog->title = 'Lorem ipsum dolor sit amet an putent nam quem';
            $blog->content = '
                        Te qui quot inimicus inciderint, in per iusto menandri. Vis id duis autem definitionem, ad
                        alia
                        constituto signiferumque
                        quo, reque causae vix at. Nec facilis voluptatibus in...
                        ';
            $blog->feature_image = 'glowna_polecane'.$i.'.jpg';
            $blog->date_add = date('Y-m-d');
            $blog->save();
        }
        /**
         * User
         */

         for ($i=1;$i<=5;$i++) {
             $user = new User();
             $user->name = 'Example examle';
             $user->id_role = '1';
             $user->business = 'example'.$i;
             $user->email = 'example'.$i.'@example.pl';
             $user->password = bcrypt('1');
             $user->save();
         }

         $user = new User();
             $user->name = 'Michał J';
             $user->id_role = '2';
             $user->business = 'mages';
             $user->email = 'biuro@mages.pl';
             $user->password = bcrypt('1');
             $user->save();
             
         

        /**
         * Cms
         */

         $cms = new Cms();
         $cms->title = 'Polityka prywatności';
         $cms->content = 'Polityka prywatności';
         $cms->save();

         $cms = new Cms();
         $cms->title = 'Regulamin';
         $cms->content = 'Regulamin';
         $cms->save();



           $oClient = new Client();
    //$oClient->setConfig('mjendraszczyk', 'Di9!neTp78', true);
    $oClient->setConfig('mjendraszczyk', 'Di9!neTp78', true);
    $oNativeApi = NativeApi::create($oClient);
    $oNativeApi->CzyZalogowany();

     $position=0;
    //var_dump($oNativeApi->PobierzListeWojewodztw());
    foreach ($oNativeApi->PobierzListeWojewodztw() as $wojewodztwo) {

        /* Wojewodztwo */
        $checkIfExist = Province::where('name', $wojewodztwo->name)->where('id_distinct', $wojewodztwo->provinceId)->count();

        if($checkIfExist== 0) {
            $w = new Province();
            $w->name = mb_strtolower($wojewodztwo->name, 'UTF-8');
            $w->id_distinct = $wojewodztwo->provinceId;
            $w->id_country = '1';
            $w->content = '<h1>Opalanie natryskowe '.mb_strtolower($wojewodztwo->name,'UTF-8').'</h1>';
            $w->save();
        }
        //var_dump($oNativeApi->PobierzListePowiatow($wojewodztwo->provinceId));

       
        foreach ($oNativeApi->PobierzListePowiatow($wojewodztwo->provinceId) as $powiat) {
            //dd($powiat);
            //dd($oNativeApi->PobierzListeGmin($powiat->provinceId, $powiat->districtId));
            foreach ($oNativeApi->PobierzListeGmin($powiat->provinceId, $powiat->districtId) as $gmina) {
                // if (($gmina->typeName != 'miasto') && ($gmina->typeName != 'gmina miejska')){
                $checkIfExist = City::where('name',$gmina->name)->where('id_province', $w->id_province)->count();
                if ($checkIfExist == 0) {
                    $m = new City();
                    $m->name = $gmina->name;
                    $m->content = '<h1>Opalanie natryskowe '.$gmina->name.'</h1>';
                    $m->id_province = $w->id_province;
                    $m->position = $position+2;
                    $m->save();
                    $position++;
                }
            }
        }
    }

            $cities = array(
            'Szczecin',
            'Warszawa',
            'Gdańsk',
            'Kalisz',
            'Poznań',
            'Katowice',
            'Kraków',
            'Wrocław',
            'Piekary Śląskie'
        );


              
         foreach ($cities as $c) {
             if($c == 'Szczecin') {
                $pozycja = 0;
             } else {
                $pozycja = 1;
             }  
             $city = City::where('name',$c)->update([
                'image' => str_slug($c).'.jpg',
                'position' => $pozycja
             ]);
         }

    }
}
