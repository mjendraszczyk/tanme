<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEkspertTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ekspert', function (Blueprint $table) {
            $table->bigIncrements('id_ekspert');
            $table->string('name')->nullable();
            $table->text('content')->nullable();
            $table->string('feature_image')->nullable();
            $table->integer('cert')->nullable();
            $table->string('phone')->nullable();
            $table->integer('typ')->nullable();
            $table->integer('city')->nullable();
            $table->string('address')->nullable();
            $table->integer('province')->nullable();
            $table->string('expert_email')->nullable();
            $table->integer('feature')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('youtube')->nullable();
            $table->float('lat')->nullable();
            $table->float('lon')->nullable();
            $table->integer('id_user');
            $table->text('short_description')->nullable();
            
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ekspert');
    }
}
