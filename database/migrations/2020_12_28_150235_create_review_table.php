<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review', function (Blueprint $table) {
            $table->bigIncrements('id_review');
            $table->integer('id_user')->nullable();
            $table->text('comment');
            $table->integer('rate');
            $table->integer('id_ekspert')->unsigned()->nullable();
            $table->string('nick')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();


            // $table->foreign('id_ekspert')->references('id_ekspert')->on('ekspert');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review');
    }
}
