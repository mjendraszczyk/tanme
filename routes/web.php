<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Ekspert;
use App\Review;
use App\Opening;
use App\User;
use App\UserServices;
use App\Category;
use App\Service;
use App\Galeria;
    Auth::routes();



Route::get('auth/facebook', 'frontend\FacebookController@redirectToFacebook')->name('redirectToFacebook');

Route::get('auth/facebook/callback', 'frontend\FacebookController@facebookSignin')->name('facebookSignin');

    /** 
         * Upload > croped
         */

        Route::post('panel/galeria/croped', 'frontend\GaleriaController@cropUpload')->name('frontend_galeria_croped');



        /**
         * Frontend > galeria
         */

        Route::get('/panel/galeria', 'frontend\GaleriaController@index')->name('frontend_galeria_index');
        Route::get('/panel/galeria/create', 'frontend\GaleriaController@create')->name('frontend_galeria_create');
        Route::get('/panel/galeria/{id}/edit', 'frontend\GaleriaController@edit')->name('frontend_galeria_edit');
        Route::post('/panel/galeria', 'frontend\GaleriaController@store')->name('frontend_galeria_store');
        Route::put('/panel/galeria/{id}/edit', 'frontend\GaleriaController@update')->name('frontend_galeria_update');
        Route::delete('/panel/galeria/{id}', 'frontend\GaleriaController@destroy')->name('frontend_galeria_destroy');



/**
 * Frontend > ekspert
 */
 Route::post('/', 'frontend\EkspertController@ekspert_kontakt_store')->name('ekspert_kontakt_store');

 Route::post('/api/login', 'Controller@login_api')->name('login_api');
 
 // Sitemap Routing
Route::get('/sitemap.xml', 'Controller@sitemap')->name('sitemap');

/**
 * Frontend > Home
 */
Route::get('/', 'frontend\HomeController@index')->name('home');

Route::domain(env('SITE_URL'))->group(function () {


     Route::get('/profil/{account}', function($account) {
        $user = User::where('business',$account)->first();

        
        if($user) {
            $ekspert = Ekspert::where('id_user', $user->id)->get();

            $reviewEkspert = Ekspert::where('id_user', $user->id)->first();

            $ocena = Review::where('id_ekspert', $reviewEkspert->id_ekspert)->where('status','1')->avg('rate');
          
            $getCennik = UserServices::where('id_ekspert',$reviewEkspert->id_ekspert)->get();
            $opening = Opening::where('id_user', $reviewEkspert->id_ekspert)->get();
            // dd($opening);
            $categories = Category::get();

            $galleries = Galeria::where('id_user', $user->id)->get();
            return view('frontend.ekspert.show')->with('ekspert', $ekspert)->with('rate', round($ocena))->with('opening', $opening)->with('getCennik', $getCennik)->with('categories', $categories)->with('galleries', $galleries);
        } else {
          return view('errors.404');
        }
      })->name('profil_alias');

     

    /**
     * Frontend > Review
     */
    Route::post('/opinia/dodaj', 'frontend\ReviewController@review_request')->name('review_request');
    

 
    Route::post('/eksperci/znajdz', 'frontend\HomeController@requestWojewodztwo')->name('wojewodztwo_request');
    
    
    /**
     * Frontend > Blog
     */
    Route::get('/blog', 'frontend\BlogController@index')->name('blog_index');
    Route::get('/blog/artykul/{id}/{title}', 'frontend\BlogController@show')->name('blog_show');

    /**
     * Frontend > Miasta
     */
    Route::get('/miasta', 'frontend\CityController@index')->name('miasta_index');
    Route::get('/miasto/{id}/{nazwa}', 'frontend\CityController@show')->name('miasta_show');
    /**
     * Frontend > Kontakt
     */
    Route::get('/kontakt', 'frontend\CmsController@kontakt')->name('kontakt_index');
    Route::post('/kontakt', 'frontend\CmsController@kontakt_store')->name('kontakt_store');

    /**
     * Frontend > Dla ekspertek
     */
    
    Route::get('/dla-ekspertek', 'frontend\CmsController@dla_ekspertek')->name('dla_ekspertek');

    /**
     * Frontend > Ekspert
     */

    Route::get('/eksperci', 'frontend\EkspertController@index')->name('ekspert_index');

    /**
     * Frontend > Ekspert
     */
    Route::post('/profil/image/croped', 'Controller@cropUpload')->name('profil_image_croped');


    Route::post('/api/profil/json', 'frontend\EkspertController@checkBusinessName')->name('checkBusinessName');
    
      Route::get('/api/wojewodztwo/{id_wojewodztwa}/miasta/', 'Controller@getAsyncMiastaByWojewodztwo')->name('get_miasta_by_wojewodztwo');
    
      Route::get('/api/wojewodztwo/{id_wojewodztwa}', 'Controller@getApiWojewodztwo')->name('get_api_by_wojewodztwo');

        Route::get('/api/miasto/{id_miasta}', 'Controller@getApiMiasto')->name('get_api_by_miasto');
    // Route::get('/ekspert/wojewodztwo/{wojewodztwo}/miasto/{miasto}/filtr/{filtr}', 'frontend\EkspertController@getEkspertByWojewodztwo')->name('ekspert_by_wojewodztwo');

    // Route::get('/ekspert/wojewodztwo/{wojewodztwo}/miasto/{miasto}/filtr/{filtr}', 'frontend\EkspertController@getEkspertsByFilters')->name('eksperts_by_filters');
    // Route::post('/ekspert/wojewodztwo/miasto/filtr', 'frontend\EkspertController@getEkspertsByFiltersRequest')->name('eksperts_by_filters_request');
    Route::get('/ekspert/opalanie-natryskowe/{wojewodztwo}/{wojewodztwo_name}/{miasto}/{miasto_name}/filtr/{filtr}', 'frontend\EkspertController@getEkspertsByFilters')->name('eksperts_by_filters');
    Route::post('/ekspert/opalanie-natryskowe/wojewodztwo/filtr', 'frontend\EkspertController@getEkspertsByFiltersRequest')->name('eksperts_by_filters_request');

    Route::get('/api/ekspert/opalanie-natryskowe/{wojewodztwo}/{wojewodztwo_name}/{miasto}/{miasto_name}/filtr/{filtr}', 'frontend\EkspertController@getApiEkspertsByFilters')->name('api_eksperts_by_filters');
    

    // Route::post('/ekspert/{wojewodztwo}', 'frontend\EkspertController@getEkspertByWojewodztwo')->name('ekspert_by_wojewodztwo_request');

    Route::get('/ekspert/{id}', 'frontend\EkspertController@show')->name('ekspert_show');

    /**
     * Frontend > Szkolenie
     */

    Route::get('/szkolenie', 'frontend\CmsController@szkolenie')->name('szkolenie_index');

    /**
     * Frontend > Miasta
     */
    
    Route::get('/miasta', 'frontend\MiastoController@index')->name('miasto_index');
    Route::get('/miasta/miasto/{id}', 'frontend\MiastoController@show')->name('miasto_show');

    /**
     * Frontend > Rejestracja
     */

    Route::get('/rejestracja/krok1', 'auth\RegisterController@krok1')->name('rejestracja_krok1');
    Route::post('/rejestracja/krok1', 'auth\RegisterController@krok1_request')->name('rejestracja_krok1_request');

     
    Route::get('/rejestracja/krok2', 'auth\RegisterController@krok2')->name('rejestracja_krok2');
    Route::post('/rejestracja/krok2', 'auth\RegisterController@krok2_request')->name('rejestracja_krok2_request');

    Route::get('/rejestracja/krok3', 'auth\RegisterController@krok3')->name('rejestracja_krok3');
    Route::post('/rejestracja/krok3', 'auth\RegisterController@krok3_request')->name('rejestracja_krok3_request');

    Route::get('/rejestracja/krok4', 'auth\RegisterController@krok4')->name('rejestracja_krok4');
    Route::post('/rejestracja/krok4', 'auth\RegisterController@krok4_request')->name('rejestracja_krok4_request');

    Route::get('/rejestracja/krok5', 'auth\RegisterController@krok5')->name('rejestracja_krok5');
    Route::post('/rejestracja/krok5', 'auth\RegisterController@krok5_request')->name('rejestracja_krok5_request');

    /**
     * Frontend > Cms
     */

    Route::get('cms/{id}/{title}', 'frontend\CmsController@show')->name('cms_show');

   

    /**
     * Frontend > Panel
     */


 

//    Route::middleware('web')->domain('ok.'.env('SITE_URL'))->group(function() {
//      echo "kokok";
//      exit();
//     // Route::get('/', function($account) {
//     //     //return "oko";
        
//     // });
   
//     // Route('/users/{id}', function($account, $id) {
//     //     //
//     // });
    // });

    Route::get('/brak-dostepu', 'Controller@redirect403')->name('redirect_403');
    Route::get('/nie-znaleziono', 'Controller@redirect404')->name('redirect_404');

    Route::group(['middleware' => 'auth'], function () {
        Route::get('/panel', 'frontend\PanelController@index')->name('panel_index');
        Route::put('/panel', 'frontend\PanelController@update')->name('panel_update');


        Route::get('/godziny-otwarcia', 'frontend\PanelController@opening')->name('opening_index');
        Route::put('/godziny-otwarcia', 'frontend\PanelController@opening_update')->name('opening_update');

        Route::get('/uslugi', 'frontend\PanelController@services')->name('services_index');
        Route::put('/uslugi', 'frontend\PanelController@services_update')->name('services_update');
    });

    Route::group(['middleware' => 'admin'], function () {
        /**
         * Backend
         */
        Route::get('/backoffice', 'backend\BackofficeController@index')->name('backend_index');

        /**
         *  Backend > opening
         */

                 Route::get('/backoffice/ekspert/{id_ekspert}/godziny-otwarcia', 'backend\EksperciController@opening')->name('backend_opening_index');
        Route::put('/backoffice/ekspert/{id_ekspert}/godziny-otwarcia', 'backend\EksperciController@opening_update')->name('backend_opening_update');

        /**
         * Backend > cms
         */
        Route::get('/backoffice/cms', 'backend\CmsController@index')->name('backend_cms_index');
        Route::get('/backoffice/cms/create', 'backend\CmsController@create')->name('backend_cms_create');
        Route::get('/backoffice/cms/{id}/edit', 'backend\CmsController@edit')->name('backend_cms_edit');
        Route::post('/backoffice/cms', 'backend\CmsController@store')->name('backend_cms_store');
        Route::put('/backoffice/cms/{id}/edit', 'backend\CmsController@update')->name('backend_cms_update');
        Route::delete('/backoffice/cms/{id}', 'backend\CmsController@delete')->name('backend_cms_delete');


        /**
          * Backend > blog
          */
        Route::get('/backoffice/blog', 'backend\BlogController@index')->name('backend_blog_index');
        Route::get('/backoffice/blog/create', 'backend\BlogController@create')->name('backend_blog_create');
        Route::get('/backoffice/blog/{id}/edit', 'backend\BlogController@edit')->name('backend_blog_edit');
        Route::post('/backoffice/blog', 'backend\BlogController@store')->name('backend_blog_store');
        Route::put('/backoffice/blog/{id}/edit', 'backend\BlogController@update')->name('backend_blog_update');
        Route::delete('/backoffice/blog/{id}', 'backend\BlogController@destroy')->name('backend_blog_delete');

    
        /**
         * Backend > opinie
         */

        Route::get('/backoffice/opinie', 'backend\ReviewController@index')->name('backend_opinie_index');
        Route::get('/backoffice/opinie/create', 'backend\ReviewController@create')->name('backend_opinie_create');
        Route::get('/backoffice/opinie/{id}/edit', 'backend\ReviewController@edit')->name('backend_opinie_edit');
        Route::post('/backoffice/opinie', 'backend\ReviewController@store')->name('backend_opinie_store');
        Route::put('/backoffice/opinie/{id}/edit', 'backend\ReviewController@update')->name('backend_opinie_update');
        Route::delete('/backoffice/opinie/{id}', 'backend\ReviewController@delete')->name('backend_opinie_delete');

        /**
         * Backend > eksperci
         */
        Route::post('/backoffice/eksperci/filtr', 'backend\EksperciController@indexFiltr')->name('backend_eksperci_index_filtr');

        Route::get('/backoffice/eksperci', 'backend\EksperciController@index')->name('backend_eksperci_index');
        Route::get('/backoffice/eksperci/create', 'backend\EksperciController@create')->name('backend_eksperci_create');
        Route::get('/backoffice/eksperci/{id}/edit', 'backend\EksperciController@edit')->name('backend_eksperci_edit');
        Route::post('/backoffice/eksperci', 'backend\EksperciController@store')->name('backend_eksperci_store');
        Route::put('/backoffice/eksperci/{id}/edit', 'backend\EksperciController@update')->name('backend_eksperci_update');
        Route::delete('/backoffice/eksperci/{id}', 'backend\EksperciController@destroy')->name('backend_eksperci_delete');


        /**
        * Backend > uzytkownicy
        */

        Route::post('/backoffice/uzytkownicy/filtr', 'backend\UzytkownicyController@indexFiltr')->name('backend_user_index_filtr');

        Route::get('/backoffice/uzytkownicy', 'backend\UzytkownicyController@index')->name('backend_uzytkownicy_index');
        Route::get('/backoffice/uzytkownicy/create', 'backend\UzytkownicyController@create')->name('backend_uzytkownicy_create');
        Route::get('/backoffice/uzytkownicy/{id}/edit', 'backend\UzytkownicyController@edit')->name('backend_uzytkownicy_edit');
        Route::post('/backoffice/uzytkownicy', 'backend\UzytkownicyController@store')->name('backend_uzytkownicy_store');
        Route::put('/backoffice/uzytkownicy/{id}/edit', 'backend\UzytkownicyController@update')->name('backend_uzytkownicy_update');
        Route::delete('/backoffice/uzytkownicy/{id}', 'backend\UzytkownicyController@delete')->name('backend_uzytkownicy_delete');


        /**
         * Backend > miasta
         */

         Route::post('/backoffice/miasta/filtr', 'backend\MiastaController@indexFiltr')->name('backend_miasta_index_filtr');
        
        Route::get('/backoffice/miasta', 'backend\MiastaController@index')->name('backend_miasta_index');
        Route::get('/backoffice/miasta/create', 'backend\MiastaController@create')->name('backend_miasta_create');
        Route::get('/backoffice/miasta/{id}/edit', 'backend\MiastaController@edit')->name('backend_miasta_edit');
        Route::post('/backoffice/miasta', 'backend\MiastaController@store')->name('backend_miasta_store');
        Route::put('/backoffice/miasta/{id}/edit', 'backend\MiastaController@update')->name('backend_miasta_update');
        Route::delete('/backoffice/miasta/{id}', 'backend\MiastaController@delete')->name('backend_miasta_delete');

        /**
        * Backend > wojewodztwa
        */

        Route::get('/backoffice/wojewodztwa', 'backend\WojewodztwaController@index')->name('backend_wojewodztwa_index');
        Route::get('/backoffice/wojewodztwa/create', 'backend\WojewodztwaController@create')->name('backend_wojewodztwa_create');
        Route::get('/backoffice/wojewodztwa/{id}/edit', 'backend\WojewodztwaController@edit')->name('backend_wojewodztwa_edit');
        Route::post('/backoffice/wojewodztwa', 'backend\WojewodztwaController@store')->name('backend_wojewodztwa_store');
        Route::put('/backoffice/wojewodztwa/{id}/edit', 'backend\WojewodztwaController@update')->name('backend_wojewodztwa_update');
        Route::delete('/backoffice/wojewodztwa/{id}', 'backend\WojewodztwaController@destroy')->name('backend_wojewodztwa_delete');


      /**
        * Backend > kategorie
        */

        Route::get('/backoffice/kategorie', 'backend\KategorieController@index')->name('backend_kategorie_index');
        Route::get('/backoffice/kategorie/create', 'backend\KategorieController@create')->name('backend_kategorie_create');
        Route::get('/backoffice/kategorie/{id}/edit', 'backend\KategorieController@edit')->name('backend_kategorie_edit');
        Route::post('/backoffice/kategorie', 'backend\KategorieController@store')->name('backend_kategorie_store');
        Route::put('/backoffice/kategorie/{id}/edit', 'backend\KategorieController@update')->name('backend_kategorie_update');
        Route::delete('/backoffice/kategorie/{id}', 'backend\KategorieController@destroy')->name('backend_kategorie_delete');


      /**
        * Backend > usługi
        */

        Route::get('/backoffice/uslugi', 'backend\UslugiController@index')->name('backend_uslugi_index');
        Route::get('/backoffice/uslugi/create', 'backend\UslugiController@create')->name('backend_uslugi_create');
        Route::get('/backoffice/uslugi/{id}/edit', 'backend\UslugiController@edit')->name('backend_uslugi_edit');
        Route::post('/backoffice/uslugi', 'backend\UslugiController@store')->name('backend_uslugi_store');
        Route::put('/backoffice/uslugi/{id}/edit', 'backend\UslugiController@update')->name('backend_uslugi_update');
        Route::delete('/backoffice/uslugi/{id}', 'backend\UslugiController@destroy')->name('backend_uslugi_delete');

        /**
         * Backend > kraje
         */

        Route::get('/backoffice/kraje', 'backend\KrajeController@index')->name('backend_kraje_index');
        Route::get('/backoffice/kraje/create', 'backend\KrajeController@create')->name('backend_kraje_create');
        Route::get('/backoffice/kraje/{id}/edit', 'backend\KrajeController@edit')->name('backend_kraje_edit');
        Route::post('/backoffice/kraje', 'backend\KrajeController@store')->name('backend_kraje_store');
        Route::put('/backoffice/kraje/{id}/edit', 'backend\KrajeController@update')->name('backend_kraje_update');
        Route::delete('/backoffice/kraje/{id}', 'backend\KrajeController@delete')->name('backend_kraje_delete');


        Route::get('/backoffice/ustawienia', 'backend\BackofficeController@index')->name('backend_ustawienia_index');


         
    });

   /**
     * Frontend > Subdomain
     */
    //Route::domain(''.env('SITE_URL').'')->group(function () {
      //Route::domain(''.env('SITE_URL').'/{account}')->group(function () {
      
       
});