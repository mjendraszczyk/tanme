<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opening extends Model
{
     protected $primaryKey = 'id_opening';
      protected $table = 'opening';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'dzien' ,'godzina_od', 'godzina_do', 'id_user'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
