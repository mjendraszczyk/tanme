<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
     protected $primaryKey = 'id_city';
      protected $table = 'city';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'image', 'id_province','content','position'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
