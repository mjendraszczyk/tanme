<?php

namespace App;
use Eloquent;
use Illuminate\Database\Eloquent\Model;

class Ekspert extends Model
{
      protected $primaryKey = 'id_ekspert';
      protected $table = 'ekspert';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'content', 'feature_image','cert','phone','typ','address','city','province','expert_email', 'feature','id_user','facebook','instagram','youtube', 'lat', 'lon', 'short_description'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

     public function reviews()
    {
        return $this->hasMany('App\Review', 'id_ekspert');
    }

    
}
