<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galeria extends Model
{
    protected $primaryKey = 'id_galeria';
    public $incrementing = true;
    protected $table = 'galeria';
     protected $fillable = [
        'id_galeria',
        'position',
        'label',
        'filename',
        'feature',
        'id_user'
    ];
}
