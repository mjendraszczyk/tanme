<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
      protected $primaryKey = 'id_review';
      protected $table = 'review';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'comment', 'rate','nick', 'id_ekspert', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

       public function ekspert()
    {
        return $this->belongsTo('App\Ekspert');
    }
}
