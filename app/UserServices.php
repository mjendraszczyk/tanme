<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserServices extends Model
{
    protected $primaryKey = 'id_user_service';
    protected $table = 'user_services';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_ekspert',
        'id_service',
        'price'
    ];
}
