<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RejestracjaKrok2Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'telefon' => 'required',
            'adres' => 'required',
            'typ' => 'required',
            'lat' => 'required',
            'lon' => 'required',
            'id_province' => 'required',
            'miasto' => 'required'
        ];
    }
}
