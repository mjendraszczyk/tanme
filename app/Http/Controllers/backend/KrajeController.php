<?php

namespace App\Http\Controllers\backend;

use App\Country;
use App\Province;
use Session;

use Illuminate\Http\Request;
use App\Http\Requests\KrajeRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class KrajeController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kraje = Country::paginate($this->limit);
        return view('backend.kraje.index')->with('items',$kraje);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kraje = Country::get();

        return view('backend.kraje.create')->with('kraje',$kraje);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KrajeRequest $request)
    {
        $kraje = new Country();
        $kraje->name = $request->input('name');
        $kraje->save();
        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kraje = Country::where('id_country', $id)->first();

        return view('backend.kraje.edit')->with('kraje', $kraje);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KrajeRequest $request, $id)
    {
        $kraje = Country::where('id_country', $id);
        $kraje->update([
        'name' => $request->input('name'),
        ]);
        Session::flash('status', 'Zapisano pomyślnie.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $kraje = Country::where('id_country', $id);
        $kraje->delete();
        return redirect()->back();
    }
  
}
