<?php

namespace App\Http\Controllers\backend;

use App\Cms;
use Session;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\CmsRequest;

use Illuminate\Support\Facades\DB;


class CmsController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cms = Cms::paginate($this->limit);
        return view('backend.cms.index')->with('items',$cms);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.cms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CmsRequest $request)
    {

        $cms = new Cms();
        $cms->insert([
          'title' => $request->get('title'),
          'content' => $request->get('content'),
          //'stan' => $request->get('stan'),
        ]);
        Session::flash('status', 'Dodano pomyślnie.');


        return redirect()->route('backend_cms_create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cms = Cms::where('id_cms', $id)->first();

        return view('backend.cms.edit')->with('cms', $cms);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CmsRequest $request, $id)
    {
        $cms = Cms::where('id_cms', $id);
        $cms->update([
        'title' => $request->get('title'),
        'content' => $request->get('content'),
        //'stan' => $request->get('stan'),
        ]);

        Session::flash('status', 'Zapisano pomyślnie.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cms = Cms::where('id_cms', $id);
        $cms->delete();
        return redirect()->back();
    }
    
}
