<?php

namespace App\Http\Controllers\backend;

use App\Category;
use App\Service;
use Session;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\backend\FaqRequest;

use Illuminate\Support\Facades\DB;


class UslugiController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::paginate($this->limit);
        return view('backend.uslugi.index')->with('items',$services);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $categories = Category::get();
        return view('backend.uslugi.create')->with('categories',$categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $services = new Service();
        $services->insert([
          'name' => $request->get('name'),
          'id_category' => $request->get('id_category'),
        ]);
        Session::flash('status', 'Dodano pomyślnie.');


        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $services = Service::where('id_service', $id)->first();
        $categories = Category::get();
        return view('backend.uslugi.edit')->with('services', $services)->with('categories',$categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $services = Service::where('id_category', $id);
        $services->update([
        'name' => $request->get('name'),
        'id_category' => $request->get('id_category'),
        ]);

        Session::flash('status', 'Zapisano pomyślnie.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $services = Service::where('id_service', $id);
        $services->delete();
        return redirect()->back();
    }
}
