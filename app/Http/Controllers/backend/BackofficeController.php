<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog;
use App\Review;
use App\User;
use App\Ekspert;

class BackofficeController extends Controller
{
    public function index() { 
        $countBlog = Blog::count();
        $countUser = User::count();
        $countExpert = Ekspert::count();
        $countReview = Review::count();
        return view('backend.backoffice.index')->with('countBlog',$countBlog)->with('countUser',$countUser)->with('countEkspert',$countExpert)->with('countReview',$countReview);
    }
    public function redirect_403() { 
        return view('backend._main.403');
    }
}
