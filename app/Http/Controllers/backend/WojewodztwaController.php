<?php

namespace App\Http\Controllers\backend;

use App\City;

use Illuminate\Http\Request;
use App\Http\Requests\WojewodztwaRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Country;
use Session;
use App\Province;

class WojewodztwaController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wojewodztwa = Province::paginate($this->limit);
        return view('backend.wojewodztwa.index')->with('items',$wojewodztwa);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.wojewodztwa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WojewodztwaRequest $request)
    {
    
        $wojewodztwa = new Province();
        $wojewodztwa->name = $request->input('name');
        $wojewodztwa->content =  $request->get('content');
        $wojewodztwa->id_country = $request->input('id_country');
        $wojewodztwa->save();
        Session::flash('status', 'Zapisano pomyślnie.');

        
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $wojewodztwa = Province::where('id_province', $id)->first();
        $kraje = Country::get();
        return view('backend.wojewodztwa.edit')->with('wojewodztwa', $wojewodztwa)->with('kraje',$kraje);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WojewodztwaRequest $request, $id)
    {
        $wojewodztwa = Province::where('id_province', $id);
        $wojewodztwa->update([
        'name' => $request->input('name'),
        'content' => $request->input('content')->nullable(),
        'id_country' => $request->input('id_country'),
        ]);
        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $wojewodztwa = Province::where('id_province', $id);
        $wojewodztwa->delete();
        return redirect()->back();
    }
}
