<?php

namespace App\Http\Controllers\backend;

use App\Ekspert;
use App\User;
use App\Opening;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EksperciController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
        $eksperci = Ekspert::paginate($this->limit);
        return view('backend.eksperci.index')->with('items',$eksperci);
    }

    public function indexFiltr(Request $request) {
        $eksperci = Ekspert::where('name','LIKE','%'.$request->input('nazwa').'%')->paginate($this->limit);
        return view('backend.eksperci.index')->with('items',$eksperci);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::get();
        $ekspert = Ekspert::get();
        return view('backend.eksperci.create')->with('ekspert',$ekspert)->with('users',$users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $eksperci = new Ekspert();
        $eksperci->name = $request->input('name');
        $eksperci->content = $request->input('email');
        $eksperci->feature_image = $request->input('feature_image');
        $eksperci->cert = $request->input('cert');
        $eksperci->phone = $request->input('phone');
        $eksperci->typ = $request->input('typ');
        $eksperci->address = $request->input('address');
        $eksperci->city = $request->input('city');
        $eksperci->province = $request->input('province');
        $eksperci->expert_mail = $request->input('expert_mail');
        $eksperci->feature = $request->input('feature');
        $eksperci->id_user = $request->input('id_user');
        $eksperci->facebook = $request->input('facebook');
        $eksperci->instagram = $request->input('instagram');
        $eksperci->youtube = $request->input('youtube');
        
        $eksperci->save();
        Session::flash('status', 'Zapisano pomyślnie.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
            $users = User::get();
        $ekspert = Ekspert::where('id_ekspert',$id)->first();
        return view('backend.eksperci.edit')->with('ekspert',$ekspert)->with('users',$users);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
// dd($request);
// echo $request->input('expert_mail');
// exit();
                // 'name', 'content', 'feature_image','cert','phone','typ','address','city','province','expert_email', 'feature','id_user','facebook','instagram','youtube', 'lat', 'lon'
         $ekspert = Ekspert::where('id_user', $id);
        
            $ekspert->update([
            'name' => $request->input('name'),
            'expert_email' => $request->input('expert_mail'),
            'content' => $request->input('opis'),
            'province' => $request->input('id_province'),
            'city' => $request->input('miasto'),
            'address' =>
            $request->input('address'),
            'phone' =>
            $request->input('phone'),
            'facebook' =>
            $request->input('facebook'),
            'instagram' =>
            $request->input('instagram'),
            'youtube' =>
            $request->input('youtube'),
            'typ' =>
            $request->input('typ'),
            'feature' =>
            $request->input('feature'),
            'lat' =>
            $request->input('lat'),
            'lon' =>
            $request->input('lon'),
            'short_description' =>
            $request->input('short_description'),
            ]);

            $getUser = User::where('id',$id);

            $getUser->update([
                'name' => $request->input('name')
            ]);

        //     if ((Input::file('ekspert_upload'))) {
        //         $upload = new Controller();
        //     $upload->upload('ekspert_upload', 'jpg,jpeg,png', '/img/ekspert/', $request->get('ekspert_upload'), $request, 800, 360);

        //     $ekspert->update([
        //         'feature_image' => $upload->getFileName()
        //         ]);
        // }
      
    
        

            // $getUser->update([
            //         'business' => $request->input('business'),
            // ]);

                return redirect()->route('backend_eksperci_edit',['id'=>$id]);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $eksperci = Ekspert::where('id_ekspert', $id);
        $eksperci->delete();

        return redirect()->back();
    }

    public function opening($id_ekspert) {
        $opening = Opening::where('id_user', $id_ekspert)->get()->toArray();

        $countOpening = Opening::where('id_user', $id_ekspert)->count();
        
        $getUser = Ekspert::where('id_user', $id_ekspert)->first();
        $countEkspert = Ekspert::where('id_user', $id_ekspert)->count();
        return view('backend.eksperci.opening')->with('ekspert', $getUser)->with('opening',$opening)->with('countEkspert',$countEkspert)->with('countOpening',$countOpening);
    }
    public function opening_update(Request $request, $id_ekspert) {
 
        $getEkspert = Ekspert::where('id_user', $id_ekspert)->first();
        $getOpening = Opening::where('id_user',  $id_ekspert)->count();
        if($getOpening == 0) {
        foreach($request->input('godzina_od') as $key => $godzina_od) {

            $opening = new Opening();
            $opening->id_user = $getEkspert->id_ekspert;
            $opening->dzien = $key+1;
            $opening->godzina_od = $godzina_od;
            $opening->godzina_do = $request->input('godzina_do')[$key];
            $opening->save();
        }

        } else {
            $opening = Opening::where('id_user',  $id_ekspert)->get();


            $getRequestKrok2GodzinyOd = array();
            $getRequestKrok2GodzinyDo = array();

            foreach ($request->input('godzina_od') as $key => $godzina_od) {
                $getRequestKrok2GodzinyOd[] = $godzina_od;
                $opening =  Opening::where('id_user',  $id_ekspert)->where('dzien', ($key+1))->first();
                $opening->update([
                'godzina_od' => $godzina_od,
                'godzina_do' => $request->input('godzina_do')[$key]
            ]);
            }
        }
     
            return redirect()->route('backend_eksperci_edit',['id'=> $id_ekspert]);
    }
}
