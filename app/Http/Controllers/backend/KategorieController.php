<?php

namespace App\Http\Controllers\backend;

use App\Category;
use Session;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\backend\FaqRequest;

use Illuminate\Support\Facades\DB;


class KategorieController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::paginate($this->limit);
        return view('backend.kategorie.index')->with('items',$categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.kategorie.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $categories = new Category();
        $categories->insert([
          'name' => $request->get('name'),
        ]);
        Session::flash('status', 'Dodano pomyślnie.');


        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::where('id_category', $id)->first();

        return view('backend.kategorie.edit')->with('categories', $categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categories = Category::where('id_category', $id);
        $categories->update([
        'name' => $request->get('name'),
        ]);

        Session::flash('status', 'Zapisano pomyślnie.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories = Category::where('id_category', $id);
        $categories->delete();
        return redirect()->back();
    }
}
