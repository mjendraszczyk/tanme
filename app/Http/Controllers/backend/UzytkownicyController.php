<?php

namespace App\Http\Controllers\backend;

use App\User;

use App\Wojewodztwo;
use Session;
use Validator;
use App\Ekspert;
use App\Role;

use Illuminate\Support\Facades\Mail;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UzytkownicyRequest;
use Illuminate\Support\Facades\DB;


class UzytkownicyController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $uzytkownicy = User::paginate($this->limit);
        return view('backend.uzytkownicy.index')->with('items',$uzytkownicy);
    }

    public function indexFiltr(Request $request) {
        $uzytkownicy = User::where('name','LIKE','%'.$request->input('nazwa').'%')->paginate($this->limit);
        return view('backend.uzytkownicy.index')->with('items',$uzytkownicy);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ekspert = Ekspert::get();
        return view('backend.uzytkownicy.create')->with('ekspert',$ekspert);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UzytkownicyRequest $request)
    {
        $uzytkownicy = new User();
        $uzytkownicy->name = $request->input('name');
        $uzytkownicy->email = $request->input('email');
        // $uzytkownicy->stan = $request->input('stan');
        $uzytkownicy->password = bcrypt($request->input('password'));
        // $uzytkownicy->telefon = $request->input('telefon');
        $uzytkownicy->id_role = $request->input('id_role');
        
        $uzytkownicy->save();
        Session::flash('status', 'Zapisano pomyślnie.');

 
        // 'o_mnie','id_specjalizacje','id_user','lat','lon','szukam', 'id_miasta'
        $email = trim($uzytkownicy->email);
        $konto = array(
                'name' =>  $uzytkownicy->name,
                'email' => $email,
                'stan' => '0'
            );
            
        $temat = 'Utworzenie konta w serwisie '.env('APP_NAME');
                $data = array('temat'=>$temat, 'email'=>$email, 'konto'=>$konto);
                 
            //       $getUstawienia = Ustawienia::where('instancja','mails')->first();
            // if ($getUstawienia->wartosc == '1') {
            //     Mail::send('mails.rejestracja', $data, function ($message) use ($temat, $email) {
            //         $message->from(env('MAIL_USERNAME'), $temat);
            //         $message->to($email)->subject($temat);
            //     });
            // }

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $uzytkownicy = User::where('id', $id)->first();
 
        return view('backend.uzytkownicy.edit')->with('uzytkownicy', $uzytkownicy);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UzytkownicyRequest $request, $id)
    {
        $uzytkownicy = User::where('id', $id);
        if(!empty($request->input('password'))) {

            if($request->input('password') == $request->input('password_repeat')) {
            $uzytkownicy->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'business' => $request->input('business'),
            'id_role' => $request->input('id_role'),
 
            ]);
            Session::flash('status', 'Zapisano pomyślnie.');
 

            } else {
              Session::flash('status_fail', 'Hasła do siebie nie pasują');
            }
        } else {
            $uzytkownicy->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'business' => $request->input('business'),
            'id_role' => $request->input('id_role'),
            ]);
 
             Session::flash('status', 'Zapisano pomyślnie.');
        }
           

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $uzytkownicy = User::where('id', $id);
        $uzytkownicy->delete();

        $profil = Profil::where('id_user',$id);
        $profil->delete();

        $ogloszenia = Ogloszenie::where('id_user',$id);
        $ogloszenia->delete();

        $edukacja = Edukacja::where('id_user',$id);
        $edukacja->delete();

        $doswiadczenie = Doswiadczenie::where('id_user',$id);
        $doswiadczenie->delete();

        return redirect()->back();
    }
    public function filter(Request $request) {//Request $request
        
        return $this->filterCore($request, (new User()));
    }
}
