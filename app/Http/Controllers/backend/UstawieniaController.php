<?php

namespace App\Http\Controllers\backend;

use App\Ustawienia;
use App\Specjalizacja;
use App\Wojewodztwo;
use Session;
use Artisan;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\backend\UstawieniaRequest;

use Illuminate\Support\Facades\DB;


class UstawieniaController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Ustawienia = Ustawienia::paginate($this->limit);
        return view('backend.ustawienia.index')->with('items',$Ustawienia);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.ustawienia.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UstawieniaRequest $request)
    {

        $Ustawienia = new Ustawienia();
        $Ustawienia->insert([
          'intancja' => $request->get('instancja'),
          'wartosc' => $request->get('wartosc'),
        ]);
        Session::flash('status', 'Dodano pomyślnie.');


        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Ustawienia = Ustawienia::where('id_ustawienia', $id)->first();

        return view('backend.ustawienia.edit')->with('ustawienia', $Ustawienia);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UstawieniaRequest $request, $id)
    {
        
        $Ustawienia = Ustawienia::where('id_ustawienia', $id);
        $Ustawienia->update([
        //'instancja' => $request->get('instancja'),
        'wartosc' => $request->get('wartosc'),
        ]);

        $getUstawienie = Ustawienia::where('instancja','maintenance')->first();

        if($getUstawienie->wartosc == '1') {
            Artisan::call('down');
        } 
        if($getUstawienie->wartosc == '0') {
            Artisan::call('up');
        } 
        Session::flash('status', 'Zapisano pomyślnie.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     $Ustawienia = Ustawienia::where('id_ustawienia', $id);
    //     $Ustawienia->delete();
    //     return redirect()->back();
    // }
    public function filter(Request $request) {//Request $request
        
        return $this->filterCore($request, (new Ustawienia()));
    }
}
