<?php

namespace App\Http\Controllers\backend;

use App\Ogloszenie;
use App\Podmiot;
use App\Specjalizacja;
use App\Wojewodztwo;
use Session;
use App\User;

use App\DodatkoweAdresy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\OgloszeniaRequest;
use Illuminate\Support\Facades\DB;


class OgloszeniaController extends Controller
{
    public $getDodatkoweAdresy;
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ogloszenia = Ogloszenie::paginate($this->limit);
        return view('backend.ogloszenia.index')->with('items',$ogloszenia);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getSpecjalizacje = (new Specjalizacja())->get();
        $getWojewodztwa = (new Wojewodztwo())->get();

        return view('backend.ogloszenia.create')->with('specjalizacje', $getSpecjalizacje)->with('wojewodztwa', $getWojewodztwa);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OgloszeniaRequest $request)
    {
        $ogloszenia = new Ogloszenie();
        $getUser = User::where('id_podmiot', $request->input('id_podmiot'))->first();
        $ogloszenia->imie_nazwisko = $request->input('imie_nazwisko');
        $ogloszenia->email = $request->input('email');
        $ogloszenia->id_user = $getUser->id;///$request->input('id_user');
        $ogloszenia->id_specjalizacje = $request->input('id_specjalizacje');
        $ogloszenia->id_wymiar_pracy = $request->input('id_wymiar_pracy');
        $ogloszenia->doswiadczenie_od = $request->input('doswiadczenie_od');
        $ogloszenia->doswiadczenie_do = $request->input('doswiadczenie_do');
        $ogloszenia->id_poziom = $request->input('id_poziom');
        $ogloszenia->id_rodzaj_umowy = $request->input('id_rodzaj_umowy');
        $ogloszenia->wynagrodzenie_od = $request->input('wynagrodzenie_od');
        $ogloszenia->wynagrodzenie_do = $request->input('wynagrodzenie_do');
        $ogloszenia->typ_wynagrodzenia = $request->input('typ_wynagrodzenia');
        $ogloszenia->waluta_wynagrodzenia = $request->input('waluta_wynagrodzenia');
        $ogloszenia->tresc = $request->input('tresc');
        $ogloszenia->id_podmiot = $request->input('id_podmiot');
        $ogloszenia->save();

        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ogloszenia = Ogloszenie::where('id_ogloszenia', $id)->first();
        $getWojewodztwa = (new Wojewodztwo())->get();

        $this->getDodatkoweAdresy = DodatkoweAdresy::where('id_ogloszenia', $id)->get();
        return view('backend.ogloszenia.edit')->with('ogloszenia', $ogloszenia)->with('wojewodztwa', $getWojewodztwa)->with('dodatkowe_adresy', $this->getDodatkoweAdresy);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OgloszeniaRequest $request, $id)
    {
        $ogloszenia = Ogloszenie::where('id_ogloszenia', $id);
        // 'imie_nazwisko', 'email', 'id_user', 'id_specjalizacje', 'id_wymiar_pracy', 'id_doswiadczenie', 'wynagrodzenie', 'tresc', 'podmiot', 'id_podmiot'
        $ogloszenia->update([
        'imie_nazwisko' => $request->input('imie_nazwisko'),
        'email' => $request->input('email'),
        'id_user' => $request->input('id_user'),
        'id_specjalizacje' => $request->input('id_specjalizacje'),
        'id_wymiar_pracy' => $request->input('id_wymiar_pracy'),
        'doswiadczenie_od' => $request->input('doswiadczenie_od'),
        'doswiadczenie_do' => $request->input('doswiadczenie_do'),
        'id_poziom' => $request->input('id_poziom'),
        'id_rodzaj_umowy' => $request->input('id_rodzaj_umowy'),
        'wynagrodzenie_od' => $request->input('wynagrodzenie_od'),
        'wynagrodzenie_do' => $request->input('wynagrodzenie_do'),
        'typ_wynagrodzenia' => $request->input('typ_wynagrodzenia'),
        'waluta_wynagrodzenia' => $request->input('waluta_wynagrodzenia'),
        'tresc' => $request->input('tresc'),
        'id_podmiot' => $request->input('id_podmiot'),
        ]);
            Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ogloszenia = Ogloszenie::where('id_ogloszenia', $id);
        $ogloszenia->delete();
        return redirect()->back();
    }
    public function filter(Request $request) {//Request $request
 
        return $this->filterCore($request, (new Ogloszenie()));
    
    }
}
