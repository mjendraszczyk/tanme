<?php

namespace App\Http\Controllers\backend;

use App\Review;
use App\Ekspert;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReviewRequest;
use Session;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $review = Review::paginate($this->limit);
        $eksperci = Ekspert::get();
        return view('backend.review.index')->with('items',$review)->with('eksperci',$eksperci);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $eksperci = Ekspert::get();
          return view('backend.review.create')->with('eksperci',$eksperci);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(ReviewRequest $request)
    {
//  'id_user', 'comment', 'rate','nick', 'id_ekspert'
if(!empty($request->input('status'))) {
$status = '1';
    } else {
        $status = '0';
    }
        $review = new Review();
        $review->insert([
          'nick' => $request->get('nick'),
          'id_ekspert' => $request->get('id_ekspert'),
          'rate' => $request->get('rate'),
          'comment' => $request->get('comment'),
          'status' => $request->get('status'),
        ]);
        Session::flash('status', 'Dodano pomyślnie.');


        return redirect()->route('backend_opinie_create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $eksperci = Ekspert::get();
        $review = Review::where('id_review', $id)->first();

        return view('backend.review.edit')->with('review', $review)->with('eksperci',$eksperci);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ReviewRequest $request, $id)
    {
        $review = Review::where('id_review', $id);
        $review->update([
          'nick' => $request->get('nick'),
          'id_ekspert' => $request->get('id_ekspert'),
          'rate' => $request->get('rate'),
          'comment' => $request->get('comment'),
            'status' => $request->get('status'),
        ]);

        Session::flash('status', 'Zapisano pomyślnie.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $review = Review::where('id_review', $id);
        $review->delete();
        return redirect()->back();
    }
    
}
