<?php

namespace App\Http\Controllers\backend;

use App\City;

use Illuminate\Http\Request;
use App\Http\Requests\MiastaRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Session;
use App\Province;
use mrcnpdlk\Teryt\Client;
use mrcnpdlk\Teryt\NativeApi;

class MiastaController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $miasta = City::paginate($this->limit);
        return view('backend.miasta.index')->with('items',$miasta);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $wojewodztwo = Province::get();

        return view('backend.miasta.create')->with('wojewodztwa',$wojewodztwo);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MiastaRequest $request)
    {
        $miasta = new City();
        $miasta->nazwa = $request->input('name');
        $miasta->content = $request->input('content')->nullable();
        $miasta->id_province = $request->input('id_province');
        $miasta->save();
        Session::flash('status', 'Zapisano pomyślnie.');

        return redirect()->back();
    }

    public function indexFiltr(Request $request) {
        // dd($request);
        $miasta = City::where('name','LIKE','%'.$request->input('nazwa').'%')->paginate($this->limit);
        return view('backend.miasta.index')->with('items',$miasta);

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $miasta = City::where('id_city', $id)->first();

        $wojewodztwo = Province::get();

        return view('backend.miasta.edit')->with('miasta', $miasta)->with('wojewodztwa', $wojewodztwo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MiastaRequest $request, $id)
    {
        $miasta = City::where('id_city', $id);
        $miasta->update([
        'name' => $request->input('name'),
        'content' => $request->input('content'),
        'id_province' => $request->input('id_province'),
        ]);
        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kraje = City::where('id_city', $id);
        $kraje->delete();
        return redirect()->back();
    }
   
    public function importLokalizacje() {
         $oClient = new Client();
    //$oClient->setConfig('mjendraszczyk', 'Di9!neTp78', true);
    $oClient->setConfig('mjendraszczyk', 'Di9!neTp78', true);
    $oNativeApi = NativeApi::create($oClient);
    $oNativeApi->CzyZalogowany();

    //var_dump($oNativeApi->PobierzListeWojewodztw());
    foreach ($oNativeApi->PobierzListeWojewodztw() as $wojewodztwo) {
      //  dd($wojewodztwo);
        $getWojewodztwoByName = "KUJAWSKO-POMORSKIE";
      
        if ($wojewodztwo->name == $getWojewodztwoByName) {
            /* Wojewodztwo */
            $checkIfExist = Wojewodztwo::where('nazwa', $wojewodztwo->name)->where('id_distinct', $wojewodztwo->provinceId)->count();

            if ($checkIfExist== 0) {
                $w = new Wojewodztwo();
                $w->nazwa = $wojewodztwo->name;
                $w->id_distinct = $wojewodztwo->provinceId;
                $w->id_kraje = '1';
                $w->save();
            } else {
                $w = Wojewodztwo::where('id_distinct', $wojewodztwo->provinceId)->first();
            }
            //dd($oNativeApi->PobierzListePowiatow($wojewodztwo->provinceId));
            $getPowiatDistrictId = "10";
            
                foreach ($oNativeApi->PobierzListePowiatow($wojewodztwo->provinceId) as $powiat) {
                    if ($powiat->districtId >= $getPowiatDistrictId) {
                    // dd($powiat);
                    // dd($oNativeApi->PobierzListeGmin($powiat->provinceId, $powiat->districtId));
                    //sleep(10);
                    foreach ($oNativeApi->PobierzListeGmin($powiat->provinceId, $powiat->districtId) as $gmina) {
                        // dd($gmina);
                        // if (($gmina->typeName != 'miasto') && ($gmina->typeName != 'gmina miejska')){
                        $checkIfExist = Miasto::where('nazwa', $gmina->name)->where('id_wojewodztwa', $w->id_wojewodztwa)->count();
                        if ($checkIfExist == 0) {
                            $g = new Miasto();
                            $g->nazwa = $gmina->name;
                            $g->id_wojewodztwa = $w->id_wojewodztwa;//$gmina->provinceId;
                            $g->save();
                        } else {
                            $g = Miasto::where('nazwa', $gmina->name)->first();
                        }
                        //dd($oNativeApi->PobierzListeMiejscowosciWGminie($powiat->provinceId, $powiat->districtId, $gmina->communeId));
                        try {
                            foreach ($oNativeApi->PobierzListeMiejscowosciWGminie($w->nazwa, $powiat->name, $g->nazwa) as $miasto) {
                                // dd($miasto);
                                $checkIfExist = Miasto::where('nazwa', $miasto->cityName)->where('id_wojewodztwa', $w->id_wojewodztwa)->count();
                                $checkIfExist2 = Miasto::where('nazwa', $miasto->cityName."(gmina: ".$g->nazwa.")")->where('id_wojewodztwa', $w->id_wojewodztwa)->count();//$w->id_wojewodztwa
                                //                       echo $miasto->cityName."+".$checkIfExist;
                                //                       echo "<br/>".$g->nazwa."+".$checkIfExist2." ".$w->id_wojewodztwa;
                                // exit();
                                //if ($w->id_wojewodztwa == $id_wojewodztwa) {
                                if (($checkIfExist == 0) && ($checkIfExist2 == 0)) {
                                    $m = new Miasto();
                                    $m->nazwa = $miasto->cityName."(gmina: ".$g->nazwa.")";
                                    $m->id_wojewodztwa = $w->id_wojewodztwa;//$gmina->provinceId;
                                    $m->save();
                                }
                                //}
                            }
                        } catch (Throwable $e) {
                            continue;
                        }
                   
                        //dd($oNativeApi->PobierzListeMiejscowosciWGminie("ZACHODNIOPOMORSKIE", "pyrzycki", "Pyrzyce"));
                // foreach ($oNativeApi->PobierzListeMiejscowosciWGminie($powiat->provinceId, $powiat->districtId) as $miejscowosc) {
                //     dd($miejscowosc);
                // }

                    //dd($gmina);
               // }
                    }
                }
            }
        }
    }
    }
}
