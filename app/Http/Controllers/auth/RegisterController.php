<?php

namespace App\Http\Controllers\auth;

use App\User;
use App\Ekspert;
use App\Opening;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests;
use App\Http\Requests\RejestracjaKrok1Request;
use App\Http\Requests\RejestracjaKrok2Request;
use App\Http\Requests\RejestracjaKrok3Request;
use App\Http\Requests\RejestracjaKrok4Request;
use App\Http\Requests\RejestracjaKrok5Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

use App\Service;
use App\Category;
use Illuminate\Support\Facades\Mail;

use Session;
use App\UserServices;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'id_role' => 1,
            'password' => Hash::make($data['password']),
        ]);
    }

    public function krok1() {
        return view('auth.krok1');
    }

    public function krok1_request(RejestracjaKrok1Request $request) { 

        // Tworzenie usera

        $getRequestKrok1Biznes = $request->input('business');
        $getRequestKrok1Imie = $request->input('business');
        $getRequestKrok1Email = $request->input('email');
        $getRequestKrok1Haslo = $request->input('haslo');
         
        $request->session()->put('form_biznes', $getRequestKrok1Biznes);
        $request->session()->put('form_imie', $getRequestKrok1Imie);
        $request->session()->put('form_email', $getRequestKrok1Email);
        $request->session()->put('form_haslo', $getRequestKrok1Haslo);


        $user = User::create([
            'name' => $getRequestKrok1Imie,
            'email' => $getRequestKrok1Email,
            'business' => $getRequestKrok1Biznes,
            'password' => Hash::make($getRequestKrok1Haslo),
            'id_role' => '1'
        ]);
        $ekspert = new Ekspert();
        $ekspert->id_user = $user->id;
        $ekspert->name = $getRequestKrok1Imie;
        $ekspert->save();
        
        Auth::login($user);
        


        $email = trim($getRequestKrok1Email);
        $konto = array(
                'name' =>  $getRequestKrok1Imie,
                'email' => $getRequestKrok1Email,
                'business' => $getRequestKrok1Biznes
            );
            
        $temat = 'Utworzenie konta w serwisie '.env('APP_NAME');
                $data = array('temat'=>'Założłeś konto w serwisie '.env('APP_NAME'), 'email'=>$email, 'konto'=>$konto);
                 
            // $getUstawienia = Ustawienia::where('instancja','mails')->first();
            // if ($getUstawienia->wartosc == '1') {
                // Mail::send('mails.rejestracja', $data, function ($message) use ($temat, $email) {
                //     $message->from(env('MAIL_USERNAME'), $temat);
                //     $message->to($email)->subject($temat);
                // });
            // }


        return redirect()->route('rejestracja_krok2');
    }

    public function krok2() {
        if (Auth::check()) {
            $id = Auth::user()->id;
            return view('auth.krok2')->with('id', ($id));
        } else {
            return redirect()->route('rejestracja_krok1');
        }
    }

     public function krok2_request(RejestracjaKrok2Request $request) { 
        
        if(count($request->input('typ')) == 2) {
            $typ = 3;
        } 
        else if(in_array('1', $request->input('typ'))) {
            $typ = 1;
        } else {
            $typ = 0;
        }
// dd($request);
        // Edycja usera
        $ekspert = Ekspert::where('id_user', Auth::user()->id)->first();
        $getRequestKrok2Telefon = $request->input('telefon');
        $getRequestKrok2Typ = $typ;
        $getRequestKrok2Adres = $request->input('adres');
        $getRequestKrok2Facebook = $request->input('facebook');
        $getRequestKrok2Instagram = $request->input('instagram');
        $getRequestKrok2Youtube = $request->input('youtube');
        $getRequestKrok2City  = $request->input('city');
         $getRequestKrok2Province  = $request->input('id_province');
         $getRequestKrok2Cert  = $request->input('cert');
         $getRequestKrok2City = $request->input('miasto');
         
         $getRequestKrok2Lat = $request->input('lat');
        $getRequestKrok2Lon = $request->input('lon');
         
        
        //$ekspert->name = Session::get('form_biznes');
        $ekspert->phone = $getRequestKrok2Telefon;
        $ekspert->typ = $getRequestKrok2Typ;
        $ekspert->address = $getRequestKrok2Adres;
        $ekspert->expert_email = Session::get('form_email');
        $ekspert->facebook = $getRequestKrok2Facebook;
        $ekspert->instagram = $getRequestKrok2Youtube;
        $ekspert->youtube = $getRequestKrok2Youtube;
        $ekspert->city = $getRequestKrok2City;
        $ekspert->province = $getRequestKrok2Province;
        $ekspert->cert = $getRequestKrok2Cert;
        $ekspert->city = $getRequestKrok2City;
        $ekspert->lat = $getRequestKrok2Lat;
        $ekspert->lon = $getRequestKrok2Lon;
        $ekspert->update();

        $getRequestKrok2GodzinyOd = array();
        $getRequestKrok2GodzinyDo = array();

            
        $checkOpening = Opening::where('id_user', Auth::user()->id)->count(); 

        if($checkOpening == 0) {
        foreach($request->input('godzina_od') as $key => $godzina_od) {
          
            $getRequestKrok2GodzinyOd[] = $godzina_od;
            $opening = new Opening();
            $opening->id_user = $ekspert->id_ekspert;
            $opening->dzien = $key+1;
            $opening->godzina_od = $godzina_od;
            $opening->godzina_do = $request->input('godzina_do')[$key];
            $opening->save();
        }
        foreach($request->input('godzina_do') as $godzina_do) {
            $getRequestKrok2GodzinyDo[] = $godzina_do;
        }
    }
        $request->session()->put('form_ekspert_id',  $ekspert->id_ekspert);
        $request->session()->put('form_godziny_od', $getRequestKrok2GodzinyOd);
        $request->session()->put('form_godziny_do', $getRequestKrok2GodzinyDo);

        


        $request->session()->put('form_telefon', $getRequestKrok2Telefon);
        $request->session()->put('form_typ', $getRequestKrok2Typ);
        $request->session()->put('form_adres', $getRequestKrok2Adres);
        $request->session()->put('form_facebook', $getRequestKrok2Facebook);
        $request->session()->put('form_instagram', $getRequestKrok2Instagram);
        $request->session()->put('form_youtube', $getRequestKrok2Youtube);

        return redirect()->route('rejestracja_krok3');
    }

    public function krok3() {
        if (Auth::check()) {
            $id = Auth::user()->id;
            return view('auth.krok3')->with('id', ($id));
        } else {
            return redirect()->route('rejestracja_krok1');
        }
    }

     public function krok3_request(RejestracjaKrok3Request $request) { 

        // Avatar usera
        //  $upload = new Controller();
        // if ((Input::file('filename'))) {
        //     $upload->upload('filename', 'jpg,jpeg,png', '/img/ekspert/', $request->get('filename'), $request, 800, 360);
    
        //     $request->session()->put('form_filename', $upload->getFileName());
        //     $ekspert = Ekspert::where('id_ekspert',Session::get('form_ekspert_id'))->first();
        //     $ekspert->feature_image = $upload->getFileName();
        // $ekspert->update();
        // }


        // Avatar usera
         $upload = new Controller();
         
        if ((Input::file('filename'))) {
           
            $upload->upload('filename', 'jpg,jpeg,png', '/img/uploads/galleries/', $request->get('filename'), $request, 800, 800);
            // dd("FD");
            // $ekspert = Ekspert::where('id_ekspert', Session::get('form_ekspert_id'))->first();
            if(!empty($request->input('croped'))) { 
                $uploadedAvatar = $request->input('croped');
            } else {
                $uploadedAvatar = $upload->getFileName();
            }
            
            $request->session()->put('form_filename', $uploadedAvatar);
            $ekspert = Ekspert::where('id_ekspert',Session::get('form_ekspert_id'))->first();
            $ekspert->feature_image = $uploadedAvatar;
            $ekspert->update();
        }

        return redirect()->route('rejestracja_krok4');
    }

    public function krok4() {
        if (Auth::check()) {
            $id = Auth::user()->id;
            $categories = Category::get();
            $services = Service::get();
            return view('auth.krok4')->with('id',$id)->with('categories',$categories)->with('services',$services);
        } else {
             return redirect()->route('rejestracja_krok1');
        }
    }

      public function krok4_request(RejestracjaKrok4Request $request) { 
        // dd($request);
        $getEkspert = Ekspert::where('id_user',Auth::user()->id)->first();
        $i=0;
        foreach($request->input('uslugi_ceny') as $key => $cena) {
            // if (!empty($cena)) {
                $userServices = new UserServices();
                $userServices->id_ekspert = $getEkspert->id_ekspert;
                $userServices->id_service = $request->input('uslugi_pozycja')[$i];
                $userServices->price = $cena;
                $userServices->save();
                $i++;
            // }
        }
        // Cennik usera
        // $getRequestKrok4UslugiNazwa = $request->input('uslugi_nazwa');
        // $getRequestKrok4UslugiCeny = $request->input('uslugi_ceny');$getRequestKrok4UslugiKategoria = $request->input('kategoria_nazwa');
      


        // $content_uslugi = '';

        //     foreach($getRequestKrok4UslugiNazwa as $key => $usluga){
        //     if($key == 0) {
        //         $content_uslugi .= '<table>';
        //     }
        //     $content_uslugi .= '<tr>';
        //     $content_uslugi .= '<td>'.$usluga.'</td>';
        //     $content_uslugi .= '<td>'.$getRequestKrok4UslugiCeny[$key].'</td>';
        //     $content_uslugi .= '</tr>';
        //     if($key == (count($getRequestKrok4UslugiNazwa) - 1)) {
        //         $content_uslugi .= '</table>';
        //     }
        // }

        // $ekspert = Ekspert::where('id_ekspert',Session::get('form_ekspert_id'))->first();
        // $ekspert->content = $content_uslugi;
        // $ekspert->update();
        
 
        // $request->session()->put('form_uslugi', $content_uslugi);
      
        return redirect()->route('rejestracja_krok5');
    }

    public function krok5() {
         if (Auth::check()) {
            $id = Auth::user()->id;
            return view('auth.krok5')->with('id',$id);
        } else {
             return redirect()->route('rejestracja_krok1');
        }
    }
      public function krok5_request(RejestracjaKrok5Request $request) { 

        // Opis usera
        $getRequestKrok5Opis = $request->input('short_description');

      
        $request->session()->put('form_short_description', $getRequestKrok5Opis);
      
        $ekspert = Ekspert::where('id_ekspert',Session::get('form_ekspert_id'))->first();
        $ekspert->update([
                'short_description' => $getRequestKrok5Opis
            ]);
        //$ekspert->update();

        ///return "DONE";
        return redirect()->route('panel_index');
    }
     
}
