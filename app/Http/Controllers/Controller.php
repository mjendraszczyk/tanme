<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use App\City;
use App\Province;
use App\Country;
use App\Review;
use App\Ekspert;
use Image;
use App\Role;
use App\Category;
use App\Service;
 
use App\Blog;
use App\Cms;
use App\UserServices;
use Illuminate\Http\Request;
use App\User;
use Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $limit;
    public function __construct() {
        $this->limit=50;
    }
    public function redirect403() {
        return view('errors.403');
    }

      public function cropUpload(Request $request) {
        $folderPath = public_path('img/ekspert/');


        $image_parts = explode(";base64,", $request->image);

        $image_type_aux = explode("image/", $image_parts[0]);

        $image_type = $image_type_aux[1];

        $image_base64 = base64_decode($image_parts[1]);

        $name = 'croped'.uniqid() . '.png';
        $file = $folderPath . $name;


        file_put_contents($file, $image_base64);


        return response()->json(['success'=>'success','path' => request()->getHttpHost().'/img/ekspert/'.$name, 'name' => $name]);

    }

      // SITEMAP
     public function sitemap() { 

        $routes_array = [route('home'),route('kontakt_index'),route('blog_index'),route('dla_ekspertek'),route('ekspert_index'),route('szkolenie_index'), route('login')];

        $getBlog = Blog::get();

        $getCms = Cms::get();

        $getEksperci = Ekspert::join('users', 'users.id', 'ekspert.id_user')->get();

        $getWojewodztwa = Province::get();
        $getMiasta = City::get();

        return response()->view('frontend.sitemap.index',compact('getEksperci','getBlog','routes_array', 'getCms','getMiasta','getWojewodztwa'))->header('Content-Type', 'text/xml');
     }

    public static function getRole($id) {
        if($id == null) {
            return Role::get();
        } else {
            return Role::where('id_role',$id)->get();
        }
    }

    public static function getProvinceDependsOnCity($id_city) {
        $miasto = City::where('id_city',$id_city)->first();

        
        $province = Province::where('id_province',$miasto->id_province)->first();
        return $province->name;
    }
    public static function getWidgetOpinie($id_ekspert) {
        $opinie = Review::where('id_ekspert',$id_ekspert)->where('status','1')->get();
        $ekspert = Ekspert::join('users','users.id','=','ekspert.id_user')->where('id_ekspert',$id_ekspert)->first();

        return view('frontend.reviews.index')->with('reviews', $opinie)->with('id_ekspert',$id_ekspert)->with('ekspert',$ekspert);
    }
    public function upload($input, $extensions, $path, $tytul, $request, $sizeX, $sizeY)
    {
        if ($extensions == 'jpg,jpeg,png') {
            $this->validate($request, [
            $input => 'image|mimes:'.$extensions.'|max:2048',
        ]);
        } else {
            $this->validate($request, [
            $input => 'file|mimes:'.$extensions.'|max:2048',
            ]);
        }


        $file = $request->file($input);
        $this->FileName = md5(str_slug($tytul.time())).'.'.$file->getClientOriginalExtension();
        $destinationPath = public_path($path);

        if ($extensions == 'jpg,jpeg,png') {
            $img = Image::make($file->path());
            $img->resize($sizeX, $sizeY, function ($constraint) {//200,200
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$this->FileName);
        } else {
            $filePath = $destinationPath.'/'.$this->FileName;
            $file->move($destinationPath, $this->FileName);
        }
    }

    public function getFileName()
    {
        return $this->FileName;
    }

    public static function getDays() {
        $days = array(
            "1" => "Poniedziałek",
            "2" => "Wtorek",
            "3" => "Środa",
            "4" => "Czwartek",
            "5" => "Piątek",
            "6" => "Sobota",
            "7" => "Niedziela"
        );
        return $days;
    }

    public static function getCountry($id_country) {
        if($id_country != '') {
            $countries = Country::where('id_country', $id_country)->orderBy('name', 'asc')->get();
        } else {
            $countries = Country::get();
        }
        return $countries;
    }
    public static function countServicesFromCategory($id_category, $id_ekspert) {
        $getServices = Service::join('user_services', 'user_services.id_service', '=', 'services.id_service')->where('services.id_category', $id_category)->where('user_services.id_ekspert', $id_ekspert)->where('user_services.price', '!=', '')->count();
        return $getServices;
    }
     public static function getProvinceName($id_province) {
         try {
             $province = Province::where('id_province', $id_province)->first();
             return $province->name;
         } catch (\Exception $e) {
             return 'wszystkie';
         }
    }
    public static function getProvinces() {
        $provinces = Province::orderBy('name','asc')->get();
        return $provinces;
    }
    public static function getPriceDependsOnService($id_service, $id_ekspert) {
        $isExist = UserServices::where('id_service', $id_service)->where('id_ekspert', $id_ekspert)->count();
        if($isExist > 0) {
            return UserServices::where('id_service', $id_service)->where('id_ekspert', $id_ekspert)->first()->price;
        } else {
            UserServices::create(['id_service' => $id_service, 'id_ekspert' => $id_ekspert]);

            return UserServices::where('id_service', $id_service)->where('id_ekspert', $id_ekspert)->first()->price;
        }
    }
 public static function getServicesByCategory($id_category,$id_ekspert) {
    $getServices = Service::join('user_services','user_services.id_service','=','services.id_service')->where('services.id_category',$id_category)->where('user_services.id_ekspert',$id_ekspert)->get();

    return $getServices;
    }
    public static function getCities() {
        $cities = City::orderBy('id_city')->orderBy('name','asc')->get();
        return $cities;
    }

    public static function getCitiesHome() {
        $cities = City::orderBy('position','asc')->limit(9)->get();
        return $cities;
    }
    public static function getAsyncMiastaByWojewodztwo($id) { 
        $getProvince = Province::where('id_province',$id)->first();
        $getMiasta = City::where('id_province', $getProvince->id_province)->get();

        return $getMiasta;
    }
     public static function getApiWojewodztwo($id) { 
        $getProvince = Province::where('id_province',$id)->first();

        return response()->json(['province'=>$getProvince]);
    }

     public static function getApiMiasto($id) { 
        $getCity = City::where('id_city',$id)->first();

        return response()->json(['city'=>$getCity]);
    }
    public static function getCityName($id_city) {
        
        try {
            if ($id_city != 'wszystkie') {
            return City::where('id_city', $id_city)->first()->name;
        }
        } catch(\Exception $e) {
            return false;
        }
    }

    public static function getReviews($id_user) {
        $reviews = Review::where('id_user', $id_user)->get();
        return $reviews;
    }
    
    public static function getInstagram() {
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://tanexpert24.pl/index.php?fc=module&module=mjinstagramfeed&controller=feature");
        
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
            $output = curl_exec($ch);
            // dd($output);
            foreach(json_decode($output, 1) as $feed) {
            ?>
            <li class="col-xs-6 col-md-3 col-sm-6" style="list-style: none;margin: 0;padding: 0;">
                <a href="<?php echo $feed['post_link']; ?>" title="<?php echo $feed['title']; ?>" target="_blank"
                    style="list-style-type:none;width:100%;background-size: cover;height: 350px;overflow: hidden;display: inline-block;background-image: url('https://tanexpert24.pl/modules/mjinstagramfeed/img/<?php echo $feed['post_id']; ?>.jpg')">
                    <img src="https://tanexpert24.pl/modules/mjinstagramfeed/img/<?php echo $feed['post_id']; ?>.jpg"
                        alt="" style="opacity:0;">
                </a>
        </li>
            <?php
            }
        } catch(\Exception $e) {
            return false;
        }
    }

    public function login_api(Request $request) {
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            return response()->json([
                'user' => $request->input('email'),
                'state' => '1',
            ]);
        } else {
            return response()->json([
                'user' => $request->input('email'),
                'state' => '0',
            ]);
        }
        die();
    }
}

