<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use Validator;
use Socialite;
use Exception;
use Auth;
use App\Ekspert;
use App\Opening;
use App\UserServices;
use App\Service;

class FacebookController extends Controller
{
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function facebookSignin()
    {
        try {
    
            $user = Socialite::driver('facebook')->user();
            $facebookId = User::where('facebook_id', $user->id)->first();
     
            if($facebookId){
                Auth::login($facebookId);
                return redirect('/login');
            }else{
                $createUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'facebook_id' => $user->id,
                    'password' => encrypt('john123'),
                    'id_role' => 1,
                    'business' => str_slug($user->email)
                ]);
    
                $ekspert = new Ekspert();
                $ekspert->id_user = $createUser->id;
                $ekspert->name = $user->name;
                $ekspert->save();

                $email = trim($user->email);
                $konto = array(
                        'name' =>  $user->name,
                        'email' => $user->email,
                        'business' => str_slug($user->email)
                );
            
                $temat = 'Utworzenie konta w serwisie '.env('APP_NAME');
                $data = array('temat'=>'Założłeś konto w serwisie '.env('APP_NAME'), 'email'=>$email, 'konto'=>$konto);

$godziny_otwarcia = array(
    "","","","","","",""
);

            foreach($godziny_otwarcia as $key => $godzina_od) {
            $opening = new Opening();
            $opening->id_user = $ekspert->id_ekspert;
            $opening->dzien = $key+1;
            $opening->godzina_od = $godzina_od;
            $opening->godzina_do = $godzina_od;
            $opening->save();
        }
      

        $uslugi_ceny = Service::get();
        foreach($uslugi_ceny as $key => $cena) {
                $userServices = new UserServices();
                $userServices->id_ekspert = $ekspert->id_ekspert;
                $userServices->id_service = $cena->id_service;
                $userServices->price = null;
                $userServices->save();
               
        }
 
                Auth::login($createUser);

                return redirect('/panel');
            }
    
        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }
}
