<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use App\Galeria;
use App\Ekspert;
use Auth;
use Session;

class GaleriaController extends Controller
{
    public function index() {

        $getUser = Ekspert::join('users', 'users.id', '=', 'ekspert.id_user')->where('id', Auth::user()->id)->first();
       $galleries = Galeria::where('id_user',(Auth::user()->id))->paginate(25);
       return view('frontend.galeria.index')->with('galleries', $galleries)->with('ekspert', $getUser);
   }

    public function cropUpload(Request $request) {
        $folderPath = public_path('img/uploads/galleries/');


        $image_parts = explode(";base64,", $request->image);

        $image_type_aux = explode("image/", $image_parts[0]);

        $image_type = $image_type_aux[1];

        $image_base64 = base64_decode($image_parts[1]);

        $name = 'croped'.uniqid() . '.png';
        $file = $folderPath . $name;


        file_put_contents($file, $image_base64);


        return response()->json(['success'=>'success','path' => '/img/uploads/galleries/'.$name, 'name' => $name]);

    }

   public function create() {
       $getUser = Ekspert::join('users', 'users.id', '=', 'ekspert.id_user')->where('id', Auth::user()->id)->first();
       return view('frontend.galeria.create')->with('ekspert', $getUser);
   }
   public function store(Request $request) {
        $galleries = new Galeria();
            
        $getGallery = Galeria::where('id_user', Auth::user()->id)->count();

        $galleries->label = $request->input('label');
        
        $galleries->position = $getGallery+1;
        //$galleries->feature = $request->input('feature');
        $galleries->id_user = (Auth::user()->id);
                  
        // Avatar usera
         $upload = new Controller();
         
        if ((Input::file('filename'))) {
           
            $upload->upload('filename', 'jpg,jpeg,png', '/img/uploads/galleries/', $request->get('filename'), $request, 800, 800);
    // dd("FD");
            // $ekspert = Ekspert::where('id_ekspert', Session::get('form_ekspert_id'))->first();
            if(!empty($request->input('croped'))) { 
                $galleries->filename = $request->input('croped');
            } else {
                $galleries->filename = $upload->getFileName();
            }
        }
        $galleries->feature = 0;
        $galleries->save();

        Session::flash('success_message', 'Dodano pomyślnie.');
        return redirect()->route('frontend_galeria_index');
   }
   public function update(Request $request, $id) {

    if(!empty($request->input('feature'))) {
        $feature = 1;
    } else {
        $feature = 0;
    }
        $galleries = Galeria::where('id_galeria', $id)->update([
           // 'label' => $request->input('label'),
            'position' => $request->input('position'),
            //'feature' => $feature,
        ]);
                    
        Session::flash('success_message', 'Zaktualizowano pomyślnie.');
     return redirect()->route('frontend_galeria_edit', ['id'=>$id]);

   }

   public function edit($id) {
       try {
           //dd($id);
           $getGallery = Galeria::where('id_galeria', $id)->first();
$getUser = Ekspert::join('users', 'users.id', '=', 'ekspert.id_user')->where('id', Auth::user()->id)->first();
           return view('frontend.galeria.edit')->with('galleries', $getGallery)->with('ekspert', $getUser)->with('id', $id);
       } catch (\Exception $e) {
            return false;
       }
   }

   public function destroy($id) {
         try {
            $galleries = Galeria::where('id_galeria', $id)->delete();
            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back();
        }
   }
}
