<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\City;

class CityController extends Controller
{
    public function index() {
        $miasta = City::get();
        return view('frontend.city.index')->with('miasta',$miasta);
    }
    public function show($id) {
        $miasto = City::where('id_city',$id)->get();
        return view('frontend.city.show')->with('miasto',$miasto);
    }
}
