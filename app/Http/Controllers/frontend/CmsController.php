<?php


namespace App\Http\Controllers\frontend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Cms;
use App\Http\Requests\KontaktRequest;
use Illuminate\Support\Facades\Mail;

class CmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function dla_ekspertek() {
        return view('frontend.cms.dla_ekspertek');
    }

    public function kontakt()
    {
        return view('frontend.cms.kontakt');
    }
    public function kontakt_store(KontaktRequest $request) { 


        $email = trim(env('MAIL_USERNAME'));
        $konto = array(
                'imie' =>  $request->input('imie'),
                'temat' => $request->input('temat'),
                'telefon' => $request->input('telefon'),
                'email' => $request->input('email'),
                'tresc' => $request->input('tresc')
            );
            
        $temat = 'Wiadomość z serwisu '.env('APP_DOMAIN').'';
                $data = array('temat'=>'Wiadomość z serwisu '.env('APP_DOMAIN'), 'email'=>$email, 'konto'=>$konto);
                 
            // $getUstawienia = Ustawienia::where('instancja','mails')->first();
            // if ($getUstawienia->wartosc == '1') {
                // Mail::send('mails.kontakt', $data, function ($message) use ($temat, $email) {
                //     $message->from(env('MAIL_USERNAME'), $temat);
                //     $message->to($email)->subject($temat);
                // });
            // }

        return redirect()->back();
    }

    public function szkolenie() {
        return view('frontend.cms.szkolenie');
    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $title)
    {
        $cms = Cms::where('id_cms', $id)->get();

        return view('frontend.cms.show')->with('cms', $cms);
    }
 
}
