<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;

use App\Review;
use Illuminate\Http\Request;
use App\Http\Requests\ReviewRequest;
use Auth;
use Session; 
use App\Ekspert;

class ReviewController extends Controller
{
    public function review_request(ReviewRequest $request) {
        $opinia = new Review();
        $opinia->id_user = (Auth::check()) ? Auth::user()->id : null;
        $opinia->comment = $request->input('comment');
        $opinia->nick = $request->input('nick');
        $opinia->rate = $request->input('rate');
        $opinia->status = '0';
        $opinia->id_ekspert = $request->input('id_ekspert');
        $opinia->save();

        $ekspert = Ekspert::join('users','users.id','=','ekspert.id_user')->where('ekspert.id_ekspert',$request->input('id_ekspert'))->first();

        Session::flash('status', 'Dziękujemy za dodanie opinii! Nasi administratorzy niebawem ją sprawdzą.');

        //Dziękujemy za dodanie opinii! Nasi administratorzy niebawem ją sprawdzą.
        //return redirect()->route('ekspert_show',['id'=>$request->input('id_ekspert')]);
        return redirect()->route('profil_alias',['account'=>$ekspert->business]);
    }
 
}
