<?php

namespace App\Http\Controllers\frontend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Ekspert;
use App\Province;
use App\Review;
use App\User;
use App\Opening;
use App\City;
use Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\UserServices;


class EkspertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ekspert = Ekspert::join('users','users.id','=','ekspert.id_user')->orderBy('feature','desc')->orderBy('cert','desc')->paginate($this->limit);
        $wojewodztwo = '';

        $miasto = '';
        $filtr = '';
        $seoWojewodztwo = '';
        $seoMiasto = '';
        return view('frontend.ekspert.index')->with('ekspert',$ekspert)->with('wojewodztwo',$wojewodztwo)->with('miasto', $miasto)->with('filtr',$filtr)->with('seoWojewodztwo',$seoWojewodztwo)->with('seoMiasto',$seoMiasto);
    }

    public static function avgReview($id_ekspert) {
        $avgReview = Review::where('id_ekspert',$id_ekspert)->avg('rate');
        return number_format($avgReview,2);
    }

    public static function countReview($id_ekspert) {
        $countReview = Review::where('id_ekspert',$id_ekspert)->count();
        return $countReview;
    }

    public function getEkspertsByFiltersRequest(Request $request) {

        
        $miasto = $request->input('miasto'); 
        $wojewodztwo = $request->input('wojewodztwo'); 

        // Jeśli jest puste wojewodztwo lub miasto
        if (empty($wojewodztwo)) {
            $wojewodztwo = '0';
            $wojewodztwo_name = 'wszystkie';
        } else {
                $wojewodztwo_name = str_slug(Province::where('id_province', $wojewodztwo)->first()->name);
        }

        if(empty($miasto)) {
            $miasto = '0';
            $miasto_name = 'wszystkie';
        } else {
                $miasto_name = str_slug(City::where('id_city', $miasto)->first()->name);
        }
        
        return redirect()->route('eksperts_by_filters',['wojewodztwo'=>$wojewodztwo,'wojewodztwo_name'=>$wojewodztwo_name,'miasto'=>$miasto,'miasto_name'=>$miasto_name,'filtr'=>$request->input, 'filtr'=>$request->input('filtr')]);
    }

    public function getApiEkspertsByFilters($wojewodztwo, $wojewodztwo_name, $miasto, $miasto_name, $filtr) {
        if ($wojewodztwo > '0') {
            $getWojewodztwo = Province::where('name', $wojewodztwo)->orWhere('id_province',$wojewodztwo)->first();
            $wojewodztwoName = $getWojewodztwo->name;
        } else {
            $getWojewodztwo = '';
            $wojewodztwoName = 'wszystkie';
        }
        if($miasto > '0') { 
            $getMiasto = City::where('name', $wojewodztwo)->orWhere('id_city',$miasto)->first();
            // dd($miasto);
            $miastoName = $getMiasto->name;
        } else {
            $getMiasto = '';
            $miastoName = 'wszystkie';
        }
        $seoMiasto = $getMiasto;
        $seoWojewodztwo = $getWojewodztwo;
        /** 
         * Sortowanie 
         * */
        
        if(($filtr != '0')) { 
            // Mobilni
            if($filtr == '1') {
                  $ekspert = Ekspert::join('users', 'users.id', '=', 'ekspert.id_user')->where('ekspert.typ','1')->orWhere('ekspert.typ','3');

            }
            // Wyroznione
            else if($filtr == '2') {
                $ekspert = Ekspert::join('users','users.id','=','ekspert.id_user')->where('ekspert.feature','1')->orderBy('id_ekspert', 'desc');

            } 
            // Salony stacjonarne
            else if($filtr == '3') {
                $ekspert = Ekspert::join('users','users.id','=','ekspert.id_user')->where('ekspert.typ','0')->orWhere('ekspert.typ','3');

            } else {
                $ekspert = Ekspert::join('users','users.id','=','ekspert.id_user')->orderBy('feature','desc')->orderBy('cert','desc');
            }
        }
        /**
         * Województwo
         */
        if (($wojewodztwo != '0')) {
            if ($getWojewodztwo) {
                if ($wojewodztwo != 'wszystkie') {
                    $ekspert  = Ekspert::join('users', 'users.id', '=', 'ekspert.id_user')->where('province', $getWojewodztwo->id_province)->orderBy('feature','desc')->orderBy('cert','desc');
                } else {
                    $ekspert = Ekspert::join('users', 'users.id', '=', 'ekspert.id_user')->orderBy('feature','desc')->orderBy('cert','desc');
                }
            } else {
                return view('errors.404');
            }
            
        }  
        /**
         * Miasto
         */
        
          if (($miasto != '0')) {
              // Wybrane wojewodzwo moge napisac gowno i bedzie to ignorowne
            if ($getWojewodztwo) {
                //if ($wojewodztwo != 'wszystkie') {
                    $ekspert  = Ekspert::join('users', 'users.id', '=', 'ekspert.id_user')->where('province', $getWojewodztwo->id_province)->where('city',$miasto)->orderBy('feature','desc')->orderBy('cert','desc');
                //}
            } else {
                // Samo miasto
                $ekspert  = Ekspert::join('users', 'users.id', '=', 'ekspert.id_user')->where('city',$miasto)->orderBy('feature','desc')->orderBy('cert','desc');
            }
            
        }

        /**
         * Bez filtrów
         */
        if(($miasto == '0') && ($wojewodztwo == '0') && ($filtr == '0')) {
            $ekspert  = Ekspert::join('users', 'users.id', '=', 'ekspert.id_user')->orderBy('feature','desc')->orderBy('cert','desc');
        }
        /**
         * Najwyzej oceniane
         */
        // if($filtr == 1) {
        //     $ekspert = Ekspert::where('province',$getWojewodztwo->id)->where('city',$miasto)->orderBy('id_ekspert','desc')->paginate(10);
        // }
        /**
         * Najpopularniejsze
         */
        // else if($filtr == 2) {
        //     //$categories = \App\Models\Category::with('articles')->get();
        //     $ekspert = Ekspert::with('review')->where('province',$getWojewodztwo->id)->orderBy('id_ekspert','desc')->paginate(10);
        // }
        /**
         * Najnowsze
         */
        // else {
        //     $ekspert = Ekspert::where('province',$getWojewodztwo->id)->where('city',$miasto)->orderBy('id_ekspert','desc')->paginate(10);
        // }
 return view('frontend.ekspert.api_response')->with('ekspert',$ekspert->paginate(500))->with('wojewodztwo',$wojewodztwoName)->with('miasto', $miasto)->with('miasto_name', $miasto_name)->with('filtr',$filtr)->with('seoWojewodztwo',$seoWojewodztwo)->with('seoMiasto',$seoMiasto);
        // return response()->json([
        //     'ekspert' => $ekspert->paginate(250),
        //     'wojewodztwo' => $wojewodztwoName,
        //     'miasto' => $miasto,
        //     'miasto_name' => $miasto_name,
        //     'filtr' => $filtr,
        //     'seoWojewodztwo' => $seoWojewodztwo,
        //     'seoMiasto' => $seoMiasto
        // ]);
    }
    public function getEkspertsByFilters($wojewodztwo, $wojewodztwo_name, $miasto, $miasto_name, $filtr) {
        if ($wojewodztwo > '0') {
            $getWojewodztwo = Province::where('name', $wojewodztwo)->orWhere('id_province',$wojewodztwo)->first();
            $wojewodztwoName = $getWojewodztwo->name;
        } else {
            $getWojewodztwo = '';
            $wojewodztwoName = 'wszystkie';
        }
        if($miasto > '0') { 
            $getMiasto = City::where('name', $wojewodztwo)->orWhere('id_city',$miasto)->first();
            // dd($miasto);
            $miastoName = $getMiasto->name;
        } else {
            $getMiasto = '';
            $miastoName = 'wszystkie';
        }
        $seoMiasto = $getMiasto;
        $seoWojewodztwo = $getWojewodztwo;
        /** 
         * Sortowanie 
         * */
        
        if(($filtr != '0')) { 
            // Mobilni
            if($filtr == '1') {
                  $ekspert = Ekspert::join('users', 'users.id', '=', 'ekspert.id_user')->where('ekspert.typ','1')->orWhere('ekspert.typ','3');

            }
            // Wyroznione
            else if($filtr == '2') {
                $ekspert = Ekspert::join('users','users.id','=','ekspert.id_user')->where('ekspert.feature','1')->orderBy('id_ekspert', 'desc');

            } 
            // Salony stacjonarne
            else if($filtr == '3') {
                $ekspert = Ekspert::join('users','users.id','=','ekspert.id_user')->where('ekspert.typ','0')->orWhere('ekspert.typ','3');

            } else {
                $ekspert = Ekspert::join('users','users.id','=','ekspert.id_user')->orderBy('id_ekspert','desc');
            }
        }
        /**
         * Województwo
         */
        if (($wojewodztwo != '0')) {
            if ($getWojewodztwo) {
                if ($wojewodztwo != 'wszystkie') {
                    $ekspert  = Ekspert::join('users', 'users.id', '=', 'ekspert.id_user')->where('province', $getWojewodztwo->id_province)->orderBy('feature','desc')->orderBy('cert','desc');
                } else {
                    $ekspert = Ekspert::join('users', 'users.id', '=', 'ekspert.id_user')->orderBy('feature','desc')->orderBy('cert','desc');
                }
            } else {
                return view('errors.404');
            }
            
        }  
        /**
         * Miasto
         */
        
          if (($miasto != '0')) {
              // Wybrane wojewodzwo moge napisac gowno i bedzie to ignorowne
            if ($getWojewodztwo) {
                //if ($wojewodztwo != 'wszystkie') {
                    $ekspert  = Ekspert::join('users', 'users.id', '=', 'ekspert.id_user')->where('province', $getWojewodztwo->id_province)->where('city',$miasto)->orderBy('feature','desc')->orderBy('cert','desc');
                //}
            } else {
                // Samo miasto
                $ekspert  = Ekspert::join('users', 'users.id', '=', 'ekspert.id_user')->where('city',$miasto)->orderBy('feature','desc')->orderBy('cert','desc');
            }
            
        }

        /**
         * Bez filtrów
         */
        if(($miasto == '0') && ($wojewodztwo == '0') && ($filtr == '0')) {
            $ekspert  = Ekspert::join('users', 'users.id', '=', 'ekspert.id_user')->orderBy('feature','desc')->orderBy('cert','desc');
        }
        /**
         * Najwyzej oceniane
         */
        // if($filtr == 1) {
        //     $ekspert = Ekspert::where('province',$getWojewodztwo->id)->where('city',$miasto)->orderBy('id_ekspert','desc')->paginate(10);
        // }
        /**
         * Najpopularniejsze
         */
        // else if($filtr == 2) {
        //     //$categories = \App\Models\Category::with('articles')->get();
        //     $ekspert = Ekspert::with('review')->where('province',$getWojewodztwo->id)->orderBy('id_ekspert','desc')->paginate(10);
        // }
        /**
         * Najnowsze
         */
        // else {
        //     $ekspert = Ekspert::where('province',$getWojewodztwo->id)->where('city',$miasto)->orderBy('id_ekspert','desc')->paginate(10);
        // }
 return view('frontend.ekspert.index')->with('ekspert',$ekspert->paginate($this->limit))->with('wojewodztwo',$wojewodztwoName)->with('miasto', $miasto)->with('miasto_name', $miasto_name)->with('filtr',$filtr)->with('seoWojewodztwo',$seoWojewodztwo)->with('seoMiasto',$seoMiasto);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ekspert = Ekspert::join('users','users.id','=','ekspert.id_user')->where('id_ekspert',$id)->get();
        $ocena = Review::where('id_ekspert', $id)->where('status','1')->avg('rate');

        $getUser = Ekspert::where('id_ekspert',$id)->first();
        $opening = Opening::where('id_user',$getUser->id_user)->get();

        $getCennik = UserServices::where('id_ekspert',$id)->get();

        return view('frontend.ekspert.show')->with('ekspert',$ekspert)->with('rate',round($ocena))->with('opening', $opening)->with('getCennik',$getCennik);
    }

    public function getEkspertByWojewodztwo($wojewodztwo){
        $getWojewodztwo = Province::where('name',$wojewodztwo)->first();
      
        $ekspert = Ekspert::where('province',$getWojewodztwo->id)->orderBy('id_ekspert','desc')->paginate(10);
        //  dd($ekspert);

        return view('frontend.ekspert.index')->with('ekspert',$ekspert)->with('wojewodztwo',$wojewodztwo);
    }
    public function ekspert_kontakt_store(Request $request) {
          $email = trim(env('MAIL_USERNAME'));
        $konto = array(
                // 'imie' =>  $request->input('imie'),
                // 'temat' => $request->input('temat'),
                // 'telefon' => $request->input('telefon'),
                'email' => $request->input('email'),
                'tresc' => $request->input('tresc')
            );
            
        $temat = 'Zapytanie do eksperta z serwisu '.env('APP_DOMAIN').'';
                $data = array('temat'=>'Zapytanie do eksperta z serwisu '.env('APP_DOMAIN'), 'email'=>$email, 'konto'=>$konto);
                 
            // $getUstawienia = Ustawienia::where('instancja','mails')->first();
            // if ($getUstawienia->wartosc == '1') {
                // Mail::send('mails.zapytanie', $data, function ($message) use ($temat, $email) {
                //     $message->from(env('MAIL_USERNAME'), $temat);
                //     $message->to($email)->subject($temat);
                // });
                return redirect()->back();
    }

    public function checkBusinessName(Request $request) {
        //$business
        $getUser = User::where('business',$request->input('business_user'))->count();
        if($getUser == 1) {
            return response()->json([
            'business' => $request->input('business_user'),
            'state' => '1',
        ]);
        } else {
               return response()->json([
                'business' => $request->input('business_user'),
                'state' => '0',
            ]);
        }
    }
}
