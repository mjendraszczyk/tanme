<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Blog;
use App\Ekspert;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $blog = Blog::orderBy('id_blog', 'desc')->limit(3)->get();
        $eksperci = Ekspert::join('users','users.id','=','ekspert.id_user')->orderBy('id_ekspert', 'desc')->limit(6)->get();
        return view('index')->with('eksperci',$eksperci)->with('blog',$blog);
    }
    public function requestWojewodztwo(Request $request) {
        // dd($request);

        if(empty($request->input('miasto'))) {
            $miasto = 'wszystkie';
        }
 
        return redirect()->route('ekspert_by_wojewodztwo',['wojewodztwo'=>$request->input('wojewodztwo'), 'miasto'=>$miasto, 'filtr'=>$request->input('filtr')]);
//         echo "TEST";
// exit();
    }
}


