<?php

namespace App\Http\Controllers\frontend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Blog;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    $blog = Blog::orderBy('id_blog', 'desc')->paginate(3);//->get();
       return view('frontend.blog.index')->with('blog',$blog);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = Blog::where('id_blog',$id)->orderBy('id_blog', 'desc')->get();
        return view('frontend.blog.show')->with('blog',$blog);
    }
}
