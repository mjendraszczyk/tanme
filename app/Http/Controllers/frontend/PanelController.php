<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ekspert;
use App\User;
use Auth;
use App\Opening;
use Illuminate\Support\Facades\Input;
use App\UserServices;
use App\Category;
use App\Service;


class PanelController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    public function index() { 

        try {
            $getUser = Ekspert::join('users', 'users.id', '=', 'ekspert.id_user')->where('id', Auth::user()->id)->first();

            if ($getUser != null) {
                return view('frontend.panel.index')->with('ekspert', $getUser);
            } else {
                return view('errors.403');
            }

        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function services() {
        $getUser = Ekspert::join('users','users.id','=','ekspert.id_user')->where('id_user',Auth::user()->id)->first();
         $id = Auth::user()->id;
            $categories = Category::get();
            $services = Service::get();
            return view('frontend.panel.services')->with('id',$id)->with('categories',$categories)->with('services',$services)->with('ekspert', $getUser);
        // return view('frontend.panel.services');
    }
    public function services_update(Request $request) {
        $getEkspert = Ekspert::where('id_user',Auth::user()->id)->first();
        $i=0;
        foreach($request->input('uslugi_ceny') as $key => $cena) {
                $getUserService = UserServices::where('id_ekspert',$getEkspert->id_ekspert)->where('id_service',$request->input('uslugi_pozycja')[$i]);

                $getUserService->update([
                    'price' => $cena 
                ]);
                
                $i++;
        }
        return redirect()->back();
    }
    public function opening() {
        try {
            
        $getEkspert = Ekspert::where('id_user',Auth::user()->id)->first();
        $opening = Opening::where('id_user',$getEkspert->id_ekspert)->get()->toArray();

        $countOpening = Opening::where('id_user',$getEkspert->id_ekspert)->count();
        
         if($countOpening == 0) { 
            $godzinaOd = array(
                "0:00",
                "0:00",
                "0:00",
                "0:00",
                "0:00",
                "0:00",
                "0:00",
            );
            
            foreach($godzinaOd as $key => $godzina_od) {
            $opening = new Opening();
            $opening->id_user = $getEkspert->id_ekspert;
            $opening->dzien = $key+1;
            $opening->godzina_od = $godzina_od;
            $opening->godzina_do = $godzina_od;
            $opening->save();
            }
            
        }

        // echo "GG".$countOpening;
        // dd($opening);
        //     exit();

        $getUser = Ekspert::join('users','users.id','=','ekspert.id_user')->where('id_user',Auth::user()->id)->first();
        $countEkspert = Ekspert::where('id_user',Auth::user()->id)->count();
        return view('frontend.panel.opening')->with('ekspert', $getUser)->with('opening',$opening)->with('countEkspert',$countEkspert)->with('countOpening',$countOpening);

        } catch(\Exception $e) {
           return false; 
        }
    }
    public function opening_update(Request $request) {

        $getEkspert = Ekspert::where('id_user',Auth::user()->id)->first();
        $getOpening = Opening::where('id_user', Auth::user()->id)->count();
        if($getOpening == 0) {
        foreach($request->input('godzina_od') as $key => $godzina_od) {

            $opening = Opening::where('id_user',$getEkspert->id_ekspert)->where('dzien',($key + 1))->update(
                [
                    'godzina_od' => $godzina_od,
                    'godzina_do' => $request->input('godzina_do')[$key]

                ]
            );
            // $opening->id_user = $getEkspert->id_ekspert;
            // $opening->dzien = $key+1;
            // $opening->godzina_od = $godzina_od;
            // $opening->godzina_do = $request->input('godzina_do')[$key];
            // $opening->save();
        }

        } else {
            $opening = Opening::where('id_user', Auth::user()->id)->get();


            $getRequestKrok2GodzinyOd = array();
            $getRequestKrok2GodzinyDo = array();

            foreach ($request->input('godzina_od') as $key => $godzina_od) {
                $getRequestKrok2GodzinyOd[] = $godzina_od;
                $opening =  Opening::where('id_user', Auth::user()->id)->where('dzien', ($key+1))->first();
                $opening->update([
                'godzina_od' => $godzina_od,
                'godzina_do' => $request->input('godzina_do')[$key]
            ]);
            }
        }
     
            return redirect()->route('opening_update');
    }
    public function update(Request $request) {

//dd($request);
                // 'name', 'content', 'feature_image','cert','phone','typ','address','city','province','expert_email', 'feature','id_user','facebook','instagram','youtube', 'lat', 'lon'
         $ekspert = Ekspert::where('id_user', Auth::user()->id);

         if(count($request->input('typ')) > 1) {
            $typ = '3';
         } else {
             $typ = $request->input('typ')[0];
         }
            $ekspert->update([
            'name' => $request->input('name'),
            'expert_email' => $request->input('expert_email'),
            // 'content' => $request->input('opis'),
            'address' =>
            $request->input('address'),
            'phone' =>
            $request->input('phone'),
            'facebook' =>
            $request->input('facebook'),
            'instagram' =>
            $request->input('instagram'),
            'youtube' =>
            $request->input('youtube'),
            'typ' =>
            $typ,
            'lat' =>
            $request->input('lat'),
            'lon' =>
            $request->input('lon'),
            'short_description' =>
            $request->input('short_description'),
            'province' =>
            $request->input('id_province'),
            'city' =>
            $request->input('miasto'),
            ]);

            $getUser = User::where('id',Auth::user()->id);

            if(!empty($request->input('croped'))) {
                 $ekspert->update([
                'feature_image' => $request->input('croped')
                ]);
            } else {
            if ((Input::file('ekspert_upload'))) {
                $upload = new Controller();
            $upload->upload('ekspert_upload', 'jpg,jpeg,png', '/img/ekspert/', $request->get('ekspert_upload'), $request, 800, 360);

            $ekspert->update([
                'feature_image' => $upload->getFileName()
                ]);
        }
    }


         $getUser->update([
                'name' => $request->input('name')
            ]);
            // $getUser->update([
            //         'business' => $request->input('business'),
            // ]);

                return redirect()->route('panel_index');
    }
}
