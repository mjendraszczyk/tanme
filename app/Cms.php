<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cms extends Model
{
      protected $primaryKey = 'id_cms';
      protected $table = 'cms';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'state'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
