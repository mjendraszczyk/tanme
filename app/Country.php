<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
        protected $primaryKey = 'id_country';
      protected $table = 'country';


      protected $fillable = [
        'name'
    ];
}
