@extends('layouts.app')

@section('meta_seo')
<title>
    TanFinder - Blog
</title>
@endsection
@section('content')

<section class="block">
    <div class="blog">
        <div class="container">
            <h1>
                Blog
            </h1>

            <div class="row">
                @foreach ($blog as $item)
                <div class="col-md-4">
                    <a href="{{route('blog_show',['id'=>$item->id_blog,'name'=>str_slug($item->title)])}}">
                        <img src="{{asset('img/blog')}}/{{$item->feature_image}}" alt="">
                    </a>
                    <h3>{{$item->title}}</h3>
                    <p>
                        {{strip_tags(str_limit($item->content,150))}}
                        <br>
                        <a href="{{route('blog_show',['id'=>$item->id_blog,'name'=>str_slug($item->title)])}}">czytaj
                            więcej</a>
                    </p>
                </div>
                @endforeach
            </div>
            {{ $blog->links() }}
        </div>
    </div>
</section>
@endsection