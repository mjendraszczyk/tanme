@extends('layouts.app')

@section('meta_seo')
<title>
    TanFinder -
    @foreach ($blog as $item)
    {{$item->title}}
    @endforeach
</title>
@endsection

@section('content')
<section class="block">
    <div class="blog">
        <div class="container">
            <div class="blog_content">
                @foreach ($blog as $item)
                <div class="blog_image"
                    style="width:100%;background-image:url({{asset('img/blog')}}/{{$item->feature_image}});margin-top:-100px;">
                    <img src="{{asset('img/blog')}}/{{$item->feature_image}}" alt=""
                        style="margin-top: -100px;opacity:0;">
                </div>
                <h1>
                    {{$item->title}}
                </h1>
                {!! $item->content !!}
                @endforeach
                <div class="clearfix"></div>
                <p>
                    <a href="{{ route('blog_index')}}">Powrót</a>
                </p>
            </div>
        </div>
    </div>
</section>
@endsection