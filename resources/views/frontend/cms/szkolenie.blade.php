@extends('layouts.app')

@section('meta_seo')
<title>Opalanie natryskowe szkolenie
</title>
@endsection

@section('body')
szkolenie
@endsection
@section('content')
<section class="block">
    <div class="container">
        <h1>
            Opalanie natryskowe Szkolenie
        </h1>
        <div class="row text-left" style="padding: 100px 0;">
            <div class="col-md-6">
                <p>
                    <strong>
                        TanExpert to brand, który zrzesza najbardziej doświadczonych polskich szkoleniowców w zakresie
                        opalania
                        natryskowego. 5
                        naszych TanExpert Master razem wziętych ma ponad 20 lat doświadczenia w branży pomimo, że są to
                        jeszcze
                        osoby w młodym i
                        bardzo produktywnym wieku.
                    </strong>
                </p>
                <br><br>
                <p>
                    W tak świetnym zespole możesz liczyć na merytoryczne wsparcie i w pełni partnerskie podejście na
                    każdym
                    etapie. Nikt z
                    nas nie próżnuje, a rozwój i dobre relacje to dla nas fundamentalna wartość pracy zawodowej.
                </p>
                <br><br>
                <p>
                    Kładziemy szczególny nacisk na doświadczenie naszych TanExpert Master i współpracujemy w tym
                    obszarze
                    wyłącznie z
                    pozytywnie zweryfikowanymi Ekspertkami, które od lat działają z powodzeniem w branży. Wg naszych
                    standardów
                    szkoleniowcem powinna być osoba dla której opalanie natryskowe stanowi główną lub dominującą oś
                    działalności.
                </p>
            </div>
            <div class="col-md-6">
                <img src="{{asset('img/szkolenie/szkolenie_img1.png')}}" alt="">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="szkolenie-box">
                    <div class="szkolenie-box-inner">
                        <img src="{{asset('img/szkolenie/czesc_teoretyczna.png')}}" alt="">
                        <h2>
                            Część teoretyczna
                        </h2>
                        <p>
                            (wdrożenie w świat samoopalaczy)
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="szkolenie-box">
                    <div class="szkolenie-box-inner">
                        <img src="{{asset('img/szkolenie/czesc_praktyczna.png')}}" alt="">
                        <h2>
                            Część praktyczna
                        </h2>
                        <p>
                            (pod okiem
                            TanExpert Master)
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="szkolenie-box-full">
                    <img src="{{asset('img/szkolenie/szkolenie_certyfikat.png')}}" style="magin-top:-120px !important;"
                        alt="">
                    <h2>
                        Certyfikat TanExpert
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="zarobki">
        <div class="container">
            <div class="row">
                <div class="col-md-4 text-left">
                    <h1>
                        Zarobki
                    </h1>
                    <p>
                        Zobacz ile może zarabiać opalając natryskowo 6x w tygodniu*:
                    </p>
                    <img class="szkolenie_img_zarobki" src="{{asset('img/szkolenie/szkolenie_zarobki_img.jpg')}}"
                        alt="">
                </div>
                <div class="col-md-8">
                    <table>
                        <tr>
                            <td></td>
                            <td>Tygodniowo</td>
                            <td>Miesięcznie</td>
                        </tr>
                        <tr>
                            <td>2 zabiegi / dzień</td>
                            <td>1200 PLN</td>
                            <td>4800 PLN</td>
                        </tr>
                        <tr>
                            <td>5 zabiegów / dzień</td>
                            <td>3000 PLN</td>
                            <td>12000 PLN</td>
                        </tr>
                        <tr>
                            <td>10 zabiegów / dzień</td>
                            <td>6000 PLN</td>
                            <td>24000 PLN</td>
                        </tr>
                        <tr>
                            <td>15 zabiegów / dzień</td>
                            <td>9000 PLN</td>
                            <td>36000 PLN</td>
                        </tr>
                        <tr>
                            <td>20 zabiegów / dzień</td>
                            <td>12000 PLN</td>
                            <td>48000 PLN</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="gwiazdy">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-9 text-left">
                            <h3>
                                Warto zauważyć, że na liście osób polecających nasze kosmetyki znajduje się wiele
                                Gwiazd.
                            </h3>
                            <br><br>
                            <p>
                                Zapraszam do odwiedzenia naszego profilu na Instagramie <a href="#">@tanexpert</a>.
                            </p>

                        </div>
                    </div>
                    
                </div>
                <div class="col-md-6">
                <img class="szkolenie_img_mobile" src="{{asset('img/szkolenie/szkolenie_mobile.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <h1 class="block-full">
                Poznaj bliżej naszych szkoleniowców
            </h1>
        </div>
        <div class="row text-left">
            <div class="col-md-6 no-padding">
                <h3>
                    Natalia / Szczecin
                </h3>
                <p>
                    Od lat zakochana jestem w brązowym odcieniu skóry 🙂 Jestem pasjonatką i zapaloną testerką samoopalaczy, balsamów
                    brązujących i wszelkich alternatyw dla opalania na słońcu i w solarium. 3 lata temu poszłam na zabieg opalania do
                    Klaudii i namówiłam ją na otwarcie profesjonalnego salonu. I tak na samym początku pracowałyśmy na marce Fake Bake
                    (posiadam certyfikat z opalania natryskowego Fake Bake zdobyty podczas szkolenia u ikony tego biznesu – Moniki
                    Naskręckiej), a w ubiegłym roku rozpoczęłyśmy współpracę z najlepszą na polskim rynku marką TanExpert Oficjalnego
                    Dystrybutora MineTan Polska. Od początku roku jestem szkoleniowcem TanExpert Master i dzięki temu mam szansę przekazywać
                    dalej swoją wiedzę i doświadczenie. Swoje zamiłowanie do zdrowego opalania staram się godzić z długoletnią pracą w
                    zawodzie prawnika, jednak zdrowe słońce jest zdecydowanie bliższe mojemu sercu 🙂 Spod mojej ręki wyszły już tysiące
                    opalonych, zadowolonych klientek – może Ty będziesz kolejna?

                </p>
                <div class="row social">
                    <div class="col-md-6">
                        <span>
                            <a href="https://www.facebook.com/Kurkowa5/"><i class="fa fa-facebook"></i> /Kurkowa5</a>
                        </span>
                    </div>
                    <div class="col-md-6">
                        <span>
                            <a href="https://www.instagram.com/kurkowa5_szczecin/">
                                <i class="fa fa-instagram"></i> /kurkowa5_szczecin
                            </a>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <img src="{{asset('img/szkolenie/szkolenie_img2.png')}}" alt="">
            </div>
        </div>


        <div class="row text-left">
            <div class="col-md-6">
                <img src="{{asset('img/szkolenie/szkolenie_img5.png')}}" alt="">
            </div>
            <div class="col-md-6 no-padding">
                <h3>
                    Kasia / Warszawa
                </h3>
                <p>
                    Lorem ipsum dolor sit amet, eu quodsi periculis temporibus duo, modo cibo has eu. No vide
                    oporteat adolescens pro, at
                    eum noluisse facilisi necessitatibus. Mel an quod indoctum neglegentur, an aeque sensibus eum,
                    nibh aperiam qui eu. Ut
                    nam clita nullam vivendo, an ignota quodsi eos. Mea dico affert abhorreant cu, cu sed putant
                    alienum.Lorem ipsum dolor
                    sit amet, eu quodsi periculis temporibus duo, modo cibo has eu. No vide oporteat adolescens pro,
                    at eum noluisse
                    facilisi clita nullam vivendo, an ignota quodsi eos. Mea dico affert abhorreant cu, cu sed
                    putant alienum.
        
                </p>
                <div class="row social">
                    <div class="col-md-6">
                        <span>
                            <a href="https://www.facebook.com/youmakemetan/">
                                <i class="fa fa-facebook"></i> /youmakemetan
                            </a>
                        </span>
                    </div>
                    <div class="col-md-6">
                        <span>
                            <a href="https://www.instagram.com/youmakemetan/">
                                <i class="fa fa-instagram"></i> /youmakemetan
                            </a>
                        </span>
                    </div>
                </div>
            </div>
        
        </div>

        <div class="row text-left">
         
            <div class="col-md-6 no-padding">
                <h3>
                    Monika / Kalisz
                </h3>
                <p>
                   Nazywam się Monika Naskręcka i od ponad 5 lat prowadzę salon w Kaliszu zajmujący się profesjonalnym zabiegiem opalania
                natryskowego z wykorzystaniem produktów MineTan. Do każdej klientki i kursantki podchodzę indywidualnie, aby sprostać
                nawet najwyższym oczekiwaniom. W swoją pracę wkładam dużo pozytywnej energii i zapewniam gwarancję zadowolenia z usługi
                opalania oraz szkolenia. W 2015 roku rozpoczęłam swoją przygodę z opalaniem natryskowym jako ekspertka mobilna. Już po
                niespełna roku otworzyłam w Kaliszu pierwszy salon specjalizujący się wyłącznie w opalaniu natryskowym. W branży beauty,
                a dokładniej w świecie produktów samoopalających jestem od ponad 7 lat. Od 2017 roku przeprowadzam profesjonalne
                szkolenia, a w lutym 2020 roku podjęłam współpracę z firmą TanExpert. Do swojej pracy podchodzę perfekcyjnie w każdym
                możliwym aspekcie. Przywiązuję dużą wagę do pielęgnacji ciała, ale przede wszystkim do jej przygotowania. Edukuję
                klientki, ale także kursantki jak ważny jest to aspekt. Kocham swoją pracę i poświęcam jej dużo czasu oraz atencji. To
                gdzie jestem zawdzięczam swoim klientkom i kursantkom, które odwiedzają mnie z najdalszych zakątków Polski, ale także z
                zagranicy. Jest mi niezmiernie miło, że zdrowa forma opalania jaką jest właśnie opalanie natryskowe cieszy się z roku na
                rok coraz większym zainteresowaniem.

                </p>
                <div class="row social">
                    <div class="col-md-6">
                        <span>
                            <a href="https://www.facebook.com/MonikaNaskreckaOpalanieNatryskowe" target="_blank">
                                <i class="fa fa-facebook"></i> /MonikaNaskrecka</a>
                        </span>
                    </div>
                    <div class="col-md-6">
                        <span>
                            <a href="https://www.instagram.com/naskrecka.opalanie/" target="_blank">
                                <i class="fa fa-instagram"></i> /naskrecka.opalanie
                            </a>
                        </span>
                    </div>
                </div>
            </div>
<div class="col-md-6">
    <img src="{{asset('img/szkolenie/szkolenie_img3.png')}}" alt="">
</div>
        </div>

        <div class="row text-left">
            <div class="col-md-6">
                <img src="{{asset('img/szkolenie/szkolenie_img4.png')}}" alt="">
            </div>
            <div class="col-md-6 no-padding">
                <h3>
                    Agnieszka / Piekary Śląskie
                </h3>
                <p>
                    Kilka lat temu zaufałam własnej intuicji. Postawiłam na opalanie natryskowe. Wiedziałam, że ta forma opalania jest
                    bezpieczna dla naszego zdrowia, a efekt może być jak po najlepszych wakacjach w życiu.
                    <br><br>
                    Zajmuję się opalaniem od 2016 roku, a już od 2017 zaczęłam swoje pierwsze szkolenia. Od tego czasu opaliłam tysiące
                    pięknych ciał i przeszkoliłam setki ekspertów w Polsce i Europie. Kocham to co robię, to moja praca marzeń. Swoją pasję
                    przekazuję dalej na szkoleniach, doszkoleniach, ale również oczywiście swoim klientom, którzy lubią do mnie wracać, nie
                    tylko przed ważną okazją, ale na co dzień, gdy potrzebują poprawy samopoczucia.
                    <br><br>
                    Przez 3 lata przemierzałam drogi całego Śląska (i nie tylko), pracując mobilnie. Zdobywając coraz większe doświadczenie
                    oraz zaufanie klienta, odważyłam się w 2019 na otwarcie własnego salonu w moim mieście rodzinnym Piekarach Śląskich i
                    tym samym przekształciłam swoją usługę w stacjonarną. Od 2020 podjęłam jedną z lepszych decyzji biznesowych tj. podjęłam
                    współpracę z marką TanExpert. Pasję opalania łączę z mediami społecznościowymi, co skutkuje od początku sporą
                    rozpoznawalnością mojej własnej marki – Bursztynowej.

                </p>
                <div class="row social">
                    <div class="col-md-6">
                        <span>
                            <a href="https://www.facebook.com/bursztynowaopalanie">
                                <i class="fa fa-facebook"></i> /bursztynowaopalanie
                            </a>
                        </span>
                    </div>
                    <div class="col-md-6">
                        <span>
                            <a href="https://www.instagram.com/bursztynowa_opalanie/">
                                <i class="fa fa-instagram"></i> /bursztynowa_opalanie
                            </a>
                        </span>
                    </div>
                </div>
            </div>
            
        </div>


       


        <div class="row text-left">
           
            <div class="col-md-6 no-padding">
                <h3>
                    Maja / Bydgoszcz
                </h3>
                <p>
                    Historia miłosna jak każda inna – ja, słowianka o półprzezroczystej skórze w kolorze nieskazitelnej bieli. Nie, tu
                    jeszcze miłości nie ma 😉 Samoopalacze, balsamy brązujące, suplementy diety, soki marchwiowe - krócej byłoby mówić czego
                    nie próbowałam, by z bielą skóry wziąć rozwód na dobre. Zamiast tego była separacja - na każdych wakacjach - biel szła w
                    odstawkę, a pierwsze skrzypce grała czerwień. Na pierwsze opalanie trafiłam do wirtuoza pistoletu natryskowego -
                    magicznej Kasi z You Make Me Tan. Od razu wiedziałam, że „ja też tak chcę”. Wtedy, niczym rycerz na białym koniu pojawił
                    się ON - MineTan w złotej zbroi TanExpertu. Miłość od pierwszego wejrzenia. Chwila na zastanowienie, a później w
                    przypływie emocji szybka decyzja - zostanę ekspertką opalania! Nawet nie wiem jak i kiedy - zamiast tylko opalać
                    zaczęłam też prowadzić szkolenia - i oto jestem, w gronie moich ukochanych Masterek - Moniki Naskręckiej (mojej
                    nauczycielki ❤ ), Agi „Bursztynowej”, Kasi z YouMakeMeTan (która mnie zaraziła miłością do opalania) i Natalki z
                    Kurkowej5 &lt;3 Nie wiem co w tej pracy kocham bardziej: cudowny efekt w 15 minut, minę moich cudownych klientek po dojściu
                        do lustra („wyglądam jakbym 5 kg schudła!”) czy moje fantastyczne kursantki, które pałają miłością do zdrowego
                        opalania tak jak ja. Kiedyś się dowiem, który z tych trzech czynników sprawia, że chcę się rozwijać, opalać i
                        przekazywać wiedzę. Przeciwskazanie do zabiegu jest jedno - należy pamiętać, że to uzależnienie bez skutków
                        ubocznych.
                </p>
                <div class="row social">
                    
                        <div class="col-md-6">
                            <span>
                                <a href="https://www.facebook.com/Tan-Me-Better-opalanie-natryskowe-106411804439996">
                                    <i class="fa fa-facebook"></i> /Tan-Me-Better
                                </a>
                            </span>
                        </div>
                        <div class="col-md-6">
                            <span>
                                <a href="https://www.instagram.com/tan_me_better/">
                                    <i class="fa fa-instagram"></i> /tan_me_better
                                </a>
                            </span>
                        </div>
                    </div>
            </div>
        <div class="col-md-6">
            <img src="{{asset('img/szkolenie/szkolenie_img6.png')}}" alt="">
        </div>
        </div>

        <div class="row szkolenie_wizyta">
            <h1>
                Zainteresowany szkoleniem?
            </h1>
            <h6>
                Jesteś zdecydowana i chcesz zacząć swój biznes z nami?
            </h6>
            <p>
                Pierwszym etapem będzie profesjonalne szkolenie.
                Skontaktuj się z nami aby wskazać Ci najbliższego dostępnego szkoleniowca.
            </p>
            <a href="{{route('kontakt_index')}}" class="btn btn-primary">Umów się na wizytę</a>
        </div>
    </div>
</section>
@endsection