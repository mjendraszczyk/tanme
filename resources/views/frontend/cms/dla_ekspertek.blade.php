@extends('layouts.app')

@section('meta_seo')
<title>Dla Ekspertek - TanMe.pl</title>
@endsection

@section('body')
dla_ekspertek
@endsection

@section('content')
<section class="block">

    <div class="container">
        <h1>Dla Ekspertek</h1>
        <div class="row">
            <div class="col-md-6 flex-middle">
                <h1 class="text-left">
                    Świadczysz usługi w zakresie opalania natryskowego?
                </h1>
                <p class="text-left">
                    Dodaj bezpłatnie swój profil w naszym serwisie i bądź bardziej widoczna dla swoich klientów.
                    To zajmie tylko parę minut
                    :)
                </p>
                <a href="{{route('rejestracja_krok1')}}" class="btn btn-primary">Załóz bezpłatne konto</a>
            </div>
            <div class="col-md-6">
                <img src="{{asset('img/dla_ekspertek_obrazek.png')}}" alt="">
            </div>
        </div>
    </div>
    <div class=" row">
        <div class="ekspertki_korzysci">
            <div class="container">
                <h1>
                    Co zyskujesz dzięki Tan-Me?
                </h1>
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-4 korzysci-box">
                        <img src="./img/dla_ekspertek_bezplatna_wizytowka_www.png"
                            data-hover="{{asset('/img/dla_ekspertek_bezplatna_wizytowka_www_w.png')}}" alt="">
                        <h3>
                            Bezpłatna wizytówka www
                        </h3>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-4 korzysci-box">
                        <img src="./img/dla_ekspertek_adres_w_domenie.png"
                            data-hover="{{asset('/img/dla_ekspertek_adres_w_domenie_w.png')}}" alt="">
                        <h3>
                            Adres w domenie .tanme.pl
                        </h3>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-4 korzysci-box">
                        <img src="./img/dla_ekspertek_darmowa_reklama.png"
                            data-hover="{{asset('/img/dla_ekspertek_darmowa_reklama_w.png')}}" alt="">
                        <h3>
                            Darmową
                            reklamę
                        </h3>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-4 korzysci-box">
                        <img src="./img/dla_ekspertek_mozliwosc_opinii.png"
                            data-hover="{{asset('/img/dla_ekspertek_mozliwosc_opinii_w.png')}}" alt="">
                        <h3>
                            Możliwość
                            zbierania opinii
                        </h3>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-4 korzysci-box">
                        <img src="{{asset('/img/dla_ekspertek_dedykowane_rabaty.png')}}"
                            data-hover="{{asset('/img/dla_ekspertek_dedykowane_rabaty_w.png')}}" alt="">
                        <h3>
                            Dedykowane
                            rabaty
                        </h3>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-4 korzysci-box">
                        <a href="{{route('rejestracja_krok1')}}">
                            <img src="{{asset('/img/dla_ekspertek_zaloz_bezplatne_konto.png')}}"
                                data-hover="{{asset('/img/dla_ekspertek_zaloz_bezplatne_konto_w.png')}}" alt="">
                            <h3>
                                Załóż bezpłatne
                                konto
                            </h3>
                        </a>
                    </div>
                </div>
            </div>

        </div>

    </div>

    </div>

</section>
@endsection