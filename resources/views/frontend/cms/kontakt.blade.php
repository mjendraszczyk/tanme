@extends('layouts.app')

@section('meta_seo')
<title>TanExpert - Kontakt</title>
@endsection

@section('body')
kontakt
@endsection
@section('content')
<section class="block">

    <div class="container">
        <h1>Masz pytania</h1>
        <h2>
            Jestesmy do Twojej dyspozycji
        </h2>
        <p>
            <a href="#">pomoc&commat;{{env('APP_DOMAIN')}}</a>
        </p>
        <p>
            +48 533 709 708
        </p>
        <p>
            Pon-pt: 9:00 - 17:00
        </p>


    </div>
    <div class="row">
        <div class="kontakt_formularz">
            <div class="container">
                <h1>
                    Napisz do nas
                </h1>
                <form method="POST" action={{route('kontakt_store')}}>
                    @csrf
                    @include('backend/_main/message')
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" name="imie" placeholder="Imię i nazwisko" id="imie">
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="email" placeholder="E-mail" id="email">
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="telefon" placeholder="Telefon" id="telefon">
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="temat" placeholder="Temat" id="temat">
                        </div>
                        <div class="col-md-12">
                            <textarea name="wiadomosc" placeholder="Wiadomość" id="wiadomosc"></textarea>
                        </div>
                        <button class="btn btn-primary">Wyślij</button>
                </form>

            </div>
        </div>

    </div>

    </div>

</section>
@endsection