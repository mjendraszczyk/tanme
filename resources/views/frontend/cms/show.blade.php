@extends('layouts.app')

@section('meta_seo')
<title>TanExpert -
    @foreach($cms as $item)
    {{$item->title}}
    @endforeach
</title>
@endsection

@section('body')
kontakt
@endsection
@section('content')
<section class="block">

    <div class="container">
        @foreach($cms as $item)
        <h1>{{$item->title}}</h1>
        <div style="text-align: initial;">
            {!!$item->content!!}
        </div>
        @endforeach


    </div>

</section>
@endsection