@php echo '<?xml version="1.0" encoding="UTF-8"?>'; @endphp
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    @foreach($routes_array as $key => $route)
    <url>
        <loc>{{$route}}</loc>
        <lastmod>{{date('Y-m-d')}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>1</priority>
        </url>
    @endforeach
    @foreach($getEksperci as $ekspert)
    <url>
        <loc>{{route('profil_alias',['alias'=>$ekspert->business])}}</loc>
        <lastmod>{{date('Y-m-d')}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
        </url>
    @endforeach



@foreach($getMiasta as $miasto)
    <url>
        <loc>{{route('eksperts_by_filters',['wojewodztwo'=>$miasto->id_province, 'wojewodztwo_name' => str_slug(\App\Http\Controllers\Controller::getProvinceName($miasto->id_province)), 'miasto' => $miasto->id_city, 'miasto_name' => str_slug($miasto->name),'filtr' => '0'])}}</loc>
        <lastmod>{{date('Y-m-d')}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
        </url>
    @endforeach

    
@foreach($getBlog as $blog)
    <url>
        <loc>{{route('blog_show',['id'=>$blog->id_blog, 'title'=>str_slug($blog->title)])}}</loc>
        <lastmod>{{date('Y-m-d')}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
        </url>
    @endforeach

    @foreach($getCms as $cms)
    <url>
        <loc>{{route('cms_show',['id'=>$cms->id_cms, 'title'=>str_slug($cms->title)])}}</loc>
        <lastmod>{{date('Y-m-d')}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
        </url>
    @endforeach
</urlset>