@extends('layouts.app')

@section('meta_seo')
<title>
    @foreach ($ekspert as $item)
    {{$item->name}}
    @endforeach - TanMe.pl
</title>
@endsection

@section('body')dla_ekspertek @endsection
@section('content')

<section class="block">

    <div class="container">
        <div class="row">
            <div class="col-md-8 flex-middle">
                <div class="row">
                    <div class="col-md-7" style="text-align: left;padding: 0;">
                        @foreach ($ekspert as $item)
                        @if($item->feature == 1)
                        <div style="background: rgb(230, 216, 163) none repeat scroll 0% 0%; color: rgb(255, 255, 255); padding: 15px; text-transform: uppercase;max-width: 200px;left: 0px;top: 0px;"
                            class="label-cert gold"><span style="margin: 3px 0px 0px; display: inline-block;">Szkoleniowiec</span> </div>
                        @endif
                        <h1 style="display: inline-block;vertical-align: middle;">
                            {{$item->name}}
                        </h1>
                        @endforeach
                        @for($i=0;$i<$rate;$i++) <img src="{{asset('/img/ekspertka_gwiazda.png')}}" alt="">
                            @endfor
                    </div>
                    <div class="col-md-5">
                        @if($item->cert == 1)
                        <div class="label"
                            style="margin-right: -15px;background: #000;color: #fff;padding: 15px 15px;text-transform: uppercase;">
                            <span style="margin: 3px 0 0 0;display: inline-block;">
                                Certyfikat
                            </span>
                            <img src="{{asset('/img/glowna_tanexpert.png')}}" alt=""
                                style="display: inline-block;width: 50%;vertical-align: bottom;padding: 0 10px;">
                        </div>
                        @endif
                    </div>
                </div>
                <p style="text-align: left;">
                    <span class="fa fa-map-marker"></span>
                    @foreach ($ekspert as $item)
                    @if($item->address == '')
                    <em>Nie podano</em>
                    @else
                    {{$item->address}}
                    @endif
                    @endforeach
                </p>
                <div class="ekspert_image"
                    style="background-image: url({{asset('/img/ekspert')}}/{{$item->feature_image}});">
                    @if($item->feature_image == '')
                    {{-- <img src="{{asset('/img/ekspert')}}/{{$item->feature_image}}"
                    alt=""> --}}
                    <img src="{{asset('/img')}}/image.svg" style="width:200px;" alt="">
                    @endif
                </div>
                <div class="ekspertka_content_description">
                    <div class="description">
                        <h1>Cennik</h1>
                        @foreach ($categories as $c)
                        @if(App\Http\Controllers\Controller::countServicesFromCategory($c->id_category,$item->id_ekspert)
                        > 0)
                        <h6>
                            {{$c->name}}
                        </h6>
                        @endif
                        <table>
                            @foreach (App\Http\Controllers\Controller::getServicesByCategory($c->id_category,
                            $item->id_ekspert) as
                            $service)
                            @if($service->price != '')
                            <tr>
                                <td>
                                    {{$service->name}}
                                </td>
                                <td>
                                    {{$service->price}}
                                </td>
                            </tr>
                            @endif
                            @endforeach
                        </table>
                        @endforeach
                        {{-- @foreach ($getCennik as $item)
                        {{$item->price}}
                        @endforeach --}}
                        {{-- @foreach ($ekspert as $item)
                        {!! $item->content !!}
                        @endforeach --}}
                    </div>
                    <div class="description">
                        <h1>Opis</h1>
                        @foreach ($ekspert as $item)
                        {{ $item->short_description }}
                        @endforeach
                    </div>

                    <div class="description">
                        <h1>Galeria</h1>
<div class="owl-carousel owl-theme">

    @foreach ($galleries as $gallery)
    <div class="item">
        <a href="{{asset('img/uploads/galleries')}}/{{$gallery->filename}}" data-fancybox="gallery">
            <img src="{{asset('img/uploads/galleries')}}/{{$gallery->filename}}" />
        </a>
    </div>
    @endforeach
</div>
                    </div>
                    
                    <div class="opinie">

                        <div class="komentarze">
                            {!!App\Http\Controllers\Controller::getWidgetOpinie($item->id_ekspert)!!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sidebar">
                    <h3>
                        Kontakt i godziny otwarcia
                    </h3>
                    <h6>

                        @if($item->phone == '')
                        @else
                        <span class="fa fa-phone"></span>
                        {{$item->phone}}
                        @endif
                    </h6>
                    {{-- {{print_r($opening)}} --}}
                    <table>
                        @foreach($opening as $o)
                        <tr>
                            <td>{{\App\Http\Controllers\Controller::getDays()[$o->dzien]}}</td>
                            <td>
                                @if(($o->godzina_od == '') || ($o->godzina_do == ''))
                                Zamknięte
                                @else
                                {{date('H:i', strtotime($o->godzina_od))}}
                                - {{date('H:i',strtotime($o->godzina_do))}}
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    <input type="hidden" name="lat" value="{{$item->lat}}" id="lat">
                    <input type="hidden" name="lon" value="{{$item->lon}}" id="lon">
                    <div id="map">

                    </div>
                    @if(($item->typ == 1) || ($item->typ == 3))
                    <div class="call_to_action_ekspert">
                        <h3>
                            Ekspert mobilny
                        </h3>
                    </div>
                    @endif
                    @if(($item->typ == 0) || ($item->typ == 3))
                    <div class="call_to_action_shop">
                        <h3>
                            Salon stacjonarny
                        </h3>
                    </div>
                    @endif
                    <h3>
                        Znajdź nas na
                    </h3>
                    <div class="social">
                        <a href="//{{$item->facebook}}" target="_blank">
                            <span class="fa fa-facebook"></span>
                        </a>
                        <a href="//{{$item->instagram}}" target="_blank">
                            <span class="fa fa-instagram"></span>
                        </a>
                        <a href="//{{$item->youtube}}" target="_blank">
                            <span class="fa fa-youtube"></span>
                        </a>
                    </div>
                    <img src="" alt="">
                    {{-- mailto:{{$item->expert_email}} --}}
                    {{-- <a href="#" data-toggle="modal" data-target="#ModalKontakt" class="btn btn-secondary">Wyślij
                        zapytanie</a> --}}

                </div>
            </div>
        </div>
    </div>



</section>

@endsection