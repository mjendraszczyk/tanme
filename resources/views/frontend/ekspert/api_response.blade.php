{{-- @section('meta_seo')
<title>TanExpert -
    @foreach ($ekspert as $item)
    {{$item->name}}
@endforeach
</title>
@endsection --}}

@foreach($ekspert as $key => $item)
<div class="row label">
    <div class="col-md-4">
        <a href="{{route('profil_alias',['business'=>$item->business])}}">
            <div class="ekspert_feature image_placeholder"
                style="background-image: url({{asset('/img/ekspert')}}/{{$item->feature_image}});">
                @if($item->feature_image == '')
                {{-- <img src="{{asset('/img/ekspert')}}/{{$item->feature_image}}"
                alt=""> --}}
                <img src="{{asset('/img')}}/image.svg" style="width:100px;" alt="">
                @endif
                {{-- <img src="{{asset('/img/ekspert')}}/{{$item->feature_image}}" alt=""> --}}
            </div>
        </a>
    </div>
    <div class="col-md-6">
        @if($item->feature == 1)
        <div style="background: rgb(230, 216, 163) none repeat scroll 0% 0%; color: rgb(255, 255, 255); padding: 15px; text-transform: uppercase;max-width: 200px;left: -15px;top: -27px;"
            class="label-cert gold"><span style="margin: 3px 0px 0px; display: inline-block;">Szkoleniowiec</span> </div>
        @endif
        <h1><a href="{{route('profil_alias',['id'=>$item->business])}}" style="color:inherit;">{{$item->name}}</a></h1>
        <p>
            {{$item->address}} {{\App\Http\Controllers\Controller::getCityName($item->city)}}
        </p>
    </div>
    <div class="col-md-2">
        <div class="ocena">
            <p>{{App\Http\Controllers\frontend\EkspertController::avgReview($item->id_ekspert)}}</p>
            <p>
                {{App\Http\Controllers\frontend\EkspertController::countReview($item->id_ekspert)}} opini
            </p>
        </div>
        @if($item->cert == 1)
        <div class="label-cert" style="background: #000;color: #fff;padding: 15px 15px;text-transform: uppercase;">
            <span style="margin: 3px 0 0 0;display: inline-block;">
                Certyfikat
            </span>
            <img class="certyfikat" src="{{asset('/img/glowna_tanexpert.png')}}" alt=""
                style="display: inline-block;width: 50%;vertical-align: bottom;padding: 0 10px;">
        </div>
        @endif
        @if(($item->typ == 1) || ($item->typ == 3))
        <img class="ekpert_icon" src="{{asset('/img/glowna_auto_icon.png')}}" alt="">
        @endif
        @if(($item->typ == 0) || ($item->typ == 3))
        <img class="ekpert_icon" src="{{asset('/img/glowna_sklep_icon.png')}}" @if($item->typ == 3) style="margin-right:50px;" @endif alt="">
        @endif
    </div>

</div>
@endforeach
{{ $ekspert->links() }}
@if($seoWojewodztwo)
<div class="seo-content">
    <p>
        @if($seoMiasto)
        {!!$seoMiasto->content!!}
        @else
        {!!$seoWojewodztwo->content!!}
        @endif
    </p>
</div>
<input type="hidden" id="wojewodztwo_name" name="wojewodztwo_name" value="{{str_slug($seoWojewodztwo->name)}}">
<input type="hidden" id="miasto_name" name="miasto_name" value="{{str_slug(@$seoMiasto->name)}}">
@endif