@extends('layouts.app')

@section('meta_seo')
<title>Opalanie natryskowe
    @if($seoWojewodztwo)
        @if($seoWojewodztwo->name == '')
        wszystkie
        @else
            @if($seoMiasto)
            @else
            {{($seoWojewodztwo->name)}}
            @endif
        @endif
        @if($seoMiasto)
            @if($seoMiasto->name)
            {{($seoMiasto->name)}}
            @else
            @endif
        @endif
    @endif
    - TanMe
</title>
@endsection

@section('body')dla_ekspertek @endsection
@section('content')
<section class="block">

    <div class="container">
        <div class="filtry">
            <h1>Opalanie natryskowe
            </h1>
            <form method="POST" action="{{route('eksperts_by_filters_request')}}">
                @csrf
                <div class="row">
                    <div class="col-md-4">
                        <label>Województwo</label>
                        <select class="customselect2 zmien_miasta" name="wojewodztwo">
                            <option value="" data-province="0">--</option>
                            @foreach(App\Http\Controllers\Controller::getProvinces() as $item)
                            <option value="{{mb_strtolower($item->id_province)}}" data-province="{{$item->id_province}}"
                                @if($wojewodztwo==($item->name)) selected="selected"
                                @endif>{{mb_strtolower($item->name)}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label>Miasto</label>
                        <select class="miastaselect" name="miasto" disabled>
                            <option value="">--</option>
                            @foreach (App\Http\Controllers\Controller::getCities() as $item)
                            <option value="{{$item->id_city}}" @if($miasto==$item->id_city) selected="selected"
                                @endif>{{$item->name}}</option>
                            @endforeach
                        </select>
                        <input type="hidden" name="miasto_store" value="{{$miasto}}">
                    </div>
                    <div class="col-md-4">
                        <label>Sortowanie</label>
                        <select class="customselect2" name="filtr">
                            <option value="0" @if($filtr==0) selected="selected" @endif>--</option>
                            <option value="1" @if($filtr==1) selected="selected" @endif>Eksperci mobilni</option>
                            <option value="2" @if($filtr==2) selected="selected" @endif>Eksperci certyfikowani</option>
                            <option value="3" @if($filtr==3) selected="selected" @endif>Salony stacjonarne</option>
                            <option value="4" @if($filtr==4) selected="selected" @endif>Najnowsze</option>
                        </select>
                    </div>
                </div>
                {{-- <button type="submit" style="margin-top: 10px;" class="btn btn-primary">Filtr</button> --}}
            </form>
        </div>
    </div>

    <div class="grey_block">
        <div class="container">
            {{-- TEST:
                    <div id="app"> --}}
            <div class="ekspert-container">
                @foreach($ekspert as $key => $item)
                <div class="row label">
                    <div class="col-md-4">
                        <a href="{{route('profil_alias',['business'=>$item->business])}}">
                            <div class="ekspert_feature image_placeholder"
                                style="background-image: url({{asset('/img/ekspert')}}/{{$item->feature_image}});">
                                @if($item->feature_image == '')
                                {{-- <img src="{{asset('/img/ekspert')}}/{{$item->feature_image}}"
                                alt=""> --}}
                                <img src="{{asset('/img')}}/image.svg" style="width:100px;" alt="">
                                @endif
                                {{-- <img src="{{asset('/img/ekspert')}}/{{$item->feature_image}}" alt=""> --}}
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6">
                        @if($item->feature == 1)
                        <div style="background: rgb(230, 216, 163) none repeat scroll 0% 0%; color: rgb(255, 255, 255); padding: 15px; text-transform: uppercase;max-width: 200px;left: -15px;top: -27px;"
                            class="label-cert gold"><span style="margin: 3px 0px 0px; display: inline-block;">Szkoleniowiec</span> </div>
                        @endif
                        <h1><a href="{{route('profil_alias',['id'=>$item->business])}}"
                                style="color:inherit;">{{$item->name}}</a></h1>
                        <p>
                            {{$item->address}} {{\App\Http\Controllers\Controller::getCityName($item->city)}}
                        </p>
                    </div>
                    <div class="col-md-2">
                        <div class="ocena">
                            <p>{{App\Http\Controllers\frontend\EkspertController::avgReview($item->id_ekspert)}}</p>
                            <p>
                                {{App\Http\Controllers\frontend\EkspertController::countReview($item->id_ekspert)}}
                                opini
                            </p>
                        </div>
                        @if($item->cert == 1)
                        <div class="label-cert"
                            style="background: #000;color: #fff;padding: 15px 15px;text-transform: uppercase;">
                            <span style="margin: 3px 0 0 0;display: inline-block;">
                                Certyfikat
                            </span>
                            <img class="certyfikat" src="{{asset('/img/glowna_tanexpert.png')}}" alt=""
                                style="display: inline-block;width: 50%;vertical-align: bottom;padding: 0 10px;">
                        </div>
                        @endif
                        @if(($item->typ == 1) || ($item->typ == 3))
                        <img class="ekpert_icon" src="{{asset('/img/glowna_auto_icon.png')}}" alt="">
                        @endif
                        @if(($item->typ == 0) || ($item->typ == 3))
                        <img class="ekpert_icon" src="{{asset('/img/glowna_sklep_icon.png')}}" @if($item->typ == 3) style="margin-right:50px;" @endif alt="">
                        @endif
                    </div>

                </div>
                @endforeach

                {{ $ekspert->links() }}
                @if($seoWojewodztwo)
                <div class="seo-content">
                    <p>
                        @if($seoMiasto)
                        {!!$seoMiasto->content!!}
                        @else
                        {!!$seoWojewodztwo->content!!}
                        @endif
                    </p>
                </div>
                @endif
            </div>
        </div>
    </div>
    @endsection