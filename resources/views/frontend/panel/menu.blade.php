<div class="col-md-4">
    <div class="sidebar">
        <h1 class="text-left">
            {{-- {{Auth::user()->business}} --}}
            {{$ekspert->name}}
        </h1>
        <ul class="nav">
            @if($ekspert->feature_image != '')
            <img src="{{asset('img/ekspert')}}/{{$ekspert->feature_image}}" class="avatar" alt="">
            @else
            <img src="{{asset('img/ekspertka_user.png')}}" class="avatar" alt="">
            @endif
            <li class="nav-item">
                <a href="{{route('profil_alias',['id'=>$ekspert->business])}}" target="_blank"><span
                        class="material-icons">
                        person
                    </span> Zobacz profil</a>
            </li>
            <li class="nav-item">
                <a href="{{route('panel_index')}}"><span class="material-icons">
                        edit
                    </span> Edycja danych</a>
            </li>
            <li class="nav-item">
                <a href="{{route('opening_index')}}"><span class="material-icons">
                        watch
                    </span> Godziny otwarcia</a>
            </li>
            <li class="nav-item">
                            <a href="{{route('frontend_galeria_index')}}"> <span class="material-icons">
                                    photo
                                </span> Galeria</a>
                        </li>
            <li class="nav-item">
                <a href="{{route('services_index')}}"><span class="material-icons">
                        miscellaneous_services
                    </span> Usługi</a>
            </li>

            <li class="nav-item">
                <form action="{{route('logout')}}" method="post">
                    @csrf
                    <button type="submit" class="logout"><span class="material-icons">
                            lock_open
                        </span> Wyloguj</button>
                </form>
            </li>
        </ul>
    </div>
</div>