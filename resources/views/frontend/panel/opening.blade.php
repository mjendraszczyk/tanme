@extends('layouts.app')
@section('meta_seo')
<title>
    TanFinder - Panel użytkownika - Godziny otwarcia
</title>
@endsection

@section('content')
<div class="container dashboard">
    <div class="row">
        @include('frontend.panel.menu')
        <div class="col-md-8">
            <form action="{{route('opening_update')}}" class="text-left" method="POST">
                @csrf
                @method('PUT')
                <h1>Godziny otwarcia</h1>
                {{-- T: {{$countEkspert}} --}}
                @if($countEkspert == 0)
                @else
                <div class="row panel godziny">

                    @for($j=1;$j<=7;$j++) <div class="row">
                        <div class="col-md-3">
                            {{App\Http\Controllers\Controller::getDays()[$j]}}
                        </div>

                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-6">
                                    {{-- {{print_r($opening[0]['godzina_od'])}} --}}
                                    <select name="godzina_od[]">
                                        <option value="">-- Zamknięte --</option>
                                        @for($i=0;$i<24;$i++) <option value="{{$i}}:00:00"
                                            @if($opening[$j-1]['godzina_od'] !=null)
                                            @if((str_contains($opening[$j-1]['godzina_od'], $i.':00:00')))
                                            selected="selected" @endif @endif>
                                            {{$i}}:00
                                            </option>
                                            @endfor

                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <select name="godzina_do[]">
                                        <option value="">-- Zamknięte --</option>
                                        @for($i=0;$i<24;$i++) <option value="{{$i}}:00:00"
                                            @if($opening[$j-1]['godzina_do'] !=null)
                                            @if((str_contains($opening[$j-1]['godzina_do'], $i.':00:00')))
                                            selected="selected" @endif @endif>{{$i}}:00
                                            </option>
                                            @endfor

                                    </select>
                                </div>
                            </div>
                        </div>
                </div>
                @endfor
                <button type="submit" class="btn  waves-effect waves-light btn-large">
                    Zapisz</button>
        </div>
        @endif
        </form>
    </div>
</div>
</div>
@endsection