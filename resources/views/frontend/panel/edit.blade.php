<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>

<form action="{{route('panel_update',['id'=>$ekspert->id_ekspert])}}" id="form_companies" class="text-left" method="POST"
    enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="col s12 m12 l12">
        <label>Nazwa</label>

        <input placeholder="Nazwa" name="name" @if(Route::currentRouteName()=='panel_index' ) value="{{$ekspert->name}}"
            @else value="" @endif type="text" class="form-control validate">


        <label>
            {{-- {{dd(old('typ'))}} --}}
            <input type="checkbox" class="inlineblock w-auto" name="typ[]" value="0" id="" @if(($ekspert->typ == 0) ||
            ($ekspert->typ == 3)) checked="checked" @endif>Salon
            stacjonarny</label>
        <label>
            <input type="checkbox" class="inlineblock w-auto" name="typ[]" value="1" id="" @if(($ekspert->typ == 1) ||
            ($ekspert->typ == 3)) checked="checked" @endif>Ekspertka
            mobilna</label>

        <div class="row panel" style="margin:15px 0;">
            <label>Województwo</label>
            <select class="form-control customselect2 zmien_miasta" required name="id_province">
                <option value="">-- Województwo --</option>
                @foreach (App\Http\Controllers\Controller::getProvinces(null) as
                $wojewodztwo)
                <option @if($ekspert->province == $wojewodztwo->id_province) selected="selected"
                    @endif
                    value="{{$wojewodztwo->id_province}}">{{mb_strtolower($wojewodztwo->name)}}
                </option>
                @endforeach
            </select>

            <div class="clearfix"></div>
            <label>Miasto</label>
            <select class="miastaselect form-control" name="miasto" disabled>
                <option value="">--</option>
                @foreach (App\Http\Controllers\Controller::getCities() as $item)
                <option value="{{$item->id_city}}" @if($ekspert->city==$item->id_city)
                    selected="selected"
                    @endif>{{$item->name}}</option>
                @endforeach
            </select>
            <input type="hidden" name="miasto_hidden" />

        </div>

        {{-- <label class="input_radio">
            <input type="radio" class="inlineblock w-auto" @if($ekspert->typ == 0) checked="checked" @endif name="typ"
            value="0" id="">Salon stacjonarny</label>
        <label class="input_radio">
            <input type="radio" class="inlineblock w-auto" @if($ekspert->typ == 1) checked="checked" @endif name="typ"
            value="1" id="">Ekspertka mobilna</label> --}}


        <label>Adres</label>

        <input placeholder="Adres" id="adres" name="address" @if(Route::currentRouteName()=='panel_index' )
            value="{{$ekspert->address}}" @else value="" @endif type="text" class="form-control validate">


        <label>Facebook</label>

        <input placeholder="Facebook" name="facebook" @if(Route::currentRouteName()=='panel_index' )
            value="{{$ekspert->facebook}}" @else value="" @endif type="text" class="form-control validate">

        <label>Instagram</label>

        <input placeholder="Instagram" name="instagram" @if(Route::currentRouteName()=='panel_index' )
            value="{{$ekspert->instagram}}" @else value="" @endif type="text" class="form-control validate">
        <label>YouTube</label>

        <input placeholder="YouTube" name="youtube" @if(Route::currentRouteName()=='panel_index' )
            value="{{$ekspert->youtube}}" @else value="" @endif type="text" class="form-control validate">

        <label>E-mail</label>
        <input placeholder="E-mail" name="expert_email" @if(Route::currentRouteName()=='panel_index' )
            value="{{$ekspert->expert_email}}" @else value="" @endif type="email" class="form-control validate">

        <label>
            <input type="checkbox" class="inlineblock w-auto" name="cert" value="1" id="" @if($ekspert->cert =='1' )
            checked="checked" @endif>Posiadam szkolenie TanExpert
            Polska</label>

        <label>Telefon</label>
        <input placeholder="Telefon" name="phone" @if(Route::currentRouteName()=='panel_index' )
            value="{{$ekspert->phone}}" @else value="" @endif type="text" class="form-control validate">

        <label>Zdjęcie</label>
        <div class="file-field">
            <input type="file" accept="image/*" class="image" name="ekspert_upload">
            <input type="hidden" id="croped" name="croped" />
        </div>
@if(Route::currentRouteName()=='panel_index' )
@if($ekspert->feature_image == '')

<img id="output" src="{{asset('img/ekspertka_user.png')}}" class="thumbnail" style="max-width:100%;clear: both;
        display: block;margin:10px 0;" />
@else
<img id="output" src="{{asset('img/ekspert/'.$ekspert->feature_image)}}" class="thumbnail" style="max-width:100%;clear: both;
        display: block;margin:10px 0;" />
        @endif
@endif
        {{-- <label>Opis</label>
        @if(Route::currentRouteName()=='panel_index' )
        <textarea name="opis" placeholder="Wpisz treść ogloszenia"
            class="editor form-control materialize-textarea validate">{{$ekspert->content}}</textarea>
        @else
        <textarea name="opis" placeholder="Wpisz treść ogloszenia"
            class="editor form-control materialize-textarea validate">{{old('content')}}</textarea>
        @endif --}}

        <label>Opis krótki</label>
        @if(Route::currentRouteName()=='panel_index' )
        <textarea name="short_description" placeholder="Wpisz treść ogloszenia"
            class="form-control materialize-textarea validate">{{$ekspert->short_description}}</textarea>
        @else
        <textarea name="short_description" placeholder="Wpisz treść ogloszenia"
            class="form-control materialize-textarea validate">{{old('short_description')}}</textarea>
        @endif
        <label>Wybierz lokalizacje (Kliknij na mapie w celu zmiany punktu)</label>
        <div id="mapLatLon"></div>
        <input type="hidden" id="lat" value="{{$ekspert->lat}}" name="lat">
        <input type="hidden" id="lon" value="{{$ekspert->lon}}" name="lon">
    </div>
    <button id="profil_save" type="submit" class="btn  waves-effect waves-light btn-large">
        Zapisz</button>
</form>

<div class="modal" id="modalUpload" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Wykadruj zdjęcie
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <div class="row">
                        <div class="col-md-8">
                            <img id="image" src="">
                        </div>
                        <div class="col-md-4">
                            <div class="preview"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button> --}}
                <button type="button" class="btn btn-primary" id="crop">Wykadruj</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$.noConflict();
(function( $ ) {
$(function() {

var $modal = $('#modalUpload');
var image = document.getElementById('image');
var cropper;

$("body").on("change", ".image", function(e){
var files = e.target.files;
var done = function (url) {
    image.src = url;
$modal.modal('show');
};


var reader;
var file;
var url;
if (files && files.length > 0) {
file = files[0];
if (URL) {
done(URL.createObjectURL(file));
} else if (FileReader) {
reader = new FileReader();
reader.onload = function (e) {
done(reader.result);
};
reader.readAsDataURL(file);
}
}
});


$modal.on('shown.bs.modal', function () {
cropper = new Cropper(image, {
aspectRatio: 2,
viewMode: 1,
preview: '.preview'
});
}).on('hidden.bs.modal', function () {
cropper.destroy();
cropper = null;
});


$("#crop").click(function(){
canvas = cropper.getCroppedCanvas({
width: 800,
height: 360,
});
canvas.toBlob(function(blob) {
url = URL.createObjectURL(blob);
var reader = new FileReader();
reader.readAsDataURL(blob); 
reader.onloadend = function() {
var base64data = reader.result; 

$.ajax({
type: "POST",
dataType: "json",
url: "{{route('profil_image_croped')}}",
data: {'_token': $('meta[name="csrf-token"]').attr('content'), 'image': base64data},
success: function(data){
console.log(data);
$modal.modal('hide');
//alert("Crop image successfully uploaded");
$("#output").attr('src',"//"+data.path);
$("#croped").val(data.name);
}
});
}
});
})


// function AjaxCall() {
// $.ajaxSetup({
// headers: {
// "X-CSRF-TOKEN": $("meta[name=\"csrf-token\"]").attr("content")
// }
// });

// $.ajax({
// 
// method: 'POST',
// data : $("#form_companies").serialize(),
// beforeSend: function() {
// $('#form_companies').css('opacity',0.5);
// },
// success: function(data) {
// $.growl.notice({ title: "", message: "Zapisano pomyślnie!" });
// $('#form_companies').css('opacity',1);
// }
// })
// }
// More code using $ as alias to jQuery
});
})(jQuery);
</script>
{{-- url: '/backoffice/companies/{{\App\Http\Controllers\Controller::getCompanyId(Auth::user()->id)}}/edit', --}}