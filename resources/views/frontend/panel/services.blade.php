@extends('layouts.app')
@section('meta_seo')
<title>
    TanFinder - Panel użytkownika - Usługi
</title>
@endsection

@section('content')
<div class="container dashboard">
    <div class="row">
        @include('frontend.panel.menu')
        <div class="col-md-8">
            <form action="{{route('services_update')}}" class="text-left" method="POST">
                @csrf
                @method('PUT')
                <h1>Podaj swoje stawki</h1>
                @foreach ($services as $s)


                <div class="row label @if(App\Http\Controllers\Controller::getPriceDependsOnService($s->id_service, $ekspert->id_ekspert) == '') hidden @endif">
                    <div class="col-md-8">
                        <input style="position: absolute;
                                                                top: 15px;
                                                                left: -15px;
                                                                width: auto;" type="checkbox" 
                                                                @if(App\Http\Controllers\Controller::getPriceDependsOnService($s->id_service, $ekspert->id_ekspert) != '') checked="checked" @endif
                                                                class="usluga_item"
                                                                name="usluga_item[]">
                        <p style="padding: 0;
margin: 10px 0;">{{$s->name}}</p>
                        <input placeholder="Nazwa usługi" name="uslugi_nazwa[]"  value="{{$s->id_service}}"
                            type="hidden">
                    </div>
                    <div class="col-md-4">
                        <strong>
                            <input name="uslugi_ceny[]" class="uslugi_ceny"
                                style="padding: 0;"
                                value="{{App\Http\Controllers\Controller::getPriceDependsOnService($s->id_service, $ekspert->id_ekspert)}}"
                                type="number" min="0" @if(App\Http\Controllers\Controller::getPriceDependsOnService($s->id_service, $ekspert->id_ekspert) == '')
                                readonly="readonly" @endif>
                            <input class="checkbox_item" type="checkbox" checked="checked" name="uslugi_pozycja[]"
                                value="{{$s->id_service}}">
                        </strong>
                    </div>
                </div>
                @endforeach

                <button type="submit" class="btn  waves-effect waves-light btn-large">
                    Zapisz</button>

            </form>
        </div>
    </div>
</div>
@endsection