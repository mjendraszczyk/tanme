@extends('layouts.app')
@section('meta_seo')
<title>
    TanFinder - Panel użytkownika
</title>
@endsection

@section('content')
<div class="container dashboard">
    <div class="row">
        @include('frontend.panel.menu')
        <div class="col-md-8">
            <h1 class="text-left">Edycja danych</h1>
            <div class="panel user-panel">
                <div class="panel-body">
                    @include('frontend.panel.edit')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection