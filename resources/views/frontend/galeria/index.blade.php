﻿@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        @include('frontend.panel.menu')
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Galeria
                    <a href="{{route('frontend_galeria_create')}}" class="btn waves-effect waves-light btn-primary">
                        <i class="material-icons">add</i>
                        Dodaj</a>
                    <a href="{{route('frontend_galeria_index')}}" class="btn waves-effect waves-light btn-secondary">
                        <i class="material-icons">keyboard_backspace</i>
                        Powrót</a>
                       
                </div>

                <div class="card-body">
<table class="table">
    <thead>
        <th>ID</th>
        <th>Obrazek</th>
        <th>Nazwa</th>
        <th>Pozycja</th>
        <th class="text-right">Opcje</th>
    </thead>
@foreach ($galleries as $gallerie)
    <tr>
        <td>
            {{$gallerie->id_galeria}}
        </td>
        <td>
                   <img src="{{asset('img/uploads/galleries/'.$gallerie->filename)}}" class="img-thumbnail" style="width:120px;"/>
        </td>
        <td>
            {{$gallerie->label}}
        </td>
        <td>
            {{$gallerie->position}}
        </td>
      
        <td class="text-right">
            <a href="{{route('frontend_galeria_edit',['id'=>$gallerie->id_galeria])}}" class="btn btn-default">
                <i class="fa fa-edit"></i>
            </a>
            <form class="inline" method="POST" action="{{route('frontend_galeria_destroy',['id'=>$gallerie->id_galeria])}}">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">
                <i class="fa fa-remove"></i>
            </button>
            </form>
        </td>
    </tr>
@endforeach
</table>
{{ $galleries->links() }}
</div>
</div>
</div>
</div>
</div>
@endsection