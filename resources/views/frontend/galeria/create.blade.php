@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        @include('frontend.panel.menu')
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Galeria
                    <a href="{{route('frontend_galeria_index')}}" class="btn waves-effect waves-light btn-secondary">
                        <i class="material-icons">keyboard_backspace</i>
                        Powrót</a>
                </div>

                <div class="card-body">
    <form method="POST" enctype="multipart/form-data" action="{{route('frontend_galeria_store')}}">
        @include('frontend.galeria.form')
    </form>
</div>
            </div>
        </div>
        </div>
        </div>
@endsection