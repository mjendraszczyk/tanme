<div class="reviews-form">
    <h1>
        Opinie
    </h1>
    <p>
        Dbamy aby opinie w naszym serwisie były w 100% prawdziwe.
    </p>


    <form class="review_form" method="POST" action="{{route('review_request',['account'=>$ekspert->business])}}">
        @csrf
        @include('backend/_main/message')
        <span class="ratings"><i class="fa fa-star yellow add"></i> <i class="fa fa-star yellow add"></i> <i
                class="fa fa-star yellow add"></i> <i class="fa fa-star yellow add"></i> <i
                class="fa fa-star yellow add"></i></span>
        <input name="rate" type="hidden" value="5" id="ranga" class="col-sm-12
                form-control">
                <input class="form-control"
                required type="email" name="email" placeholder="E-mail">
        <input class="form-control" type="text" name="nick" placeholder="Imię i nazwisko">
        <textarea class="form-control" name="comment" placeholder="Treść"></textarea>
        @if(Auth::check())
        <input type="hidden" name="id_user" value="{{Auth::user()->id}}">
        @else
        <input type="hidden" name="id_user" value="">
        @endif
        <input type="hidden" name="id_ekspert" value="{{$id_ekspert}}">
        <input type="submit" class="btn btn-primary" value="Dodaj">
    </form>
</div>
<div class="reviews-content">
    @if(count($reviews) > 0)
    <ul>
        @foreach($reviews as $review)
        <li>
            <div class="row">
                <div class="col-md-2">
                    <div class="text-center">
                        <img src="{{asset('/img/ekspertka_user.png')}}" alt="">
                    </div>
                </div>
                <div class="col-md-8">
                    <h6>
                        {{$review->nick}}
                    </h6>
                    <p>
                        {{$review->comment}}
                    </p>
                </div>
                <div class="col-md-2">
                    <div class="komentarz-ocena">
                        @for($i=0;$i<$review->rate;$i++)
                            <img src="{{asset('/img/ekspertka_gwiazda.png')}}" alt="">
                            @endfor
                    </div>
                    <p class="komentarz-data">{{$review->created_at}}</p>
                </div>
            </div>
        </li>
        @endforeach
    </ul>

    @else
    <div class="alert">
        Brak opini
    </div>

    @endif
</div>