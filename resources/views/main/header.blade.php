{{-- <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
    integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
    crossorigin=""></script>
<script src="https://cdn.ckeditor.com/ckeditor5/25.0.0/classic/ckeditor.js"></script> --}}

<header @if(Route::currentRouteName()=='home' ) class="home" @endif>
    @if(Route::currentRouteName()=='home' )
<video autoplay="" muted="" loop=""
    style="position: absolute;right: 0;width: auto;height: 100%;z-index: 0;top: 0;transform: translateX(0%) translateY(0%);"
    width="100%" height="100%">
    {{-- <source src="https://v.ftcdn.net/04/39/66/31/700_F_439663186_p3BSsI5uWv3NMTLxRtj2WthYlqnP2ipi_ST.mp4"
        type="video/mp4"> --}}
        <source src="{{asset('video/slider.mp4')}}"
                type="video/mp4">
    Your browser does not support the video tag.
</video>
@endif
    <div class="row top">
        <div class="col-md-5">
            <a href="{{route('home')}}">
                <img src="{{asset('img/glowna_top_tanexpert_tanme.png')}}" alt="" class="mtop50">
            </a>
            {{-- <a href="//tanexpert.pl" target="_blank">
                <img src="{{asset('img/glowna_top_minetan.png')}}" alt="" class="mleft50">
            </a> --}}
        </div>
        <div class="col-md-7">
            <div class="nawigacja">

                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                        MENU
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                @if(Auth::check())
                                @if(Auth::user()->id_role == '2')
                                <a class="nav-link" href="{{route('backend_index')}}">
                                    <span class="fa fa-user"></span>
                                    Panel</a>
                                @else
                                <a class="nav-link" href="{{route('panel_index')}}">
                                    <span class="fa fa-user"></span>
                                    Panel</a>
                                @endif
                                @else
                                <a class="nav-link" href="#" data-toggle="modal" data-target="#ModalLogin">
                                    <span class="fa fa-user"></span>
                                    Zaloguj się / Załóż konto</a>
                                @endif
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('dla_ekspertek')}}">Dla Ekspertek</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{route('szkolenie_index')}}">Szkolenie</a>
                            </li>
                            <li class="nav-item">
                                    <a class="nav-link" href="\\tanexpert24.pl" target="_blank">Sklep</a>
                                </li>
                        </ul>

                    </div>
                </nav>

            </div>
        </div>
    </div>

    @if(Route::currentRouteName() == 'home')
    <div class="container">
        <div class="slider">
            <h1>Opalanie natryskowe
                w Twoim mieście </h1>
            <h3>Wskaż województwo i znajdź Ekspertkę</h3>
            <form method="POST" action="{{route('eksperts_by_filters_request')}}">
                @csrf
                <div class="content_wojewodztwo">
                    <select class="customselect2" name="wojewodztwo" id="wojewodztwo">
                        <option value="">Znajdź wojewodztwo</option>
                        @foreach(App\Http\Controllers\Controller::getProvinces() as $item)
                        <option value="{{$item->id_province}}">{{mb_strtolower($item->name)}}</option>
                        @endforeach
                        {{-- {{mb_strtolower($item->name)}} --}}
                    </select>
                </div>
                <input type="hidden" name="misto" value="">
                <input type="hidden" name="filtr" value="0">
                <button type="submit" class="green_bg">Szukaj</button>
            </form>
        </div>
    </div>
    @endif
</header>