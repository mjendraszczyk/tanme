<footer>
    <div class="row">
        <div class="col-md-8">
            <ul>
                <li>
                    <a href="{{route('cms_show',['id'=>'1','title'=>'polityka-prywatnosci'])}}">
                        Prywatność
                    </a>
                </li>
                <li>
                    <a href="{{route('cms_show',['id'=>'2','title'=>'regulamin'])}}">
                        Regulamin
                    </a>
                </li>
                <li>
                    <a href="{{route('szkolenie_index')}}">
                        Szkolenie
                    </a>
                </li>
                <li>
                    <a href="//tanexpert24.pl">
                        Sklep online
                    </a>
                </li>
                <li>
                    <a href="{{route('blog_index')}}">
                        Blog
                    </a>
                </li>
                <li>
                    <a href="{{route('kontakt_index')}}">
                        Kontakt
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-md-4">
            <a href="https://www.facebook.com/tanexpertPL/" target="_blank">
                <span class="fa fa-facebook"></span>
            </a>
            <a href="https://www.instagram.com/tanexpert/" target="_blank">
                <span class="fa fa-instagram"></span>
            </a>
            <a href="https://www.youtube.com/channel/UCRfiXbrZqKG2OA0SCOXFv-w" target="_blank">
                <span class="fa fa-youtube"></span>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            {{-- <img src="{{asset('/img/glowna_tanexpert.png')}}" alt="">
            <img src="{{asset('/img/glowna_minetan.png')}}" alt=""> --}}
            © {{date('Y')}} TanExpert. Wszelkie prawa zastrzeżone.
        </div>
 

        <div class="col-md-4">
            Projekt i wykonanie <a href="//virtualpeople.pl" target="_blank">Virtual People</a>
        </div>
    </div>
</footer>
<!-- 
        Logowanie
     -->
<div class="modal" id="ModalLogin">
    <div class="modal-body">
        <h1>
            Logowanie
        </h1>
        <div class="login_stan"></div>
        <form action="{{ route('login') }}" method="POST" data-url="{{route('login_api')}}">
            @csrf
            <input required type="email" name="email" placeholder="Adres e-mail">
            <input required type="password" name="password" placeholder="Hasło">
            <button class="btn btn-primary" type="submit" id="login_modal">Zaloguj się</button>
            <p class="separator" style="text-align: center;margin: 0;position: relative;z-index: 5;">
                <span style="background: #fff;position: relative;z-index: 2;padding: 0 15px;"> lub</span>
            </p>
            <a href="{{route('redirectToFacebook')}}" class="btn btn-social" style="background: #485394;">
                <i class="fa fa-facebook"></i>
                Zaloguj przez facebook</a>
            <div style="display: block;text-align:center;">
                <p>
                    Nie masz konta? <a href="{{route('rejestracja_krok1')}}">Zarejestruj się</a>
            </div>
            </p>
        </form>
    </div>
    {{-- <script type="text/javascript">
$("#login_modal").click(function(e) {
    e.preventDefault();
    alert("G");
})
</script> --}}
</div>


<!-- 
                                Modal kontakt
                             -->
<div class="modal" id="ModalKontakt">
    <div class="modal-body">
        <h1>
            Kontakt
        </h1>
        <form action="{{ route('ekspert_kontakt_store') }}" method="POST">
            @csrf
            <input required type="email" name="email" placeholder="Adres e-mail">
            <textarea name="tresc" class="form-control" cols="30" rows="10"
                placeholder="Podaj treść wiadomości"></textarea>
            <button class="btn btn-primary" type="submit">Wyślij</button>
            </p>
        </form>
    </div>
</div>