@extends('layouts.app')


@section('content')
<div class="container">
  <div class="row">
    @include('backend._main.menu')
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">
          Województwa
          <a href="{{route('backend_wojewodztwa_create')}}" class="btn waves-effect waves-light btn-secondary">
            <i class="material-icons">add_circle_outline</i>
            Nowy</a>
        </div>

        <div class="card-body">
          <table>
            <thead>
              <tr>
                <th>Nazwa</th>
                <th>Opcje</th>
              </tr>


            </thead>

            <tbody>
              @foreach($items as $wojewodztwo)
              <tr>
                <td>{{$wojewodztwo->name}}</td>
                <td>
                  <a href="{{route('backend_wojewodztwa_edit',['id'=>$wojewodztwo->id_province])}}"
                    class="btn waves-effect waves-light btn-default">
                    <i class="material-icons">
                      edit
                    </i>
                  </a>
                  <form method="POST"
                    action="{{route('backend_wojewodztwa_delete',['id'=>$wojewodztwo->id_province])}}">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn waves-effect waves-light btn-danger">
                      <i class="material-icons">
                        restore_from_trash
                      </i></button></form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>

          {{ $items->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection