@csrf
<div class="col s12 m12 l12">
    @include('backend/_main/message')
    <label>Nazwa</label>
    @if(Route::currentRouteName()=='backend_wojewodztwa_edit' )

    @endif
    <input required placeholder="Nazwa" name="name" @if(Route::currentRouteName()=='backend_wojewodztwa_edit' )
        value="{{$wojewodztwa->name}}" @else value="" @endif type="text" class="form-control validate">

    <label>Opis</label>
    @if(Route::currentRouteName()=='backend_wojewodztwa_edit' )

    @endif
    <textarea required placeholder="Opis" class="form-control editor" name="content"> @if(Route::currentRouteName()=='backend_wojewodztwa_edit' )
    {{$wojewodztwa->content}}
    @else  @endif </textarea>


    <label>Kraj</label>
    <select class="form-control" name="id_country">
        @foreach(App\Http\Controllers\Controller::getCountry(null) as $kraj)
        <option value="{{$kraj->id_country}}" @if((Route::currentRouteName()=='backend_wojewodztwa_edit' ) &&
            ($wojewodztwa->id_country ==
            $kraj->id_country))
            selected="selected"
            @else
            @endif>{{$kraj->name}}</option>
        @endforeach
    </select>

    <div class="clearfix"></div>

    <button type="submit" class="btn  waves-effect waves-light btn-large">
        Zapisz</button>