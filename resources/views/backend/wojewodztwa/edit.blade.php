@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row">
        @include('backend._main.menu')
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Województwa
                    <a href="{{route('backend_wojewodztwa_index')}}" class="btn waves-effect waves-light btn-secondary">
                        <i class="material-icons">keyboard_backspace</i>
                        Powrót</a>
                </div>

                <div class="card-body">
                    <form method="POST"
                        action="{{route('backend_wojewodztwa_update', ['id' => $wojewodztwa->id_province])}}">
                        @method('PUT')
                        @include('backend.wojewodztwa.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection