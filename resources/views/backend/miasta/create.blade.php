@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row">
        @include('backend._main.menu')
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Miasta
                    <a href="{{route('backend_miasta_index')}}" class="btn waves-effect waves-light btn-secondary">
                        <i class="material-icons">keyboard_backspace</i>
                        Powrót</a>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{route('backend_miasta_store')}}">
                        @include('backend.miasta.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection