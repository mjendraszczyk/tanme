@csrf
<div class="col s12 m12 l12">
    @include('backend/_main/message')
    <label>Nazwa miasta</label>

    <input required placeholder="Nazwa" name="name" @if(Route::currentRouteName()=='backend_miasta_edit' )
        value="{{$miasta->name}}" @else value="{{old('name')}}" @endif type="text" class="form-control validate">
    <label>Obrazek</label>
    @if(Route::currentRouteName()=='backend_miasta_edit' )
    <img src="{{asset('img/city/'.$miasta->image)}}" class="thumbnail" style="max-width:100%;clear: both;
        display: block;margin:10px 0;" />
    @endif
    <div class="file-field form-control">
        <div class="btn">
            <span>Plik</span>
            <input type="file" name="miasta_upload">
        </div>
    </div>

    <label>Opis</label>
    @if(Route::currentRouteName()=='backend_miasta_edit' )

    @endif
    <textarea placeholder="Opis" class="form-control editor" name="content"> @if(Route::currentRouteName()=='backend_miasta_edit' )
        {{$miasta->content}}
        @else  @endif </textarea>

    <label>Województwo</label>

    <select class="form-control" required name="id_province">
        <option value="" disabled selected>Województwo</option>

        @foreach (App\Http\Controllers\Controller::getProvinces(null) as
        $wojewodztwo)
        <option value="{{$wojewodztwo->id_province}}" @if(Route::currentRouteName()=='backend_miasta_edit' ) @if((
            $wojewodztwo->id ==
            $miasta->id_province) || (old('id_province') == $wojewodztwo->id))
            selected="selected"
            @endif
            @endif
            >{{$wojewodztwo->name}}</option>
        @endforeach
    </select>

</div>
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>