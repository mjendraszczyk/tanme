@csrf
@include('backend/_main/message')

<div class="clearfix"></div>

<div class="clearfix"></div>

<div class="row">

    <div class="col-md-12">
        <label>Nazwa</label>
        <input placeholder="Nazwa" name="name" @if(Route::currentRouteName()=='backend_uslugi_edit' )
            value="{{$services->name}}" @else value="{{old('name')}}" @endif type="text" class="form-control validate">
    </div>
    <div class="col-md-12">
        <label>Ekspert</label>
        <select name="id_category" class="form-control">
            @foreach($categories as $category)
            <option value="{{$category->id_category}}" @if(Route::currentRouteName()=='backend_uslugi_edit' )
                @if($category->id_category == $services->id_category)
                selected="selected" @endif @endif>{{$category->name}}</option>
            @endforeach
        </select>
    </div>
    <button type="submit" class="btn  waves-effect waves-light btn-large">
        Zapisz</button>
</div>