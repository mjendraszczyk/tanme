@extends('layouts.app')


@section('content')
<div class="container">
  <div class="row">
    @include('backend._main.menu')
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">
          Usługi
          <a href="{{route('backend_uslugi_create')}}" class="btn waves-effect waves-light btn-secondary">
            <i class="material-icons">add_circle_outline</i>
            Nowy</a>
        </div>

        <div class="card-body">
          <table>
            <thead>
              <tr>
                <th>ID</th>
                <th>Nazwa</th>
                <th>Kategoria</th>
              </tr>


            </thead>

            <tbody>
              @foreach($items as $service)
              <tr>
                <td>
                  {{$service->id_service}}
                </td>
                <td>
                  {{$service->name}}
                </td>
                <td>
                  {{$service->id_category}}
                </td>
                <td>
                  <a href="{{route('backend_uslugi_edit',['id'=>$service->id_service])}}"
                    class="btn waves-effect waves-light btn-default">
                    <i class="material-icons">
                      edit
                    </i>
                  </a>
                  <form method="POST" action="{{route('backend_uslugi_delete',['id'=>$service->id_service])}}">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn waves-effect waves-light btn-danger">
                      <i class="material-icons">
                        restore_from_trash
                      </i></button></form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>

          {{ $items->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection