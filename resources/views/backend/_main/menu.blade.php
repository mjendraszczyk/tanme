<div class="col-md-4">
    <div class="sidebar">
        <ul class="nav">
            <li class="nav-item">
                <a href="{{route('backend_eksperci_index')}}"><span class="material-icons">
                        school
                    </span> Eksperci</a>
            </li>
            <li class="nav-item">
                <a href="{{route('backend_cms_index')}}"><span class="material-icons">
                        insert_drive_file
                    </span> Strony CMS</a>
            </li>
            <li class="nav-item">
                <a href="{{route('backend_blog_index')}}"> <span class="material-icons">
                        library_books
                    </span> Blog</a>
            </li>
            <li class="nav-item">
                <a href="{{route('backend_kraje_index')}}"><span class="material-icons">
                        public
                    </span> Kraje</a>
            </li>
            <li class="nav-item">
                <a href="{{route('backend_wojewodztwa_index')}}"><span class="material-icons">
                        terrain
                    </span> Województwa</a>
            </li>
            <li class="nav-item">
                <a href="{{route('backend_miasta_index')}}"><span class="material-icons">
                        location_city
                    </span> Miasta</a>
            </li>
            <li class="nav-item">
                <a href="{{route('backend_opinie_index')}}"><span class="material-icons">
                        star_outline
                    </span> Opinie</a>
            </li>
            <li class="nav-item">
                <a href="{{route('backend_uzytkownicy_index')}}"><span class="material-icons">
                        people_outline
                    </span> Uzytkownicy</a>
            </li>
            <li class="nav-item">
                <a href="{{route('backend_kategorie_index')}}"><span class="material-icons">
                        book
                    </span> Kategorie</a>
            </li>
            <li class="nav-item">
                <a href="{{route('backend_uslugi_index')}}"><span class="material-icons">
                        settings
                    </span> Usługi</a>
            </li>
            <li class="nav-item">
                <form action="{{route('logout')}}" method="post">
                    @csrf
                    <button type="submit" class="logout"><span class="material-icons">
                            lock_open
                        </span> Wyloguj</button>
                </form>
            </li>
        </ul>
    </div>
</div>