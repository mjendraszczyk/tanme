@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
  <div class="card">
    <div class="card-header">
      Ustawienia
      <a href="{{route('backend_ustawienia_create')}}" class="btn waves-effect waves-light btn-secondary">
        <i class="material-icons">add_circle_outline</i>
        Nowy</a>
    </div>

    <div class="card-body">
      <table>
        <thead>
          <tr>
            <th>Nazwa</th>
            <th>Opis</th>
            <th>Opcje</th>
          </tr>
          <tr>
            <form method="GET" action="{{route('backend_ustawienia_filter')}}">
              @csrf
              <th>
                <input id="instancja" type="text" class="@error('instancja') is-invalid @enderror" name="instancja"
                  value="{{ Session::get('ustawienia_instancja') }}" autocomplete="no" placeholder="Instancja"
                  autofocus>
              </th>
              <th>
                <input id="wartosc" type="text" class="@error('wartosc') is-invalid @enderror" name="wartosc"
                  value="{{ Session::get('ustawienia_wartosc') }}" autocomplete="no" placeholder="Wartosc" autofocus>
              </th>
              <th>
                <button type="submit" name="save_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    search
                  </i> Szukaj</button>
                <button type="submit" name="reset_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    clear
                  </i>
                  Reset</button>
              </th>
            </form>
          </tr>

        </thead>

        <tbody>
          @foreach($items as $ustawienia)
          <tr>

            <td>{{$ustawienia->instancja}}</td>

            <td>{{$ustawienia->wartosc}}</td>

            <td>
              <a href="{{route('backend_ustawienia_edit',['id'=>$ustawienia->id_ustawienia])}}"
                class="btn waves-effect waves-light btn-default">
                <i class="material-icons">
                  edit
                </i>
                Edytuj</a>
              <form method="POST" action="{{route('backend_ustawienia_delete',['id'=>$ustawienia->id_ustawienia])}}">
                @csrf
                @method('DELETE')
                <button disabled type="submit" class="btn waves-effect waves-light btn-danger">
                  <i class="material-icons">
                    restore_from_trash
                  </i>Usuń</button></form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>

      {{ $items->links() }}
    </div>
  </div>
</div>
@endsection