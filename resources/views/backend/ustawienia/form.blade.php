@csrf
<div class="col s12 m12 l12">
    @include('backend/_main/message')
    <label>Instancja</label>


    <input readonly placeholder="Instancja" name="instancja" @if(Route::currentRouteName()=='backend_ustawienia_edit' )
        value="{{$ustawienia->instancja}}" @else value="" @endif type="text" class="form-control-input validate">

    <label>Wartosc</label>
    @if(($ustawienia->instancja == 'mails') || ($ustawienia->instancja == 'maintenance'))
    <select name="wartosc">
        <option value="1">Włączone</option>
        <option value="0">Wyłączone</option>
    </select>
    @else
    <input required placeholder="Wartosc" name="wartosc" @if(Route::currentRouteName()=='backend_ustawienia_edit' )
        value="{{$ustawienia->wartosc}}" @else value="" @endif type="text" class="form-control-input validate">
    @endif
</div>
</div>
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>