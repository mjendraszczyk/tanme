@extends('layouts.app')


@section('content')
<div class="container">
  <div class="row">
    @include('backend._main.menu')
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">
          Kategorie
          <a href="{{route('backend_kategorie_create')}}" class="btn waves-effect waves-light btn-secondary">
            <i class="material-icons">add_circle_outline</i>
            Nowy</a>
        </div>

        <div class="card-body">
          <table>
            <thead>
              <tr>
                <th>ID</th>
                <th>Nazwa</th>
                <th></th>
              </tr>


            </thead>

            <tbody>
              @foreach($items as $category)
              <tr>
                <td>
                  {{$category->id_category}}
                </td>
                <td>
                  {{$category->name}}
                </td>

                <td>
                  <a href="{{route('backend_kategorie_edit',['id'=>$category->id_category])}}"
                    class="btn waves-effect waves-light btn-default">
                    <i class="material-icons">
                      edit
                    </i>
                  </a>
                  <form method="POST" action="{{route('backend_kategorie_delete',['id'=>$category->id_category])}}">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn waves-effect waves-light btn-danger">
                      <i class="material-icons">
                        restore_from_trash
                      </i></button></form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>

          {{ $items->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection