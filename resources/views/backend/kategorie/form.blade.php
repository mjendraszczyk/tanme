@csrf
@include('backend/_main/message')

<div class="clearfix"></div>

<div class="clearfix"></div>

<div class="row">

    <div class="col-md-12">
        <label>Nazwa</label>
        <input placeholder="Nazwa" name="name" @if(Route::currentRouteName()=='backend_kategorie_edit' )
            value="{{$category->name}}" @else value="{{old('name')}}" @endif type="text" class="form-control validate">
    </div>

    <button type="submit" class="btn  waves-effect waves-light btn-large">
        Zapisz</button>
</div>