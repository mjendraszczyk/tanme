@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row">
        @include('backend._main.menu')
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Blog
                    <a href="{{route('backend_blog_index')}}" class="btn waves-effect waves-light btn-secondary">
                        <i class="material-icons">keyboard_backspace</i>
                        Powrót</a>
                </div>

                <div class="card-body">
                    <form method="POST" enctype="multipart/form-data" action="{{route('backend_blog_store')}}">
                        @include('backend.blog.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection