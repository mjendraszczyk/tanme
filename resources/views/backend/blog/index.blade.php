@extends('layouts.app')


@section('content')
<div class="container">
  <div class="row">
    @include('backend._main.menu')
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">
          Blog
          <a href="{{route('backend_blog_create')}}" class="btn waves-effect waves-light btn-secondary">
            <i class="material-icons">add_circle_outline</i>
            Nowy</a>
        </div>

        <div class="card-body">
          <table>
            <thead>
              <tr>
                <th>Tytuł</th>
                <th>Tresc</th>
                <th>Opcje</th>
              </tr>


            </thead>

            <tbody>
              @foreach($items as $blog)
              <tr>

                <td>{{$blog->title}}</td>

                <td>{{$blog->content}}</td>

                <td>
                  <a href="{{route('backend_blog_edit',['id'=>$blog->id_blog])}}"
                    class="btn waves-effect waves-light btn-default">
                    <i class="material-icons">
                      edit
                    </i>
                  </a>
                  <form method="POST" action="{{route('backend_blog_delete',['id'=>$blog->id_blog])}}">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn waves-effect waves-light btn-danger">
                      <i class="material-icons">
                        restore_from_trash
                      </i></button></form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>

          {{ $items->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection