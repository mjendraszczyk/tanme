@csrf
<div class="col s12 m12 l12">

    @include('backend/_main/message')
    <label>Tytuł</label>

    <input required placeholder="Nazwa" name="title" @if(Route::currentRouteName()=='backend_blog_edit' )
        value="{{$blog->title}}" @else value="{{old('tytul')}}" @endif type="text" class="form-control validate">

    <label>Tresc</label>
    @if(Route::currentRouteName()=='backend_blog_edit' )
    <textarea name="content" placeholder="Wpisz treść ogloszenia"
        class="editor form-control validate">{{$blog->content}}</textarea>
    @else
    <textarea name="content" placeholder="Wpisz treść ogloszenia"
        class="editor form-control validate">{{old('tresc')}}</textarea>
    @endif

    <label>Obraz</label>
    @if(Route::currentRouteName()=='backend_blog_edit' )
    <img src="{{asset('img/blog/'.$blog->feature_image)}}" class="thumbnail" style="max-width:100%;clear: both;
    display: block;margin:10px 0;" />
    @endif
    <div class="file-field form-control">
        <div class="btn">
            <span>Plik</span>
            <input type="file" name="blog_upload">
        </div>

    </div>
    <button type="submit" class="btn  waves-effect waves-light btn-large">
        Zapisz</button>
</div>