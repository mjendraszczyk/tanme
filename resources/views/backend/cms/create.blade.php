@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('backend._main.menu')
        <div class="col-md-8">
            <div class="card">
                @include('backend.cms.header')
                <div class="card-body">
                    <form method="POST" action="{{route('backend_cms_store')}}">
                        @include('backend.cms.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection