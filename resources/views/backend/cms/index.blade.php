@extends('layouts.app')


@section('content')
<div class="container">
  <div class="row">
    @include('backend._main.menu')
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">
          Strony CMS
          <a href="{{route('backend_cms_create')}}" class="btn waves-effect waves-light btn-secondary">
            <i class="material-icons">add_circle_outline</i>
            Nowy</a>
        </div>

        <div class="card-body">
          <table>
            <thead>
              <tr>
                <th>Tytuł</th>
                <th>Stan</th>
                <th>Opcje</th>
              </tr>


            </thead>

            <tbody>
              @foreach($items as $cms)
              <tr>
                <td>{{$cms->title}}</td>
                <td>{{str_limit($cms->content)}}</td>
                <td>
                  <a href="{{route('backend_cms_edit',['id'=>$cms->id_cms])}}"
                    class="btn waves-effect waves-light btn-default">
                    <i class="material-icons">
                      edit
                    </i>
                  </a>
                  <form method="POST" action="{{route('backend_cms_delete',['id'=>$cms->id_cms])}}">
                    @csrf
                    @method('DELETE')
                    <button type="submit" disabled class="btn waves-effect waves-light btn-danger">
                      <i class="material-icons">
                        restore_from_trash
                      </i></button></form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>

          {{ $items->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection