@csrf

<div>
    @include('backend/_main/message')
    <label>Tytuł</label>
    <input required placeholder="Tytuł" name="title" @if(Route::currentRouteName()=='backend_cms_edit' )
        value="{{$cms->title}}" @else value="" @endif type="text" class="form-control validate">
    <label>Treść</label>

    <textarea class="editor form-control" placeholder="Tresc"
        name="content">@if(Route::currentRouteName()=='backend_cms_edit' ){{$cms->content}} @else @endif </textarea>
    <label>Stan</label>

    <select name="stan" class="form-control">
        @if(Route::currentRouteName()=='backend_cms_edit' )
        <option value="1" @if($cms->stan == '1') selected="selected" @endif>Włączone</option>
        <option value="0" @if($cms->stan != '1') selected="selected" @endif>Wyłączone</option>
        @else
        <option value="1">Włączone</option>
        <option value="0" selected="selected">Wyłączone</option>
        @endif
    </select>
</div>
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>