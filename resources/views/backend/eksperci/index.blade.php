@extends('layouts.app')


@section('content')
<div class="container">
  <div class="row">
    @include('backend._main.menu')
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">
          Eksperci
          <a href="{{route('backend_eksperci_create')}}" class="btn waves-effect waves-light btn-secondary">
            <i class="material-icons">add_circle_outline</i>
            Nowy</a>
        </div>

        <div class="card-body">
          <form action="{{route('backend_eksperci_index_filtr')}}" method="post" style="padding: 15px;">
            @csrf
            <input type="text" value="{{old('nazwa')}}" name="nazwa" class="form-control" placeholder="Nazwa miasta"
              style="width: auto;display: inline-block;" />
            <input type="submit" class="btn btn-primary" value="Filtr" />
          </form>
          <table>
            <thead>
              <tr>
                <th>Nazwa</th>
                <th>Adres</th>
                <th>Opcje</th>
              </tr>
            </thead>

            <tbody>
              @foreach($items as $ekspert)
              <tr>

                <td>{{$ekspert->name}}</td>

                <td>{{$ekspert->address}}</td>

                <td>
                  <a href="{{route('backend_eksperci_edit',['id'=>$ekspert->id_ekspert])}}"
                    class="btn waves-effect waves-light btn-default">
                    <i class="material-icons">
                      edit
                    </i>
                  </a>
                  <form method="POST" action="{{route('backend_eksperci_delete',['id'=>$ekspert->id_ekspert])}}">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn waves-effect waves-light btn-danger">
                      <i class="material-icons">
                        restore_from_trash
                      </i></button></form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>

          {{ $items->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection