@csrf
<div class="col s12 m12 l12">


    <label>Nazwa</label>

    <input required placeholder="Nazwa" name="name" @if(Route::currentRouteName()=='backend_eksperci_edit' )
        value="{{$ekspert->name}}" @else value="" @endif type="text" class="form-control validate">


    <div class="row panel" style="margin:15px 0;">
        <label>Województwo</label>
        <select class="form-control customselect2 zmien_miasta" required name="id_province">
            <option value="">-- Województwo --</option>
            @foreach (App\Http\Controllers\Controller::getProvinces(null) as
            $wojewodztwo)
            <option @if($ekspert->province == $wojewodztwo->id_province) selected="selected"
                @endif
                value="{{$wojewodztwo->id_province}}">{{mb_strtolower($wojewodztwo->name)}}
            </option>
            @endforeach
        </select>

        <div class="clearfix"></div>
        <label>Miasto</label>
        <select class="miastaselect form-control" name="miasto" disabled>
            <option value="">--</option>
            @foreach (App\Http\Controllers\Controller::getCities() as $item)
            <option value="{{$item->id_city}}" @if($ekspert->city==$item->id_city)
                selected="selected"
                @endif>{{$item->name}}</option>
            @endforeach
        </select>
        <input type="hidden" name="miasto_hidden" />

    </div>


    <label>Adres</label>

    <input required placeholder="Adres" name="address" @if(Route::currentRouteName()=='backend_eksperci_edit' )
        value="{{$ekspert->address}}" @else value="" @endif type="text" class="form-control validate">


    <label>Facebook</label>

    <input placeholder="Facebook" name="facebook" @if(Route::currentRouteName()=='backend_eksperci_edit' )
        value="{{$ekspert->facebook}}" @else value="" @endif type="text" class="form-control validate">

    <label>Instagram</label>

    <input placeholder="Instagram" name="instagram" @if(Route::currentRouteName()=='backend_eksperci_edit' )
        value="{{$ekspert->instagram}}" @else value="" @endif type="text" class="form-control validate">
    <label>YouTube</label>

    <input placeholder="YouTube" name="youtube" @if(Route::currentRouteName()=='backend_eksperci_edit' )
        value="{{$ekspert->youtube}}" @else value="" @endif type="text" class="form-control validate">

    <label>E-mail</label>
    <input required placeholder="E-mail" name="expert_mail" @if(Route::currentRouteName()=='backend_eksperci_edit' )
        value="{{$ekspert->expert_email}}" @else value="" @endif type="email" class="form-control validate">

    <label>Telefon</label>
    <input required placeholder="Telefon" name="phone" @if(Route::currentRouteName()=='backend_eksperci_edit' )
        value="{{$ekspert->phone}}" @else value="" @endif type="text" class="form-control validate">


    <label>Wyrózniony</label>
    <select name="feature" class="form-control">
        <option value="1" @if($ekspert->feature) selected="selected" @endif>Tak</option>
        <option value="0" @if($ekspert->feature) selected="selected" @endif>Nie</option>
    </select>

    <label>Uzytkownik</label>

    <select name="id_user" class="form-control">
        @foreach ($users as $user)
        <option value="{{$user->id}}" @if($ekspert->id_user == $user->id) selected="selected" @endif>#{{$user->id}} |
            {{$user->name}}
        </option>
        @endforeach
    </select>


    <label>Zdjęcie</label>
    @if(Route::currentRouteName()=='backend_eksperci_edit' )
    <img src="{{asset('img/ekspert/'.$ekspert->feature_image)}}" class="thumbnail" style="max-width:100%;clear: both;
        display: block;margin:10px 0;" />
    @endif
    <div class="file-field form-control">
        <div class="btn">
            <span>Plik</span>
            <input type="file" name="ekspert_upload">
        </div>
    </div>

    <label>Opis</label>
    @if(Route::currentRouteName()=='backend_eksperci_edit' )
    <textarea name="opis" required placeholder="Wpisz treść ogloszenia"
        class="editor form-control materialize-textarea validate">{{$ekspert->content}}</textarea>
    @else
    <textarea name="opis" required placeholder="Wpisz treść ogloszenia"
        class="editor form-control materialize-textarea validate">{{old('content')}}</textarea>
    @endif
</div>

<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>