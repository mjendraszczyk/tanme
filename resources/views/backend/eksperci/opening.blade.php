@extends('layouts.app')


@section('content')
<div class="container dashboard">
    <div class="row">
        @include('backend._main.menu')
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Godziny otwarcia

                    <a href="{{route('backend_eksperci_edit',['id'=>$ekspert->id_ekspert])}}"
                        class="btn waves-effect waves-light btn-secondary">
                        <i class="material-icons">keyboard_backspace</i>
                        Powrót</a>
                </div>
            </div>
            <form action="{{route('backend_opening_update',['id_ekspert'=>$ekspert->id_ekspert])}}" class="text-left"
                method="POST">
                @csrf
                @method('PUT')
                {{-- T: {{$countEkspert}} --}}
                @if($countEkspert == 0)
                @else
                <div class="row panel godziny">

                    @for($j=1;$j<=7;$j++) <div class="row">
                        <div class="col-md-3">
                            {{App\Http\Controllers\Controller::getDays()[$j]}}
                        </div>

                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-6">

                                    <select name="godzina_od[]">
                                        <option value="">-- Zamknięte --</option>
                                        @for($i=0;$i<24;$i++) <option value="{{$i}}:00:00" @if($countOpening> 0)
                                            @if(($opening[$j-1]['godzina_od']==$i) && ($opening[$j-1]['godzina_do']
                                            !=null)) selected="selected" @endif @endif> {{$i}}:00
                                            </option>
                                            @endfor

                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <select name="godzina_do[]">
                                        <option value="">-- Zamknięte --</option>
                                        @for($i=0;$i<24;$i++) <option value="{{$i}}:00:00" @if($countOpening> 0)
                                            @if(($opening[$j-1]['godzina_do']==$i) && ($opening[$j-1]['godzina_do']
                                            !=null)) selected="selected" @endif @endif>{{$i}}:00
                                            </option>
                                            @endfor

                                    </select>
                                </div>
                            </div>
                        </div>
                </div>
                @endfor
                <button type="submit" class="btn  waves-effect waves-light btn-large">
                    Zapisz</button>
        </div>
        @endif
        </form>
    </div>
</div>
</div>
@endsection