@extends('layouts.app')


@section('content')
<div class="container">
  <div class="row">
    @include('backend._main.menu')
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">
          Opinie
          <a href="{{route('backend_opinie_create')}}" class="btn waves-effect waves-light btn-secondary">
            <i class="material-icons">add_circle_outline</i>
            Nowy</a>
        </div>

        <div class="card-body">
          <table>
            <thead>
              <tr>
                <th>Imię nazwisko</th>
                <th>Ocena</th>
                <th>Opinia</th>
                <th>Opcje</th>
              </tr>


            </thead>

            <tbody>
              @foreach($items as $review)
              <tr>
                <td>
                  {{$review->nick}}
                </td>
                <td>
                  {{$review->rate}}
                </td>
                <td>
                  {{$review->comment}}
                </td>
                <td>
                  <a href="{{route('backend_opinie_edit',['id'=>$review->id_review])}}"
                    class="btn waves-effect waves-light btn-default">
                    <i class="material-icons">
                      edit
                    </i>
                  </a>
                  <form method="POST" action="{{route('backend_opinie_delete',['id'=>$review->id_review])}}">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn waves-effect waves-light btn-danger">
                      <i class="material-icons">
                        restore_from_trash
                      </i></button></form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>

          {{ $items->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection