@csrf
@include('backend/_main/message')

<div class="clearfix"></div>

<div class="clearfix"></div>

<div class="row">

    <div class="col-md-6">
        <label>Imię i nazwisko</label>
        <input placeholder="Imie i nazwisko" name="nick" @if(Route::currentRouteName()=='backend_opinie_edit' )
            value="{{$review->nick}}" @else value="{{old('nick')}}" @endif type="text" class="form-control validate">
    </div>
    <div class="col-md-6">
        <label>Ocena</label>
        <select name="rate" class="form-control">
            <option value="1" @if(Route::currentRouteName()=='backend_opinie_edit' ) @if($review->rate == '1')
                selected="selected" @endif @endif>1</option>
            <option value="2" @if(Route::currentRouteName()=='backend_opinie_edit' ) @if($review->rate == '2')
                selected="selected" @endif @endif>2</option>
            <option value="3" @if(Route::currentRouteName()=='backend_opinie_edit' ) @if($review->rate == '3')
                selected="selected" @endif @endif>3</option>
            <option value="4" @if(Route::currentRouteName()=='backend_opinie_edit' ) @if($review->rate == '4')
                selected="selected" @endif @endif>4</option>
            <option value="5" @if(Route::currentRouteName()=='backend_opinie_edit' ) @if($review->rate == '5')
                selected="selected" @endif @endif>5</option>
        </select>
    </div>

    <div class="col-md-12">
        <label>Komentarz</label>
        <textarea placeholder="Treść" name="comment" class="form-control validate">@if(Route::currentRouteName()=='backend_opinie_edit' )
            {{$review->comment}} @else {{old('comment')}} @endif</textarea>
    </div>

    <div class="col-md-12">
        <label>Status</label>
        <select name="status" class="form-control">
            <option value="0" @if($review->status == '0') selected="selected" @endif>Nieopublikowana</option>
            <option value="1" @if($review->status == '1') selected="selected" @endif>Opublikowana</option>
        </select>
    </div>

    <div class="col-md-12">
        <label>Ekspert</label>
        <select name="id_ekspert" class="form-control">
            @foreach($eksperci as $ekspert)
            <option value="{{$ekspert->id_ekspert}}"># {{$ekspert->id_ekspert}} | {{$ekspert->name}}</option>
            @endforeach
        </select>
    </div>
    <button type="submit" class="btn  waves-effect waves-light btn-large">
        Zapisz</button>
</div>