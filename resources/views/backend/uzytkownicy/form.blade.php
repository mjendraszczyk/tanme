@csrf
<div class="col s12 m12 l12">
    @include('backend/_main/message')
    <label>Nazwa użytkownika</label>
    @if(Route::currentRouteName()=='backend_uzytkownicy_edit' )

    @endif
    <input required placeholder="Nazwa" name="name" @if(Route::currentRouteName()=='backend_uzytkownicy_edit' )
        value="{{$uzytkownicy->name}}" @else value="" @endif type="text" class="form-control validate">

    <label>Email</label>
    <input required placeholder="E-mail" name="email" @if(Route::currentRouteName()=='backend_uzytkownicy_edit' )
        value="{{$uzytkownicy->email}}" @else value="" @endif type="text" class="form-control validate">

    <label>Firma</label>
    <input required placeholder="Nazwa firmy" name="business" @if(Route::currentRouteName()=='backend_uzytkownicy_edit'
        ) value="{{$uzytkownicy->business}}" @else value="" @endif type="text" class="form-control validate">

    <label>Hasło</label>
    <input placeholder="Hasło" name="password" @if(Route::currentRouteName()=='backend_uzytkownicy_edit' ) value=""
        @else value="" @endif type="password" class="form-control validate">
    <label>Powtórz hasło</label>
    <input placeholder="Powtórz hasło" name="password_repeat" @if(Route::currentRouteName()=='backend_uzytkownicy_edit'
        ) value="" @else value="" @endif type="password" class="form-control validate">

    <label>Rola</label>
    <select class="form-control" name="id_role">
        <option value="" disabled selected>rola</option>
        @foreach(App\Http\Controllers\Controller::getRole(null) as $rola)
        <option value="{{$rola->id_role}}" @if((Route::currentRouteName()=='backend_uzytkownicy_edit' ) &&
            ($uzytkownicy->id_role ==
            $rola->id_role))
            selected="selected"
            @else
            @endif>{{$rola->name}}</option>
        @endforeach
    </select>



    <div class="clearfix"></div>

    <button type="submit" class="btn  waves-effect waves-light btn-large">
        Zapisz</button>