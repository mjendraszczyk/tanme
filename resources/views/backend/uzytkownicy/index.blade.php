@extends('layouts.app')


@section('content')
<div class="container">
  <div class="row">
    @include('backend._main.menu')
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">
          Użytkownicy
          <a href="{{route('backend_uzytkownicy_create')}}" class="btn waves-effect waves-light btn-secondary">
            <i class="material-icons">add_circle_outline</i>
            Nowy</a>
        </div>

        <div class="card-body">
          <form action="{{route('backend_user_index_filtr')}}" method="post" style="padding: 15px;">
            @csrf
            <input type="text" value="{{old('nazwa')}}" name="nazwa" class="form-control" placeholder="Nazwa miasta"
              style="width: auto;display: inline-block;" />
            <input type="submit" class="btn btn-primary" value="Filtr" />
          </form>
          <table>
            <thead>
              <tr>
                <th>Nazwa</th>
                <th>Opcje</th>
              </tr>


            </thead>

            <tbody>
              @foreach($items as $uzytkownik)
              <tr>
                <td>{{$uzytkownik->name}}</td>
                <td>{{$uzytkownik->email}}</td>
                <td>
                  <a href="{{route('backend_uzytkownicy_edit',['id'=>$uzytkownik->id])}}"
                    class="btn waves-effect waves-light btn-default">
                    <i class="material-icons">
                      edit
                    </i>
                  </a>
                  <form method="POST" action="{{route('backend_uzytkownicy_delete',['id'=>$uzytkownik->id])}}">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn waves-effect waves-light btn-danger">
                      <i class="material-icons">
                        restore_from_trash
                      </i></button></form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>

          {{ $items->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection