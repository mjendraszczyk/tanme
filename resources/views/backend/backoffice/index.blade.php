@extends('layouts.app')


@section('content')
<div class="container dashboard">
  <div class="row">
    @include('backend._main.menu')
    <div class="col-md-8">
      <div class="panel">
        <div class="row">
          <div class="col-md-6">
            <div class="card text-white bg-primary mb-3">

              <a href="{{route('backend_eksperci_index')}}" class="card-body">
                <i class="material-icons">school</i>
                <h5 class="card-title">Eksperci</h5>
                <p class="card-text">{{$countEkspert}} Liczba eksperów
                </p>
              </a>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card text-white bg-primary mb-3">
              <a href="{{route('backend_blog_index')}}" class="card-body">
                <i class="material-icons">library_books</i>
                <h5 class="card-title">Blog</h5>
                <p class="card-text">
                  {{$countBlog}} wpisów na blogu
                </p>
              </a>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="card text-white bg-primary mb-3">
              <a href="{{route('backend_opinie_index')}}" class="card-body">
                <i class="material-icons">star</i>
                <h5 class="card-title">Opinie</h5>
                <p class="card-text">{{$countReview}} Liczba opinii
                </p>
              </a>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card text-white bg-primary mb-3">
              <a href="{{route('backend_uzytkownicy_index')}}" class="card-body">
                <i class="material-icons">person</i>
                <h5 class="card-title">Uzytkownicy</h5>
                <p class="card-text">
                  {{$countUser}} uzytkowników
                </p>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection