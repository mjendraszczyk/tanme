@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('backend._main.menu')
        <div class="col-md-8">
            <div class="card">
                @include('backend.kraje.header')
                <div class="card-body">
                    <form method="POST" action="{{route('backend_kraje_update', ['id' => $kraje->id_country])}}">
                        @method('PUT')
                        @include('backend.kraje.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection