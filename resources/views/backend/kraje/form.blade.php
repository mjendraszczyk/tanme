@csrf
<div class="col s12 m12 l12">
    @include('backend/_main/message')
    <label>Nazwa kraju</label>
    <input required placeholder="Nazwa" name="name" @if(Route::currentRouteName()=='backend_kraje_edit' )
        value="{{$kraje->name}}" @else value="" @endif type="text" class="form-control validate">


</div>
<button type="submit" class="btn waves-effect waves-light btn-large">
    Zapisz</button>