@extends('layouts.app')
@section('meta_seo')
<title>
    TanFinder - Rejestracja - Krok 1
</title>
@endsection

@section('content')

<section class="block">
    <div class="rejestracja">
        <div class="container">
            <h1>Rejestracja</h1>
            <div class="row">
                <ul class="rejestracja_lista">
                    <li class="active">Krok 1</li>
                    <li class="active">Krok 2</li>
                    <li>Krok 3</li>
                    <li>Krok 4</li>
                    <li>Krok 5</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="grey_block">
                {{-- test: {{$id}} --}}
                <div class="container">
                    <div class="rejestracja_block">
                        <form method="post" action="{{route('rejestracja_krok2_request')}}">
                            @csrf
                            @include('backend/_main/message')
                            {{-- <label for="telefon">Nr telefonu</label> --}}
                            <input placeholder="Nr telefonu (*)" type="text" name="telefon" id="telefon"
                                value="{{old('telefon')}}">
                            <label>
                                {{-- {{dd(old('typ'))}} --}}
                                <input type="checkbox" class="inlineblock w-auto" name="typ[]" value="0" id=""
                                    @if(old('typ')) @if(in_array('0',old('typ'))) checked="checked" @endif @endif>Salon
                                stacjonarny</label>
                            <label>
                                <input type="checkbox" class="inlineblock w-auto" name="typ[]" value="1" id=""
                                    @if(old('typ')) @if(in_array('1',old('typ'))) checked="checked" @endif
                                    @endif>Ekspertka
                                mobilna</label>


                            {{-- <label for="adres">Adres</label> --}}

                            <div class="row panel" style="margin:15px 0;">
                                <select class="form-control customselect2 zmien_miasta" required name="id_province">
                                    <option value="">-- Województwo --</option>
                                    @foreach (App\Http\Controllers\Controller::getProvinces(null) as
                                    $wojewodztwo)
                                    <option @if(old('id_province')==$wojewodztwo->id_province) selected="selected"
                                        @endif
                                        value="{{$wojewodztwo->id_province}}">{{mb_strtolower($wojewodztwo->name)}}
                                    </option>
                                    @endforeach
                                </select>

                                <div class="clearfix"></div>
                                <select class="miastaselect form-control" name="miasto" disabled>
                                    <option value="">--</option>
                                    @foreach (App\Http\Controllers\Controller::getCities() as $item)
                                    <option value="{{$item->id_city}}" @if(old('miasto')==$item->id_city)
                                        selected="selected"
                                        @endif>{{$item->name}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" name="miasto_hidden" />

                            </div>

                            <input placeholder="Adres (*)" type="text" name="adres" id="adres" value="{{old('adres')}}">
                            <div class="clearfix"></div>
                            <label>
                                <input type="checkbox" class="inlineblock w-auto" name="cert" value="1" id=""
                                    @if(old('cert')=='1' ) checked="checked" @endif>Posiadam szkolenie TanExpert
                                Polska</label>
                            {{-- <label for="godziny">Godziny otwarcia</label> --}}
                            {{-- {{dd(old('godzina_od[]'))}} --}}
                            <div class="row panel godziny">
                                @for($j=1;$j<=7;$j++) <div class="row">
                                    <div class="col-md-3">
                                        {{App\Http\Controllers\Controller::getDays()[$j]}}
                                    </div>
                                    {{-- {{dd(old('typ'))}} --}}
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <select name="godzina_od[]">
                                                    <option value="">-- Zamknięte --</option>
                                                    @for($i=0;$i<24;$i++) <option @if(old('godzina_od'))
                                                        @if(in_array($i, old('godzina_od'))) selected="selected" @endif
                                                        @endif value="{{$i}}:00:00">{{$i}}:00
                                                        </option>
                                                        @endfor

                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="godzina_do[]">
                                                    <option value="">-- Zamknięte --</option>
                                                    @for($i=0;$i<24;$i++) <option @if(old('godzina_do'))
                                                        @if(in_array($i, old('godzina_do'))) selected="selected" @endif
                                                        @endif value="{{$i}}:00:00">{{$i}}:00
                                                        </option>
                                                        @endfor

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            @endfor
                    </div>
                    {{-- <input type="text" name="godziny" id="godziny"> --}}

                    {{-- <label for="facebook">Facebook</label> --}}
                    <input placeholder="Facebook" type="text" value="{{old('facebook')}}" name="facebook" id="godziny">

                    {{-- <label for="instagram">Instagram</label> --}}
                    <input placeholder="Instagram" type="text" value="{{old('instagram')}}" name="instagram"
                        id="instagram">

                    {{-- <label for="godziny">YouTube</label> --}}
                    <input placeholder="YouTube" type="text" value="{{old('youtube')}}" name="youtube" id="youtube">


                    <label>Wybierz lokalizacje (Kliknij na mapie w celu zmiany punktu)</label>
                    <div id="mapLatLon"></div>
                    <input type="hidden" name="lat" value="{{old('lat')}}" id="lat">
                    <input type="hidden" name="lon" id="lon" value="{{old('lon')}}">
                    <div style="text-align: center;margin-top:15px;">
                        <a class="btn btn-primary btn-secondary center" href="{{route('rejestracja_krok1')}}">Wstecz</a>
                        <button type="submit" class="btn btn-primary center">Dalej</button>
                        
                    </div>


                    </form>

                </div>
            </div>

        </div>

    </div>

    </div>
    </div>
</section>
@endsection