@extends('layouts.app')
@section('meta_seo')
<title>
    TanFinder - Rejestracja - Krok 3
</title>
@endsection

@section('content')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>

<section class="block">
    <div class="rejestracja">
        <div class="container">
            <h1>Rejestracja</h1>
            <div class="row">
                <ul class="rejestracja_lista">
                    <li class="active">Krok 1</li>
                    <li class="active">Krok 2</li>
                    <li class="active">Krok 3</li>
                    <li>Krok 4</li>
                    <li>Krok 5</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="grey_block">
                <div class="container">
                    <div class="rejestracja_block">
                        <form method="post" action="{{route('rejestracja_krok3_request')}}"
                            enctype="multipart/form-data">
                            @csrf
                            @include('backend/_main/message')
                            <div class="panel-body uploader">
                                <div class="file">
                                    <input class="image" type="file" name="filename" accept="image/*" onchange="loadFile(event)">
                                    <input type="hidden" id="croped" name="croped" />
                                    {{-- <input type="file" > --}}
                                    <img id="output" />
                                </div>
                            </div>


                            <div style="text-align: center;">
                                <a class="btn btn-primary btn-secondary center" href="{{route('rejestracja_krok2')}}">Wstecz</a>
                                <button type="submit" class="btn btn-primary center">Dalej</button>
                              
                            </div>
                        </form>

                        
                        <script>
                            var loadFile = function(event) {
                            var output = document.getElementById('output');
                            output.src = URL.createObjectURL(event.target.files[0]);
                            output.onload = function() {
                              URL.revokeObjectURL(output.src) // free memory
                            }
                          };
                        </script>

                        <div class="modal" id="modalUpload" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="modalLabel">Wykadruj zdjęcie
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="img-container">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="preview"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button> --}}
                                        <button type="button" class="btn btn-primary" id="crop">Wykadruj</button>
                                    </div>
                                </div>
                            </div>
                        </div>

<script type="text/javascript">
    $.noConflict();
(function( $ ) {
$(function() {

var $modal = $('#modalUpload');
var image = document.getElementById('image');
var cropper;

$("body").on("change", ".image", function(e){
var files = e.target.files;
var done = function (url) {
    image.src = url;
$modal.modal('show');
};


var reader;
var file;
var url;
if (files && files.length > 0) {
file = files[0];
if (URL) {
done(URL.createObjectURL(file));
} else if (FileReader) {
reader = new FileReader();
reader.onload = function (e) {
done(reader.result);
};
reader.readAsDataURL(file);
}
}
});


$modal.on('shown.bs.modal', function () {
cropper = new Cropper(image, {
aspectRatio: 2,
viewMode: 1,
preview: '.preview'
});
}).on('hidden.bs.modal', function () {
cropper.destroy();
cropper = null;
});


$("#crop").click(function(){
canvas = cropper.getCroppedCanvas({
width: 800,
height: 360,
});
canvas.toBlob(function(blob) {
url = URL.createObjectURL(blob);
var reader = new FileReader();
reader.readAsDataURL(blob); 
reader.onloadend = function() {
var base64data = reader.result; 

$.ajax({
type: "POST",
dataType: "json",
url: "{{route('profil_image_croped')}}",
data: {'_token': $('meta[name="csrf-token"]').attr('content'), 'image': base64data},
success: function(data){
console.log(data);
$modal.modal('hide');
//alert("Crop image successfully uploaded");
$("#output").attr('src',"//"+data.path);
$("#croped").val(data.name);
}
});
}
});
})

// More code using $ as alias to jQuery
});
})(jQuery);
</script>
                    </div>
                </div>

            </div>

        </div>

    </div>
    </div>
</section>
@endsection