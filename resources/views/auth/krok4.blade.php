@extends('layouts.app')
@section('meta_seo')
<title>
    TanFinder - Rejestracja - Krok 4
</title>
@endsection

@section('content')
<section class="block">
    <div class="rejestracja">
        <div class="container">
            <h1>Rejestracja</h1>
            <div class="row">
                <ul class="rejestracja_lista">
                    <li class="active">Krok 1</li>
                    <li class="active">Krok 2</li>
                    <li class="active">Krok 3</li>
                    <li class="active">Krok 4</li>
                    <li>Krok 5</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="grey_block">
                <div class="container">
                    <div class="rejestracja_block">
                        <form method="post">
                            @csrf
                            @include('backend/_main/message')
                            <div class="main-row">
                                <h1>Podaj swoje stawki</h1>
                                {{-- {{print_r($categories)}}
                                {{print_r($services)}} --}}
                                @foreach ($services as $s)


                                <div class="row label hidden">
                                    <div class="col-md-8">
                                        <input style="position: absolute;
                                        top: 5px;
                                        left: -15px;
                                        width: auto;" type="checkbox" class="usluga_item" name="usluga_item[]">
                                        <p style="padding: 0;
margin: 10px 0;">{{$s->name}}</p>
                                        <input placeholder="Nazwa usługi" name="uslugi_nazwa[]"
                                            value="{{$s->id_service}}" type="hidden">
                                    </div>
                                    <div class="col-md-4">
                                        <strong>
                                            {{-- placeholder="{{rand(2,15)}}0 zł" --}}
                                            <input name="uslugi_ceny[]" placeholder="zł" class="uslugi_ceny"
                                                style="padding: 0;" type="number"
                                readonly="readonly">
                                            <input class="checkbox_item" type="checkbox" checked="checked"
                                                name="uslugi_pozycja[]" value="{{$s->id_service}}">

                                        </strong>
                                    </div>
                                </div>
                                @endforeach
                            </div>

                            <div class="footer-panel">
                                {{-- <button class="dodaj_usluge btn btn-primary" type="button">+ Dodaj usługe</button> --}}
                                {{-- <a href="#">+ Dodaj kategorię</a> --}}
                                {{-- <button class="dodaj_kategorie" type="button">+ Dodaj kategorię</button> --}}
                                {{-- <button class="usun_usluge btn-danger" type="button">- Usuń pozycję</button> --}}
                            </div>

                            <div style="text-align: center;">
                                <a class="btn btn-primary btn-secondary center" href="{{route('rejestracja_krok3')}}">Wstecz</a>
                                <button type="submit" class="btn btn-primary center">Dalej</button>
                                
                            </div>
                        </form>

                    </div>
                </div>

            </div>

        </div>

    </div>
    </div>
</section>
@endsection