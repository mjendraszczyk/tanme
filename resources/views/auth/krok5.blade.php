@extends('layouts.app')
@section('meta_seo')
<title>
    TanFinder - Rejestracja - Krok 5
</title>
@endsection
@section('content')
<section class="block">
    <div class="rejestracja">
        <div class="container">
            <h1>Rejestracja</h1>
            <div class="row">
                <ul class="rejestracja_lista">
                    <li class="active">Krok 1</li>
                    <li class="active">Krok 2</li>
                    <li class="active">Krok 3</li>
                    <li class="active">Krok 4</li>
                    <li class="active">Krok 5</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="grey_block">
                <div class="container">
                    <div class="rejestracja_block">
                        <form method="post">
                            @csrf
                            @include('backend/_main/message')
                            <textarea placeholder="Opis" name="short_description" id="short_description"></textarea>
                            <div style="text-align: center;">
                                 <a class="btn btn-primary center btn-secondary" href="{{route('rejestracja_krok4')}}">Wstecz</a>
                                <button type="submit" class="btn btn-primary center">Dalej</button>
                               
                            </div>
                        </form>

                    </div>
                </div>

            </div>

        </div>

    </div>
    </div>
</section>
@endsection