@extends('layouts.app')

@section('meta_seo')
<title>
    TanFinder - Rejestracja - Krok 1
</title>
@endsection

@section('content')

<section class="block">
    <div class="rejestracja">
        <div class="container">
            <h1>Rejestracja</h1>
            <div class="row">
                <ul class="rejestracja_lista">
                    <li class="active">Krok 1</li>
                    <li>Krok 2</li>
                    <li>Krok 3</li>
                    <li>Krok 4</li>
                    <li>Krok 5</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="grey_block">
                <div class="container">
                    <div class="rejestracja_block">
                        <form method="post" action="{{route('rejestracja_krok1_request')}}">
                            @csrf
                            @include('backend/_main/message')
                            <input class="ajax_call" placeholder="Nazwa Twojego profilu (*)" type="text"
                                name="business_user" data-url="{{route('checkBusinessName')}}" id="business_user"
                                value="{{old('business_user')}}">
                            <input placeholder="Alias url twojego profilu (*)" type="hidden" name="business"
                                id="business" value="{{old('business')}}">
                            {{-- <label for="">Nazwa twojego biznesu</label> --}}
                            Link do twojego profilu: <span class="profil_url"></span>
                            <span class="profil_stan"></span>

                            {{-- <label for="">Imię i nazwisko</label> --}}
                            <input placeholder="Imię i nazwisko (*)" type="hidden" name="imie" id="imie"
                                value="{{old('imie')}}">
                            {{-- <label for="">E-mail</label> --}}
                            <input placeholder="E-mail (*)" type="email" name="email" id="email"
                                value="{{old('email')}}">
                            {{-- <label for="">Hasło</label> --}}
                            <input placeholder="Hasło (*)" type="password" value="{{old('haslo')}}" name="haslo"
                                id="haslo">

                            <div style="text-align: center;">
                                <button type="submit" class="btn btn-primary center">Dalej</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>

    </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $('.ajax_call').keyup(function() {
            if($('.ajax_call').val().length > 3) {
            console.log("Dfd");
            //$(this).attr('name')
        $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });

        var request = $.ajax({
        url: $(this).attr('data-url'),
        method: "POST",
        data: { business_user : $(this).val() },
        // dataType: "html"
        });
        
        request.done(function( msg ) {
        if(msg.state == '1') {
            // console.log('zajete');
            $('.profil_stan').html('<span class="btn-danger">Zajęte</span>');
        } else {
            // console.log('wolne')
            $('.profil_stan').html('<span class="btn-success">Wolne</span>');
        }
        });
            } else {
                $('.profil_stan').html("");
            }
    })
    });
</script>
@endsection