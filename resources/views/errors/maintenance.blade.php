@extends('layouts.medbrain')


@section('content')
<div class="container">
    <div class="white m-50" style="    margin: 150px;">
        <div class="alert alert-danger blank-page">

            <i class="material-icons" style="
                font-size: 72px;
                color: #c8342a;
            ">
                error_outline
            </i>
            <h1 style="color: #c8342a;font-size:1.5rem;">Prace konserwacyjne</h1>

        </div>
    </div>
</div>
@endsection