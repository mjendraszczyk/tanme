<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('meta_seo')
    <!-- Scripts -->

    {{--
    <script src="{{ asset('js/app.js') }}" defer></script>
    --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous">
    </script>
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css2?family=Material+Icons" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
        crossorigin="" />
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js" defer></script>


    <script src="{{ asset('js/custom.js') }}" defer></script>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
        integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
        crossorigin=""></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/25.0.0/classic/ckeditor.js"></script>
    <script src="https://ckeditor.com/apps/ckfinder/3.5.0/ckfinder.js"></script>
    <script src="https://unpkg.com/vue/dist/vue.js"></script>
    <script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-6XLJ6QDM06"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'G-6XLJ6QDM06');
    </script>
</head>

<body class="@yield('body')">
    <div id="app">
        @include('main.header')
        <main class="py-4">
            @yield('content')
        </main>
        @include('main.footer')

    </div>


    <script type="text/javascript">
        $(document).ready(function() {
            
            $("#business_user").keyup(function() {
                var alias = $(this).val()
                .toLowerCase()
                .replace(/ /g,'-')
                .replace(/[^\w-]+/g,'');
                $(".profil_url").text("{{env('SITE_URL')}}/profil/"+alias);

    $("#business").val(alias);

    }).ready(function() {
    if($('#business_user').val() != '') {
    // $(".profil_url").text($('#business').val()+".tanfinder.pl");

    var alias = $("#business").val();
    $(".profil_url").text("{{env('SITE_URL')}}/profil/"+alias);

    $("#business").val(alias);
    }
    });
    // $(".korzysci-box").hover(function () {
    // var getNewUrl = $(this).children('img').attr('data-hover');

    // $(this).children('img').attr('data-hover',$(this).children('img').attr('src'));
    // $(this).children('img').attr('src',getNewUrl);

    // }).mouseout(function() {
    // var getNewUrl = $(this).children('img').attr('data-hover');
    // $(this).children('img').attr('data-hover',$(this).children('img').attr('src'));
    // $(this).children('img').attr('src',getNewUrl);

    // });

    //import SimpleUploadAdapter from '@ckeditor/ckeditor5-upload/src/adapters/simpleuploadadapter';

    ClassicEditor
    .create( document.querySelector( '.editor' ), {
    //plugins: [ SimpleUploadAdapter],
    // simpleUpload: {
    // // The URL that the images are uploaded to.
    // uploadUrl: 'http://tanfinder.mages.pl/img/cms',

    // // Enable the XMLHttpRequest.withCredentials property.
    // withCredentials: true,

    // // Headers sent along with the XMLHttpRequest to the upload server.
    // headers: {
    // 'X-CSRF-TOKEN': 'CSRF-Token',
    // Authorization: 'Bearer <JSON Web Token>'
        // }
        // }
        })
        .then( editor => {
        console.log( editor );
        } )
        .catch( error => {
        console.error( error );
        } );


        var map = L.map('map').setView([$("#lat").val(), $("#lon").val()], 13);

        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: '',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoibWFnZXMiLCJhIjoiY2trazBjcGl3MjI3MTJwcXQ3eXcwdjdscyJ9.MHsv5I0zZVZhXTceF5fv5w'
        }).addTo(map);


        var icon = L.divIcon({
        iconSize:null,
        html:`<div class="btn-floating icon" style="background-image:url({{asset('img/ekspertka_marker.png')}});">
        </div>`});

        // var marker = L.marker([data.lat, data.lon],{icon:
        // icon}).addTo(mymap);
        // });

        L.marker([$("#lat").val(), $("#lon").val()],{icon: icon}).addTo(map);



        });

    </script>

    <script>
        $(document).ready(function() {
        var mymap2 = L.map('mapLatLon').setView([52.3009024,19.7999369], 5);
        
        mymap2.options.minZoom = 5;
        mymap2.options.maxZoom = 15;

        var gl = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}',{
        attribution: '',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoibWFnZXMiLCJhIjoiY2trazBjcGl3MjI3MTJwcXQ3eXcwdjdscyJ9.MHsv5I0zZVZhXTceF5fv5w'
        }).addTo(mymap2);
        
        
        var dx=0;
        var marker;
        
        var icon = L.divIcon({
        iconSize:null,
        html:`<div class="btn-floating icon" style="background-image:url({{asset('img/ekspertka_marker.png')}});">
    </div>`});

    if(($("#lat").val() != '') && ($("#lon").val() != '')) {
    marker = L.marker([$("#lat").val(), $("#lon").val()],{icon:icon}).addTo(mymap2);
    } else {
    marker = L.marker([52.3009024, 19.7999369],{icon:icon}).addTo(mymap2);
    }


    mymap2.on('click', function(e) {
    //if(dx > 0) {

    //alert("F");

    $(".leaflet-marker-icon").remove();
    $(".leaflet-popup").remove();
    $(".leaflet-shadow-pane").remove();
    //}

    marker = L.marker([e.latlng.lat, e.latlng.lng], {icon:icon}).addTo(mymap2);
    $("#lat").val(e.latlng.lat);
    $("#lon").val(e.latlng.lng);

    dx++;
    });



    $("#adres").change(function() {
    var ulica = $("#adres").val();
    console.log("ulica "+ulica);
    var getMiasto = $("input[name=miasto_hidden]").val();
    var address = getMiasto + ", "+ulica;
    $.get(location.protocol + '//nominatim.openstreetmap.org/search?format=json&q=' + address, function (data) {

    $("#lat").val(data[0].lat);
    $("#lon").val(data[0].lon);

    $(".leaflet-marker-icon").remove();
    $(".leaflet-popup").remove();
    $(".leaflet-shadow-pane").remove();
    //}

    marker = L.marker([$("#lat").val(), $("#lon").val()], { icon: icon }).addTo(mymap2);
    })
    });
    $("select[name=miasto]").change(function () {
    var ulica = $("#adres").val();
    var data = $(this).select2('data');
    $("input[name=miasto_hidden]").val(data[0].text);
    var address = (data[0].text+ ", "+ ulica);
    console.log(data[0].id);
    // var address = $(this);
    // console.log(address);
    $.get(location.protocol + '//nominatim.openstreetmap.org/search?format=json&q=' + address, function (data) {

    $("#lat").val(data[0].lat);
    $("#lon").val(data[0].lon);

    $(".leaflet-marker-icon").remove();
    $(".leaflet-popup").remove();
    $(".leaflet-shadow-pane").remove();
    //}

    marker = L.marker([$("#lat").val(), $("#lon").val()], { icon: icon }).addTo(mymap2);
    // $("#lat").val(e.latlng.lat);
    // $("#lon").val(e.latlng.lng);
    });
    });
    });
    </script>

    <script>
        $(document).ready(function(){
        setTimeout(() => {
        $('.ekspert-container').css('opacity','1');
        }, 500);
    $('.customselect2').select2();
    $('.miastaselect').select2({
        minimumInputLength: 3
    });


      $(".filtry select").change(function () {
       //   $('.ekspert-container').css('opacity','0');
          $(".ekspert-container").html("<img src='{{asset('img')}}/loading.gif' />");
//alert("F");
$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
var wojewodztwo = '';
if ($('select[name=wojewodztwo]').val() == '') {
wojewodztwo = '0';
} else {
wojewodztwo = $('select[name=wojewodztwo]').val();
//miasto = '';
}

var miasto = '';



if ($('select[name=miasto]').val() == '') {
miasto = '0';
} else {
miasto = $('select[name=miasto]').val();
}

if($(this).attr('name') == 'wojewodztwo') {
// alert($(this).attr('name'));
miasto = '0';
}
var url_ajax_api =
"/api/ekspert/opalanie-natryskowe/"+wojewodztwo+"/wszystkie/"+miasto+"/wszystkie/filtr/"+$('select[name=filtr]').val();
console.log(url_ajax_api);

$.ajax({

method: "GET",
dataType:'html',
//url: "{{route('ekspert_kontakt_store')}}",
url: url_ajax_api,
beforeSend: function() {
// setting a timeout

// $(".ekspert-container")
},
//url:"./ekspert/opalanie-natryskowe/1/2/3",
//data: { wojewodztwo: $("select[name=wojewodztwo]").val(), miasto: $("select[name=miasto]").val(), filtr:
$("select[name=filtr]").val() }
})
.done(function( msg ) {
setTimeout(() => {
$('.ekspert-container').css('opacity','1');
console.log(msg);
$(".ekspert-container").html(msg);
}, 500);


});
});
});
    </script>


    <script>
        // 0. If using a module system (e.g. via vue-cli), import Vue and VueRouter
        // and then call `Vue.use(VueRouter)`.
    
        // 1. Define route components.
        // These can be imported from other files
        const getEksperts = {
            template: `
        <div class="row label">
            <div class="col-md-4">
                <a href="{{route('profil_alias',['business'=>$item->business])}}">
                    <div class="ekspert_feature image_placeholder"
                        style="background-image: url({{asset('/img/ekspert')}}/{{$item->feature_image}});">
                        @if($item->feature_image == '')
                        {{-- <img src="{{asset('/img/ekspert')}}/{{$item->feature_image}}"
                        alt=""> --}}
                        <img src="{{asset('/img')}}/image.svg" style="width:100px;" alt="">
                        @endif
                        {{-- <img src="{{asset('/img/ekspert')}}/{{$item->feature_image}}" alt=""> --}}
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                @if($item->feature == 1)
                <div style="background: rgb(230, 216, 163) none repeat scroll 0% 0%; color: rgb(255, 255, 255); padding: 15px; text-transform: uppercase;max-width: 200px;left: -15px;top: -27px;"
                    class="label-cert gold"><span style="margin: 3px 0px 0px; display: inline-block;">Szkoleniowiec</span> </div>
                @endif
                <h1><a href="{{route('profil_alias',['id'=>$item->business])}}" style="color:inherit;">{{$item->name}}</a></h1>
                <p>
                    {{$item->address}} {{\App\Http\Controllers\Controller::getCityName($item->city)}}
                </p>
            </div>
            <div class="col-md-2">
                <div class="ocena">
                    <p>{{App\Http\Controllers\frontend\EkspertController::avgReview($item->id_ekspert)}}</p>
                    <p>
                        {{App\Http\Controllers\frontend\EkspertController::countReview($item->id_ekspert)}}
                        opini
                    </p>
                </div>
                @if($item->cert == 1)
                <div class="label-cert" style="background: #000;color: #fff;padding: 15px 15px;text-transform: uppercase;">
                    <span style="margin: 3px 0 0 0;display: inline-block;">
                        Certyfikat
                    </span>
                    <img class="certyfikat" src="{{asset('/img/glowna_tanexpert.png')}}" alt=""
                        style="display: inline-block;width: 50%;vertical-align: bottom;padding: 0 10px;">
                </div>
                @endif
                @if(($item->typ == 1) || ($item->typ == 3))
                <img class="ekpert_icon" src="{{asset('/img/glowna_auto_icon.png')}}" alt="">
                @endif
                @if(($item->typ == 0) || ($item->typ == 3))
                <img class="ekpert_icon" src="{{asset('/img/glowna_sklep_icon.png')}}" @if($item->typ == 3) style="margin-right:50px;" @endif alt="">
                @endif
            </div>
        
        </div>
        ` }
        const Bar = {
            template: `
        <div>bar</div>
        ` }
    
        // 2. Define some routes
        // Each route should map to a component. The "component" can
        // either be an actual component constructor created via
        // `Vue.extend()`, or just a component options object.
        // We'll talk about nested routes later.
        const routes = [
            { path: '/ekspert/opalanie-natryskowe/wojewodztwo/:wojewodztwo/miasto/:miasto/filtr/:filtr', component: getEksperts },
            { path: '/bar', component: Bar }
        ]
    
        // 3. Create the router instance and pass the `routes` option
        // You can pass in additional options here, but let's
        // keep it simple for now.
        const router = new VueRouter({
            routes // short for `routes: routes`
        })
    
        // 4. Create and mount the root instance.
        // Make sure to inject the router with the router option to make the
        // whole app router-aware.
        const app = new Vue({
            router
        }).$mount('#app')
    
    // Now the app has started!
    
    </script>
</body>

</html>