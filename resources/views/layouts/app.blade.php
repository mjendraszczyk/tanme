<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('meta_seo')
    <!-- Scripts -->


    <script src="{{ asset('js/app.js') }}" defer></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous">
    </script>
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css2?family=Material+Icons" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <link href="{{ asset('css/growl.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
        crossorigin="" />
    

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js" defer></script>

<link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/owl.theme.default.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css"
        integrity="sha512-H9jrZiiopUdsLpg94A333EfumgUBpO9MdbxStdeITo+KEIMaNfHNvwyjjDJb+ERPaRS6DpyRlKbvPUasNItRyw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

    <script src="{{ asset('js/custom.js') }}" defer></script>
    <script src="{{ asset('js/growl.js') }}" defer></script>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
        integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
        crossorigin=""></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"
            integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script src="https://cdn.ckeditor.com/ckeditor5/25.0.0/classic/ckeditor.js"></script>
    <script src="https://ckeditor.com/apps/ckfinder/3.5.0/ckfinder.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-6XLJ6QDM06"></script>

<script src="{{asset('js/owl.carousel.min.js')}}" defer></script>

<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }

    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }

    .modal-lg {
        max-width: 1000px !important;
    }
</style>

<script>
    window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-6XLJ6QDM06');
</script>
</head>

<body class="@yield('body')">
    <div id="app">
        @include('main.header')
        <main class="py-4">
            @yield('content')
        </main>
        @include('main.footer')

    </div>


    <script type="text/javascript">
        $(document).ready(function() {
            
            $("#business_user").keyup(function() {
                var alias = $(this).val()
                .toLowerCase()
                .replace(/ /g,'-')
                .replace(/[^\w-]+/g,'');
                $(".profil_url").text("{{env('SITE_URL')}}/profil/"+alias);

    $("#business").val(alias);

    }).ready(function() {
    if($('#business_user').val() != '') {
    // $(".profil_url").text($('#business').val()+".tanfinder.pl");

    var alias = $("#business").val();
    $(".profil_url").text("{{env('SITE_URL')}}/profil/"+alias);

    $("#business").val(alias);
    }
    });

if(document.querySelector('.editor') != null) {
    ClassicEditor
    .create( document.querySelector( '.editor' ), {
        })
        .then( editor => {
        console.log( editor );
        } )
        .catch( error => {
        console.error( error );
        } );
}

if((document.getElementById("map") != null) || (document.getElementById("mapLatLon") != null)) {
        var map = L.map('map').setView([$("#lat").val(), $("#lon").val()], 13);

        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: '',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoibWFnZXMiLCJhIjoiY2trazBjcGl3MjI3MTJwcXQ3eXcwdjdscyJ9.MHsv5I0zZVZhXTceF5fv5w'
        }).addTo(map);


        var icon = L.divIcon({
        iconSize:null,
        html:`<div class="btn-floating icon" style="background-image:url({{asset('img/ekspertka_marker.png')}});">
        </div>`});

        // var marker = L.marker([data.lat, data.lon],{icon:
        // icon}).addTo(mymap);
        // });

        L.marker([$("#lat").val(), $("#lon").val()],{icon: icon}).addTo(map);
}
        });


    </script>

    <script>
        $(document).ready(function() {
            if((document.getElementById("map") != null) || (document.getElementById("mapLatLon") != null)) {
        var mymap2 = L.map('mapLatLon').setView([52.3009024,19.7999369], 5);
        
        mymap2.options.minZoom = 5;
        mymap2.options.maxZoom = 15;

        var gl = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}',{
        attribution: '',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoibWFnZXMiLCJhIjoiY2trazBjcGl3MjI3MTJwcXQ3eXcwdjdscyJ9.MHsv5I0zZVZhXTceF5fv5w'
        }).addTo(mymap2);
        
        
        var dx=0;
        var marker;
        
        var icon = L.divIcon({
        iconSize:null,
        html:`<div class="btn-floating icon" style="background-image:url({{asset('img/ekspertka_marker.png')}});">
    </div>`});

    if(($("#lat").val() != '') && ($("#lon").val() != '')) {
    marker = L.marker([$("#lat").val(), $("#lon").val()],{icon:icon}).addTo(mymap2);
    } else {
    marker = L.marker([52.3009024, 19.7999369],{icon:icon}).addTo(mymap2);
    }


    mymap2.on('click', function(e) {

    $(".leaflet-marker-icon").remove();
    $(".leaflet-popup").remove();
    $(".leaflet-shadow-pane").remove();
    //}

    marker = L.marker([e.latlng.lat, e.latlng.lng], {icon:icon}).addTo(mymap2);
    $("#lat").val(e.latlng.lat);
    $("#lon").val(e.latlng.lng);

    dx++;
    });


/*
    $("#adres").change(function() {
    var ulica = $("#adres").val();
    console.log("ulica "+ulica);
    var getMiasto = $("input[name=miasto_hidden]").val();
    var address = getMiasto + ", "+ulica;
    $.get(location.protocol + '//nominatim.openstreetmap.org/search?format=json&q=' + address, function (data) {

    $("#lat").val(data[0].lat);
    $("#lon").val(data[0].lon);

    $(".leaflet-marker-icon").remove();
    $(".leaflet-popup").remove();
    $(".leaflet-shadow-pane").remove();
    //}

    marker = L.marker([$("#lat").val(), $("#lon").val()], { icon: icon }).addTo(mymap2);
    })
    }); */

    function protoMapMarker() {
        var ulica = $("#adres").val();
        var data = $("select[name=miasto]").select2('data');
        $("input[name=miasto_hidden]").val(data[0].text);
        var address = (data[0].text+ ", "+ ulica);
        console.log(data[0].id);
        //alert(address);
        // var address = $(this);
        // console.log(address);
        $.get(location.protocol + '//nominatim.openstreetmap.org/search?format=json&q=' + address, function (data) {
        
        $("#lat").val(data[0].lat);
        $("#lon").val(data[0].lon);
        
        $(".leaflet-marker-icon").remove();
        $(".leaflet-popup").remove();
        $(".leaflet-shadow-pane").remove();
        //}
        
        marker = L.marker([$("#lat").val(), $("#lon").val()], { icon: icon }).addTo(mymap2);
        });
    }
    $("select[name=miasto], #adres").on('change blur',function () {
      // alert("zmiana miasta");
        protoMapMarker();
    });

$('#profil_save').click(function (e) {
$('#form_companies').css('opacity',0.5);
e.preventDefault();
protoMapMarker();

setTimeout(function(){
//$('#form_companies').css('opacity',1.0);
document.getElementById("form_companies").submit();
//alert("Hello");
}, 3000);

});
    }
    });
    </script>

    <script type="text/javascript">

        $(document).ready(function(){
$('.owl-carousel').owlCarousel({
//loop:true,
margin:10,
nav:true,
dots:false,
//autoplay:true,
responsiveClass:true,
responsive:{
0:{
items:1
},
600:{
items:3
},
1000:{
items:4
}
}
});


        setTimeout(() => {
        $('.ekspert-container').css('opacity','1');
        }, 500);
    $('.customselect2').select2();
    $('.miastaselect').select2({
        minimumInputLength: 3
    });


      $(".filtry select").change(function () {
       //   $('.ekspert-container').css('opacity','0');
          $(".ekspert-container").html("<img src='{{asset('img')}}/loading.gif' />");
//alert("F");
$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
var wojewodztwo = '';
if ($('select[name=wojewodztwo]').val() == '') {
wojewodztwo = '0';
} else {
wojewodztwo = $('select[name=wojewodztwo]').val();
//miasto = '';
}

var miasto = '';

if ($('select[name=miasto]').val() == '') {
miasto = '0';
} else {
miasto = $('select[name=miasto]').val();
}

if($(this).attr('name') == 'wojewodztwo') {
// alert($(this).attr('name'));
miasto = '0';
}

if(wojewodztwo != undefined) {
var url_ajax_api =
"/api/ekspert/opalanie-natryskowe/"+wojewodztwo+"/wszystkie/"+miasto+"/wszystkie/filtr/"+$('select[name=filtr]').val();
var url_ajax_api_wojewodztwo =
"/api/wojewodztwo/"+wojewodztwo;

var url_ajax_api_miasto =
"/api/miasto/"+miasto;

//console.log(url_ajax_api);

$.ajax({

method: "GET",
dataType:'html',
//url: "{{route('ekspert_kontakt_store')}}",
url: url_ajax_api,
beforeSend: function() {
// setting a timeout

// $(".ekspert-container")
},
//url:"./ekspert/opalanie-natryskowe/1/2/3",
data: { wojewodztwo: $("select[name=wojewodztwo]").val(), miasto: $("select[name=miasto]").val(), filtr:
$("select[name=filtr]").val() }
})
.done(function( msg ) {
    console.log(msg);
 
    var $response=$(msg);
   
    var wojewodztwo_name = '';
    if($response.filter('#wojewodztwo_name').val() == '') {
         wojewodztwo_name = 'wszystkie';
    } else {
        wojewodztwo_name = $response.filter('#wojewodztwo_name').val();
    }
    

    if($response.filter('#miasto_name').val() == '') {
    miasto_name = 'wszystkie';
    } else {
    miasto_name = $response.filter('#miasto_name').val();
    }

// console.log(msg);
    var wojewodztwo_title = '';
    var miasto_title = '';
    if(wojewodztwo_name == 'wszystkie') {
         wojewodztwo_title = '';
    } else {
        wojewodztwo_title = wojewodztwo_name;
    }

    if(miasto_name == 'wszystkie') {
    miasto_title = '';
    } else {
        
    miasto_title = ' ' + miasto_name.charAt(0).toUpperCase() + miasto_name.slice(1);
    wojewodztwo_title = '';
    }
    
    $(document).attr('title', 'Opalanie natryskowe '+wojewodztwo_title+' '+miasto_title+' - TanMe');
    window.history.pushState("", "", '/ekspert/opalanie-natryskowe/'+wojewodztwo+'/'+wojewodztwo_name+'/'+miasto+'/'+miasto_name+'/filtr/0');
setTimeout(() => {
$('.ekspert-container').css('opacity','1');
console.log(msg);
$(".ekspert-container").html(msg);
}, 500);


});
}
});

});
    </script>


</body>

</html>