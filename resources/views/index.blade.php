@extends('layouts.app')

@section('meta_seo')
<title>
    TanMe - Opalanie natryskowe w Twoim mieście
</title>
@endsection
@section('content')
<section class="block"> 
    
    <div class="container">
        <h1>Polecane Ekspertki</h1>
        <div class="row">
            @foreach ($eksperci as $item)
            <div class="col-md-4">
                <a href="{{route('profil_alias',['id'=>$item->business])}}">
                    <div style="background-image:url({{asset('/img/ekspert')}}/{{$item->feature_image}});"
                        class="image_placeholder">
                        @if($item->feature_image == '')
                        {{-- <img src="{{asset('/img/ekspert')}}/{{$item->feature_image}}"
                        alt=""> --}}
                        <img src="{{asset('/img')}}/image.svg" style="width:100px;" alt="">
                        @endif
                        {{-- <img src="{{asset('/img/ekspert')}}/{{$item->feature_image}}" alt=""> --}}
                    </div>
                </a>
                <div class="row caption-block">
                    <div class="col-md-8">
                        <h3>
                            <a href="{{route('profil_alias',['business'=>$item->business])}}">
                                {{$item->name}}
                            </a>
                        </h3>
                        <p>
                            {{$item->address}}
                        </p>
                    </div>
                    <div class="col-md-4">
                        @if(($item->typ == 1) || ($item->typ == 3))
                        <img class="ekpert_icon" src="{{asset('/img/glowna_auto_icon.png')}}" alt="">
                        @endif
                        @if(($item->typ == 0) || ($item->typ == 3))
                        <img class="ekpert_icon" src="{{asset('/img/glowna_sklep_icon.png')}}" @if($item->typ == 3) style="margin-right:0px;position: relative;" @endif alt="">
                        @endif
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</section>
<section class="block">
    <div class="dolacz">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="col-md-6">
                        <h1>
                            Dołącz do nas
                        </h1>
                        <p>
                            Dołącz do TanExpert i pracuj na najlepszych kosmetykach z najbardziej prestiżowym teamem w Polsce! Zapisz się
                            na szkolenie w zakresie opalania natryskowego i opalaj z wykorzystaniem organicznych kosmetyków marki TanExpert i
                            MineTan.
                        </p>
                    </div>
                    <a href="{{route('szkolenie_index')}}">
                        <img src="{{asset('/img/szkolenia01.png')}}" alt="">
                    </a>
                    <a href="{{route('szkolenie_index')}}">
                        <img src="{{asset('/img/szkolenia-kalisz.png')}}" alt="">
                    </a>
                    <a href="{{route('szkolenie_index')}}">
                        <img src="{{asset('/img/szkolenia03.png')}}" alt="">
                    </a>
                    <a href="{{route('szkolenie_index')}}">
                        <img src="{{asset('/img/szkolenia04.png')}}" alt="">
                    </a>
                    <a href="{{route('szkolenie_index')}}">
                        <img src="{{asset('/img/szkolenia-bydgoszcz.png')}}" alt="">
                    </a>
                    <br>
                    <div class="col-md-6">
                        <a href="{{route('rejestracja_krok1')}}" class="btn btn-primary">Zobacz</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- <section class="block">
    <div class="container">
        <h1>
            Sprawdź nasz Instagram i poznaj gwiazdy, które polecają TanExpert
        </h1>
    </div> 
     <div class="instagram">
        <ul class="instagram-feed-block row" style="margin:0;padding:0;">
            {{App\Http\Controllers\Controller::getInstagram()}}
        </ul>
    </div> 
</section> --}}
<section class="block">
    <div class="ekspertka">
        <div class="container">
            <h1>
                Szukasz ekspertki w zakresie opalania natryskowego?
            </h1>
            <p>
                TanMe to serwis, na którym znajdziesz specjalistów wyłącznie z branży opalania natryskowego. Warto zwrócić uwagę, że
                wszystkie osoby, które ogłaszają się tutaj i przeszły profesjonalne szkolenie w TanExpert oznaczone są dodatkową
                informacją „Certyfikat TanExpert”. Dzięki temu otrzymujesz gwarancję, że Twój zabieg przeprowadzi osoba, która została
                wdrożona przez jednego z naszych szkoleniowców. Ponadto po dodaniu Ekspertki do naszej bazy - zweryfikowaliśmy czy osoba
                lub studio faktycznie świadczy czynnie usługi w zakresie opalania natryskowego. Na podstronie każdego wpisu znajdziesz
                m.in. informacje na temat oferty oraz czy dana osoba wykonuje zabieg mobilnie czy stacjonarnie co pomoże Ci dobrać
                odpowiednią osobę według Twoich preferencji.  Przed skorzystaniem z usługi sugerujemy zweryfikować opinię, a po
                skorzystaniu serdecznie zapraszamy do opisania swoich wrażeń :)
            </p>
            <div class="row">
                @foreach(App\Http\Controllers\Controller::getCitiesHome() as $item)
                <div class="col-md-20">
                    <a
                        href="{{route('eksperts_by_filters',['wojewodztwo'=>$item->id_province,'wojewodztwo_name'=>str_slug(App\Http\Controllers\Controller::getProvinceDependsOnCity($item->id_city)),'miasto'=>$item->id_city,'miasto_name'=>str_slug(App\Http\Controllers\Controller::getCityName($item->id_city)),'filtr'=>'0'])}}">
                        <span>
                            {{$item->name}}
                        </span>
                        <img src="{{asset('/img/glowna_')}}{{$item->image}}" alt="">
                    </a>
                </div>
                @endforeach
                <div class="col-md-20">
                    <a href="{{route('ekspert_index')}}" class="green_bg">
                        <span>Więcej</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="block">
    <div class="blog">
        <div class="container">
            <h1>Blog</h1>
            <div class="row">
                @foreach ($blog as $item)
                <div class="col-md-4">
                    <a href="{{route('blog_show',['id'=>$item->id_blog,'name'=>str_slug($item->title)])}}">
                        <img src="{{asset('/img/blog')}}/{{$item->feature_image}}" alt="">
                    </a>
                    <h3>{{$item->title}}</h3>
                    <p>
                        {{strip_tags(str_limit($item->content, 150))}}
                        <br>
                        <a href="{{route('blog_show',['id'=>$item->id_blog,'name'=>str_slug($item->title)])}}">czytaj
                            więcej</a>
                    </p>
                </div>
                @endforeach
            </div>
            <a href="{{route('blog_index')}}" class="btn btn-priamry">Zobacz wszystkie artykuły</a>
        </div>
    </div>
</section>
@endsection