<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
<div style="text-align:center;background:#fff;width:100%;padding:25px 0;">
    <img src="{{asset('img/glowna_top_tanexpert_black.png')}}" />
</div>
<div class="container">

    <h1 style="margin:50px 0;display:block;text-align:center;">{{$temat}}</h1>
    <div class='card' style="border:0px;border-radius:0px;width:85%;margin:20px auto;display:block;">
        <div class='card-body'>
            <div style="text-align:center;width:100%;display:block;margin:25px 0;">
                <strong>Dane użytkownika:</strong>
                <br />
                {{$konto['imie']}} <br />
                {{$konto['telefon']}} <br />
                {{$konto['email']}}
                <br />
                <br />
                <strong>Treść:</strong>
                <br /><br />
                {{$konto['wiadomosc']}} <br />
            </div>
        </div>
    </div>

    <div style="width:100%;clear:both;text-align:center;">
        Pozdrawiam,<br>
        {{env('APP_DOMAIN')}}
        {{-- {{(App\Http\Controllers\Controller::getUstawienia()['firma'])}}
        <br>
        {{(App\Http\Controllers\Controller::getUstawienia()['adres'])}} <br />
        tel. {{(App\Http\Controllers\Controller::getUstawienia()['telefon'])}} <br />
        email:
        {{(App\Http\Controllers\Controller::getUstawienia()['email'])}}
        <br> --}}
    </div>
</div>